﻿import getpass
import inspect
import errno
import glob
import win32security        # pywin32 package
import win32api             # pywin32 package
import winreg
from sys import argv, exit, exc_info
from os import path, getcwd, makedirs, walk, system, getenv, remove, rename, listdir, mkdir
import PyQt5.QtWebEngineWidgets
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog, QMessageBox, QProgressBar, QLabel, QVBoxLayout, QDialog, QListWidget, QAbstractItemView, QLineEdit, QAction, QSlider, QHBoxLayout
from PyQt5.QtCore import Qt, QRegExp, pyqtSlot, QTimer, pyqtSignal, QEvent
from PyQt5.QtGui import QRegExpValidator, QValidator, QTextDocument, QPixmap, QIcon, QTextCursor
from PyQt5.QtPrintSupport import QPrinter
from shutil import move, copyfile, rmtree, copy
from datetime import datetime
from frm_xw_launcher import Ui_frm_XW_Launcher
from frm_xw_newcase import Ui_frm_XW_NewCase
from frm_xw_config import MainWindow
from copy import deepcopy
from subprocess import Popen
from configparser import ConfigParser
from logging import StreamHandler, basicConfig, getLogger, DEBUG
from logging.handlers import TimedRotatingFileHandler


__version__ = '$1.0.2019.05.16.1915 indev python3.6-pyqt5.10$'
__date__ = '25.11.2017'
__author__ = 'Unkraut77 (https://gitlab.com/no_more_secrets/)'
__release__ = 'master'
__src_dir__ = path.dirname(path.realpath(__file__))
# __update_cmd__ = ('pip install --src=%s% --upgrade -e git://gitlab.com/unkraut77/xways-launcher.git@%s#egg=swork')

app = None
glboolDebugToFile = True
glDebugLevel = DEBUG

if path.ismount(getcwd()):
    glfileLogPath = path.join(getcwd(), "log", getpass.getuser())
else:
    glfileLogPath = path.join(getenv('LOCALAPPDATA'), 'Xways-Launcher', 'log')
    # glfileLogPath = path.join("c:\\users\\", getpass.getuser(), "\\AppData\\Local\\xways-Launcher\\log\\")

glfileLogFile = path.join(glfileLogPath, "X-Ways_Launcher_errlog.txt")     # _" + datetime.now().strftime("%Y-%m-%d") + ".txt"      # _%H-%M-%S
if not path.exists(glfileLogPath):
    makedirs(glfileLogPath)
# else:
# glfileLogFile ="\\X-Ways_Launcher_errlog_" + datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + ".txt"
timeRotateHandler = TimedRotatingFileHandler(glfileLogFile, when="d", interval=1, backupCount=30)   # INFO: Logfilerotation: wann: täglich, wie oft=Intervall:1, backupCount:bis zu 30 Tage zurück
basicConfig(level=glDebugLevel,
            format='%(asctime)s %(name)-12s: %(levelname)-8s Zeile:%(lineno)-12d %(message)s',
            datefmt='%d-%m-%y %H:%M',
            handlers=[StreamHandler(), timeRotateHandler]
            )  # Filelogger  filename=glfileLogFile, ,filemode='w'

# console = StreamHandler()  # Consolelogger
# console.setLevel(DEBUG)  # Level set to DEBUG on Console
# formatter = Formatter('%(name)-12s: %(levelname)-8s Zeile:%(lineno)-12d %(message)s')
# console.setFormatter(formatter)
# logger = getLogger('')  # logger bekommt root handler
# logger.addHandler(console)  # logger bekommt console handler
# timeRotateHandler = TimedRotatingFileHandler(glfileLogFile, when="D", interval=14, backupCount=15)
# logger.addHandler(timeRotateHandler)
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# https://elementztechblog.wordpress.com/2015/04/14/getting-started-with-pycharm-and-qt-4-designer/
# https://github.com/baoboa/pyqt5/blob/master/examples/widgets/lineedits.py
# https://www.tutorialspoint.com/pyqt/pyqt_qmessagebox.htm
# https://www.programcreek.com/python/
# https://www.devdungeon.com/content/working-binary-data-python
# https://docs.python.org/3.5/library/struct.html
# https://www.mkyong.com/python/python-3-convert-string-to-bytes/

# Dictionary dictLauncher: # Grundeinstellung.

gldictLauncher = {
    'XWLauncherID': {
        'AppXWI': __version__,
        'AppXWF': __version__,
        'FilePos': 0},
    'XWFquelle': {
        'AppXWI': 'XWI-Versionen',
        'AppXWF': 'XWF-Versionen',
        'FilePos': 0},
    'CFGFileName': {
        'AppXWI': 'Winhex.cfg',
        'AppXWF': 'Winhex.cfg',
        'FilePos': 0},
    'FolderNameOpenContainer': {
        'AppXWI': '',
        'AppXWF': '',
        'FilePos': 14252},
    'TerminationChar': {
        'AppXWI': b'\0',
        'AppXWF': b'\0',
        'FilePos': 0},
    'XWAYSexe64': {
        'AppXWI': 'xwinvestigator64.exe',
        'AppXWF': 'xwforensics64.exe',
        'FilePos': 0},
    'XWAYSexe32': {
        'AppXWI': 'xwinvestigator.exe',
        'AppXWF': 'xwforensics.exe',
        'FilePos': 0},
    'FolderNameViewer64': {
        'AppXWI': '..\\12_viewer\\x64\\viewer',
        'AppXWF': '.\\x64\\viewer',
        'FilePos': 10502},          # 10310 v19.7
    'FolderNameViewer32': {
        'AppXWI': '..\\12_viewer',
        'AppXWF': '.\\viewer',
        'FilePos': 10310},
    'FolderNamePlayer': {
        'AppXWI': '..\\11_mplayer\\mplayer.exe',
        'AppXWF': '.\\MPlayer\\mplayer.exe',
        'FilePos': 11462},
    'FolderNameFavMediaPlayer': {
        'AppXWI': '..\\11_mplayer\\mplayer.exe',
        'AppXWF': '.\\MPlayer\\mplayer.exe',
        'FilePos': 15805},
    'FolderNameNotepad': {
        'AppXWI': '.\\editor\\notepad++.exe',
        'AppXWF': '.\\editor\\notepad++.exe',
        'FilePos': 11718},
    'FolderNameIExplorer': {
        'AppXWI': 'C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe',
        'AppXWF': 'C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe',
        'FilePos': 10822},
    'FolderNameFavHTMLViewer': {
        'AppXWI': 'C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe',
        'AppXWF': 'C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe',
        'FilePos': 11974},
    'FolderNameExtensions': {
        'AppXWI': '.',
        'AppXWF': '.\\X-Tensions',
        'FilePos': 10054},
    'XWAYSpath': {
        'AppXWI': '10_Programm_XWI',
        'AppXWF': '10_XWF',
        'FilePos': 0},
    'FolderNameSicherungen': {
        'AppXWI': '21_Sicherungen',
        'AppXWF': 'Images',
        'FilePos': 9670},
    'FolderNameTemp': {
        'AppXWI': '20_Temp',
        'AppXWF': '60_TMP',
        'FilePos': 9414},
    'FolderNameHashDB': {
        'AppXWI': '30_HashDB',
        'AppXWF': '30_HashDB',
        'FilePos': 8390},
    'FolderNameHashDB2': {
        'AppXWI': '31_HashDB2',
        'AppXWF': '31_HashDB2',
        'FilePos': 8646},
    'FolderNameHashDBPDNA': {
        'AppXWI': '32_HashDB_PDNA',
        'AppXWF': '32_HashDB_PDNA',
        'FilePos': 8134},
    'FolderNameHashDBFuzzy': {
        'AppXWI': '33_HashDB_FuZzyDoc',
        'AppXWF': '33_HashDB_FuZzyDoc',
        'FilePos': 0},
    'FolderNameFillInHashDB': {
        'AppXWI': '',
        'AppXWF': 'D:\\Hashwerte',
        'FilePos': 0},
    'FolderNameFillInHashDB_PDNA': {
        'AppXWI': '',
        'AppXWF': 'D:\\Hashwerte',
        'FilePos': 0},
    'FolderNameFillInTemp': {
        'AppXWI': '',
        'AppXWF': 'C:\\Temp',
        'FilePos': 0},
    'FolderNameFillInSicherungen': {
        'AppXWI': '..',
        'AppXWF': '..\\..',
        'FilePos': 0},
    'FolderNameFillInFall': {
        'AppXWI': '',
        'AppXWF': '',
        'FilePos': 0},
    'FolderNameFillInContainer': {
        'AppXWI': '',
        'AppXWF': '',
        'FilePos': 0},
    'FolderNameFillInExport': {
        'AppXWI': '',
        'AppXWF': '',
        'FilePos': 0},
    'FolderNameFillInStandardNewCase': {
        'AppXWI': 'D:\\Vorgaenge\\' + getpass.getuser(),
        'AppXWF': 'E:\\Vorgaenge',
        'FilePos': 0},
    'FolderNameFall': {
        'AppXWI': '40_Faelle',
        'AppXWF': '20_Fall',
        'FilePos': 7878},
    'FolderNameExport': {
        'AppXWI': '60_Export',
        'AppXWF': '40_Export',
        'FilePos': 0},
    'FolderNameContainer': {
        'AppXWI': '50_Container',
        'AppXWF': '50_Container',
        'FilePos': 0},
    'FolderStandardNewCase': {
        'AppXWI': 'D:\\Vorgaenge\\' + getpass.getuser(),
        'AppXWF': 'E:\\Vorgaenge',
        'FilePos': 0},
    'FolderDictionary': {
        'AppXWI': 'App;StandardDir;Fall;Sicherungen;HashDB;HashDB2;HashDBPDNA;HashDBFuzzy;Export;Container;Temp;OpenContainer;AppVersion;erstellt',
        'AppXWF': 'App;StandardDir;Fall;Sicherungen;HashDB;HashDB2;HashDBPDNA;HashDBFuzzy;Export;Container;Temp;OpenContainer;AppVersion;erstellt',
        'FilePos': 0},
    'listREGSettings': {
        'AppXWI': ('XWLauncherID', 'XWAYSpath', 'XWFquelle', 'XWAYSexe64', 'XWAYSexe32', 'CFGFileName', 'FolderNameViewer64',
                   'FolderNameViewer32', 'FolderNameOpenContainer', 'FolderNamePlayer', 'FolderNameFavMediaPlayer',
                   'FolderNameNotepad', 'FolderNameIExplorer', 'FolderNameFavHTMLViewer', 'FolderNameExtensions',
                   'FolderNameSicherungen', 'FolderNameTemp', 'FolderNameHashDB', 'FolderNameHashDB2',
                   'FolderNameHashDBPDNA', 'FolderNameHashDBFuzzy', 'FolderNameFall', 'FolderNameExport',
                   'FolderNameContainer', 'FolderDictionary', 'FolderStandardNewCase', 'XWAlternativeSourceFolder', 'XWCaseSubfolder',
                   'XWColorSchema', 'FolderNameFillInHashDB', 'FolderNameFillInHashDB_PDNA', 'FolderNameFillInTemp', 'FolderNameFillInSicherungen',
                   'FolderNameFillInFall', 'FolderNameFillInContainer', 'FolderNameFillInExport', 'FolderNameFillInStandardNewCase', 'XWUpdateLocation',
                   'XWToolTipLocation'),
        'AppXWF': ('XWLauncherID', 'XWAYSpath', 'XWFquelle', 'XWAYSexe64', 'XWAYSexe32', 'CFGFileName', 'FolderNameViewer64',
                   'FolderNameViewer32', 'FolderNameOpenContainer', 'FolderNamePlayer', 'FolderNameFavMediaPlayer',
                   'FolderNameNotepad', 'FolderNameIExplorer', 'FolderNameFavHTMLViewer', 'FolderNameExtensions',
                   'FolderNameSicherungen', 'FolderNameTemp', 'FolderNameHashDB', 'FolderNameHashDB2',
                   'FolderNameHashDBPDNA', 'FolderNameHashDBFuzzy', 'FolderNameFall', 'FolderNameExport',
                   'FolderNameContainer', 'FolderDictionary', 'FolderStandardNewCase', 'XWAlternativeSourceFolder', 'XWCaseSubfolder',
                   'XWColorSchema', 'FolderNameFillInHashDB', 'FolderNameFillInHashDB_PDNA', 'FolderNameFillInTemp', 'FolderNameFillInSicherungen',
                   'FolderNameFillInFall', 'FolderNameFillInContainer', 'FolderNameFillInExport', 'FolderNameFillInStandardNewCase', 'XWUpdateLocation',
                   'XWToolTipLocation'),
        'FilePos': 0},
    'XWAlternativeSourceFolder': {
        'AppXWI': 'D:\\X-WAYS_Launcher\\XWI-Versionen',
        'AppXWF': 'D:\\X-WAYS_Launcher\\XWF-Versionen',
        'FilePos': 0},
    'XWComplementaryFolder': {
        'AppXWI': 'Zusatzinhalt zur Installation Investigator',
        'AppXWF': 'Zusatzinhalt zur Installation Forensics',
        'FilePos': 0},
    'XWCaseSubfolder': {
        'AppXWI': 'xwi',
        'AppXWF': 'xwf',
        'FilePos': 0},
    'XWColorSchema': {
        'AppXWI': 'dark',     # M-15-50-50     # M = monochromatic, A=analog, K=Komplementär, T=triadisch, S=aufgesplittet; Start 0-39; Helligkeit 0-99;  Sättigung 0-99
        'AppXWF': 'dark',
        'FilePos': 0},
    'XWUpdateLocation': {
        'AppXWI': '.',
        'AppXWF': '.',
        'FilePos': 0},
    'XWDokumentationLocation': {
        'AppXWI': 'XWI-Anleitung',
        'AppXWF': 'XWF-Anleitung',
        'FilePos': 0},
    'XWToolTipLocation': {
        'AppXWI': path.join(path.expanduser('~'), 'Xways-Launcher'),
        'AppXWF': path.join(path.expanduser('~'), 'Xways-Launcher'),
        'FilePos': 0}
}

gldictCase = {}

glsRegXWFPath = "SOFTWARE\X-Ways AG\X-Ways Forensics\X-Ways_Launcher"
glsRegXWIPath = "SOFTWARE\X-Ways AG\X-Ways Investigator\X-Ways_Launcher"
glsRegXWPath = "SOFTWARE\X-Ways AG\X-Ways Forensics"
glsRegXWLKey = "x-Ways_Launcher"
glboolXWAppInvestigator = False  # false -> Forensics, true -> Investigator, wird intern nach der Wahl des App-Ordners gesetzt
glsXWAYSLauncherINI = 'xwayslauncher.ini'
glsXWAYSLauncherHLP = 'xwayslauncher_hlp.htm'
glsUserAdmin = "Administrator"
glsRegRoot = winreg.HKEY_CURRENT_USER
glsRegRootString = 'HKEY_CURRENT_USER'
gliXWAppBinary = 0
glsCaseSettingsStorage = ''
glsXWAYSSourcePath = getcwd()
glPreSortFunc = None
glBoolSortReverseOrder = False
glDirectoryPrefix = 'aktuell_'


class classCase:

    def __init__(self, sCaseName, sAppXW):
        logger__init__ = getLogger('XWAYS_Launcher.classCase.__init__{sig}  '.format(sig=inspect.signature(self.__init__)))
        logger__init__.debug('Beginn __init__ für den Fall {0}'.format(sCaseName))

        self.sCaseName = sCaseName
        # generiere dictionary aus string.split()
        try:
            if sAppXW == 'AppXWF':
                self.dictCase = {i: '' for i in gldictLauncher['FolderDictionary'].get('AppXWF').split()}
                # {'Container': '', 'Export': '', 'Fall': '', 'HashDB': '', 'HashDBPDNA': '', 'HashDBFuzzy': '', 'Sicherungen': '', 'Temp': '', 'App': '', 'Pfad': '', 'OpenContainer': ''}
            elif sAppXW == 'AppXWI':
                self.dictCase = {i: '' for i in gldictLauncher['FolderDictionary'].get('AppXWI').split()}
            else:
                logger__init__.debug('unbekannte sAppXW-Bezeichnung {}, es wird das AppXWF-Format angenommen!'.format(sAppXW))
                self.dictCase = {i: '' for i in gldictLauncher['FolderDictionary'].get('AppXWF').split()}
        except Exception:
            logger__init__.exception('unbekannte sAppXW-Bezeichnung {}, Fall kann nicht angelegt werden! Return {}'.format(sAppXW, sAppXW))
            logger__init__.exception('Fehler: {}:{}'.format(exc_info()[1], exc_info()[0]))
            raise

    @classmethod
    def deco(cls):
        return deepcopy(cls)

    def set(self, key):
        logger_set = getLogger('XWAYS_Launcher.classCase.set{sig}  '.format(sig=inspect.signature(self.set)))
        logger_set.debug('Beginn set {0}'.format(''))

        try:
            if key[0] != 'Err':
                self.dictCase[key[0]] = key[1]
                logger_set.debug('Einlesen eines Schlüssels: {}'.format(key))
            else:
                logger_set.debug('Fehler beim Einlesen eines Schlüssels: "Err" {}'.format(key))
            return key
        except Exception:
            logger_set.exception('allgemeiner Key-Fehler: {!r}'.format(key))
            logger_set.exception('Fehler: {}:{}'.format(exc_info()[1], exc_info()[0]))


class XWColorSchemaDlg(QDialog):

    def __init__(self, parent=None):
        logger__init__ = getLogger('XWAYS_Launcher.XWColorSchemaDlg.__init__{sig}'.format(sig=inspect.signature(self.__init__)))
        logger__init__.debug('Beginn __init__ {0}'.format(''))

        super(XWColorSchemaDlg, self).__init__(parent)
        self.cshMethod = gldictLauncher['XWColorSchema'].get('AppXWF')
        self.spMethod = QSlider(Qt.Horizontal)
        self.intMaxColors = 36
        # M = monochromatic, A=analog, K=Komplementär, T=triadisch, S=aufgesplittet; Start 0-39; Helligkeit 0-99;  Sättigung 0-99
        self.dictMethods = {'M': 0, 'A': 1, 'K': 2, 'T': 3, 'S': 4}
        self.dictMethods_rev = {0: 'M', 1: 'A', 2: 'K', 3: 'T', 4: 'S'}
        self.spHue = QSlider(Qt.Horizontal)
        self.spLighting = QSlider(Qt.Horizontal)
        self.spSaturation = QSlider(Qt.Horizontal)
        self.labelMethodsM = QLabel('M')
        self.labelMethodsM.setToolTip('M = monochromatisch')
        self.labelMethodsA = QLabel('A')
        self.labelMethodsA.setToolTip('A = analog')
        self.labelMethodsK = QLabel('K')
        self.labelMethodsK.setToolTip('K = Komplementär')
        self.labelMethodsT = QLabel('T')
        self.labelMethodsT.setToolTip('T = triadisch')
        self.labelMethodsS = QLabel('S')
        self.labelMethodsS.setToolTip('S = aufgesplittet')
        self.labelMethodsSelected = QLabel('')
        # self.labelHue = QLabel('')
        # self.labelLighting = QLabel('')
        # self.labelSaturation = QLabel('')
        self.labelFarbSchema = QLabel('Farben-Schema')
        self.labelFarbwert = QLabel('Farbwert')
        self.labelSaettigung = QLabel('Sättigung')
        self.labelHelligkeit = QLabel('Helligkeit')
        self.build_ui()
        logger__init__.debug('Ende __init__ {0}'.format(''))

    def build_ui(self):
        logger_build_ui = getLogger('XWAYS_Launcher.XWColorSchemaDlg.build_ui{sig}'.format(sig=inspect.signature(self.build_ui)))
        logger_build_ui.debug('Beginn build_ui {0}'.format(''))
        self.setObjectName("frm_XW_ColorSchemaDialog")
        layout = QVBoxLayout(self)
        labelLayout = QHBoxLayout(self)
        labelLayout.addWidget(self.labelFarbSchema, alignment=Qt.AlignCenter)
        labelLayout.addWidget(self.labelMethodsSelected, alignment=Qt.AlignLeft)
        layout.addLayout(labelLayout)

        # self.labelMethods.setFrameStyle(1)
        # self.labelMethods.setAlignment(Qt.AlignCenter)
        # self.labelMethods.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        # labelLayout0.addStretch(1)
        self.labelMethodsM.setAlignment(Qt.AlignCenter)
        self.labelMethodsA.setAlignment(Qt.AlignCenter)
        self.labelMethodsK.setAlignment(Qt.AlignCenter)
        self.labelMethodsT.setAlignment(Qt.AlignCenter)
        self.labelMethodsS.setAlignment(Qt.AlignCenter)
        labelLayout0 = QHBoxLayout(self)
        labelLayout0.addWidget(self.labelMethodsM, alignment=Qt.AlignCenter)
        labelLayout0.addWidget(self.labelMethodsA, alignment=Qt.AlignCenter)
        labelLayout0.addWidget(self.labelMethodsK, alignment=Qt.AlignCenter)
        labelLayout0.addWidget(self.labelMethodsT, alignment=Qt.AlignCenter)
        labelLayout0.addWidget(self.labelMethodsS, alignment=Qt.AlignCenter)
        layout.addLayout(labelLayout0)

        self.spMethod.setMinimum(0)
        self.spMethod.setMaximum(len(self.dictMethods)-1)
        self.spMethod.setValue(self.dictMethods['M'])
        self.spMethod.setToolTip(self.dictMethods_rev[self.spMethod.sliderPosition()])
        self.spMethod.valueChanged.connect(self.spMethodValueChanged)
        self.spMethod.setTickPosition(QSlider.TicksAbove)
        self.spMethod.setTickInterval(1)
        labelLayout1 = QHBoxLayout(self)
        labelLayout1.setContentsMargins(40, 0, 30, 0)
        labelLayout1.addWidget(self.spMethod)
        layout.addLayout(labelLayout1)

        labelLayout2 = QHBoxLayout(self)
        # self.labelFarbwert = QLabel('Farbwert')
        self.spHue = QSlider(Qt.Horizontal)
        self.spHue.setMinimum(0)
        self.spHue.setMaximum(self.intMaxColors)
        self.spHue.setValue(15)     # self.cshMethod
        self.spHue.setTickPosition(QSlider.TicksAbove)
        self.spHue.setTickInterval(1)
        self.spHue.setToolTip(str(self.spHue.value()))
        self.spHue.valueChanged.connect(self.spHueValueChanged)
        # self.labelHue = QLabel('')
        labelLayout2a = QHBoxLayout(self)
        labelLayout2a.addWidget(self.labelFarbwert)
        # labelLayout2a.addWidget(self.labelHue)
        layout.addLayout(labelLayout2a)
        labelLayout2.addWidget(self.spHue)
        labelLayout2.setContentsMargins(40, 0, 30, 0)
        layout.addLayout(labelLayout2)

        labelLayout3 = QHBoxLayout(self)
        # self.labelHelligkeit = QLabel('Helligkeit')
        self.spLighting = QSlider(Qt.Horizontal)
        self.spLighting.setMinimum(0)
        self.spLighting.setMaximum(100)
        self.spLighting.setValue(50)        # self.cshMethod
        self.spLighting.setTickPosition(QSlider.TicksAbove)
        self.spLighting.setTickInterval(1)
        self.spLighting.setToolTip(str(self.spLighting.sliderPosition()))
        self.spLighting.valueChanged.connect(self.spLightingValueChanged)
        # self.labelLighting = QLabel('')
        labelLayout3a = QHBoxLayout(self)
        labelLayout3a.addWidget(self.labelHelligkeit)
        # labelLayout3a.addWidget(self.labelLighting)
        layout.addLayout(labelLayout3a)
        labelLayout3.addWidget(self.spLighting)
        labelLayout3.setContentsMargins(40, 0, 30, 0)
        layout.addLayout(labelLayout3)

        labelLayout4 = QHBoxLayout()
        # self.labelSaettigung = QLabel('Sättigung')
        self.spSaturation = QSlider(Qt.Horizontal)
        self.spSaturation.setMinimum(0)
        self.spSaturation.setMaximum(100)
        self.spSaturation.setValue(50)          # self.cshMethod
        self.spSaturation.setTickPosition(QSlider.TicksAbove)
        self.spSaturation.setTickInterval(1)
        self.spSaturation.setToolTip(str(self.spSaturation.sliderPosition()))
        self.spSaturation.valueChanged.connect(self.spSaturationValueChanged)
        # self.labelSaturation = QLabel('')
        labelLayout4a = QHBoxLayout(self)
        labelLayout4a.addWidget(self.labelSaettigung)
        # labelLayout4a.addWidget(self.labelSaturation)
        layout.addLayout(labelLayout4a)
        labelLayout4.addWidget(self.spSaturation)
        labelLayout4.setContentsMargins(40, 0, 30, 0)
        layout.addLayout(labelLayout4)
        layout.setContentsMargins(40, 0, 30, 40)

        self.setLayout(layout)
        logger_build_ui.debug('Ende build_ui {0}'.format(''))
        # self.setGeometry(500, 500, 500, 150)
        self.setWindowTitle('Farbeinstellung')

    def spMethodValueChanged(self, value):
        logger_spMethodValueChanged = getLogger('.spMethodValueChanged{sig}'.format(sig=inspect.signature(self.spMethodValueChanged)))
        logger_spMethodValueChanged.debug('Beginn spMethodValueChanged {0}'.format(''))
        self.spMethod.setToolTip(self.dictMethods_rev[self.spMethod.sliderPosition()])
        self.labelMethodsSelected.setText(self.dictMethods_rev[value])

    def spHueValueChanged(self, value):
        logger_spHueValueChanged = getLogger('.spHueValueChanged{sig}'.format(sig=inspect.signature(self.spHueValueChanged)))
        logger_spHueValueChanged.debug('Beginn spHueValueChanged {0}'.format(''))
        self.spHue.setToolTip(str(self.spHue.value()))
        self.labelHue.setText(str(value))

    def spLightingValueChanged(self, value):
        logger_spLightingValueChanged = getLogger('.spLightingValueChanged{sig}'.format(sig=inspect.signature(self.spLightingValueChanged)))
        logger_spLightingValueChanged.debug('Beginn spLightingValueChanged {0}'.format(''))
        self.spLighting.setToolTip(str(value))   # self.spLighting.setToolTip(str(self.spLighting.sliderPosition()))
        self.labelLighting.setText(str(value))

    def spSaturationValueChanged(self, value):
        logger_spSaturationValueChanged = getLogger('.spSaturationValueChanged{sig}'.format(sig=inspect.signature(self.spSaturationValueChanged)))
        logger_spSaturationValueChanged.debug('Beginn spSaturationValueChanged {0}'.format(''))
        self.spSaturation.setToolTip(str(value))   # self.spSaturation.setToolTip(str(self.spSaturation.sliderPosition()))
        self.labelSaturation.setText(str(value))


class XWFileCopyProgress(QDialog):  # INFO: fertig

    def __init__(self, parent, src=None, dest=None):
        # logger__init__ = getLogger('XWAYS_Launcher.XWFileCopyProgress.__init__{sig}  '.format(sig=inspect.signature(self.__init__)))
        # logger__init__.debug('Beginn __init__ {0}'.format(''))
        super(XWFileCopyProgress, self).__init__(parent)
        self._want_to_close = False
        self.src = src
        self.dest = dest
        self.pb = QProgressBar()
        self.auto_start_timer = QTimer()
        self.lbl_src = QLabel('Quelle: ' + self.src)
        self.lbl_dest = QLabel('Ziel: ' + self.dest)
        self.lbl_info = QLabel('INFO: mal kurz die Luft anhalten... Danke!')
        # self.returnFileCopyStatus = 0  # Initialwert: 0, Vorgang dauert an: 1++, Vorgang abgeschlossen: ==filecount
        self.build_ui()

    def build_ui(self):
        logger_build_ui = getLogger('XWAYS_Launcher.XWFileCopyProgress.build_ui{sig}'.format(sig=inspect.signature(self.build_ui)))
        logger_build_ui.debug('Beginn build_ui {0}'.format(''))
        logger_build_ui.debug('Beginne Kopiervorgang mit Quelle: {0} \nund Ziel: {1}'.format(self.src, self.dest))
        self.setObjectName("frm_XW_FCP")
        # self.setStyleSheet(open("styles.qss", "r").read())
        hbox = QVBoxLayout()
        self.pb.setMinimum(0)
        self.pb.setMaximum(100)
        self.pb.setValue(0)
        self.pb.setGeometry(250, 80, 250, 20)
        hbox.addWidget(self.lbl_src, alignment=Qt.AlignLeft)
        hbox.addWidget(self.lbl_dest, alignment=Qt.AlignLeft)
        hbox.addWidget(self.lbl_info, alignment=Qt.AlignLeft)
        hbox.addWidget(self.pb, alignment=Qt.AlignVCenter)
        self.setLayout(hbox)
        self.setWindowTitle('Dateien werden kopiert...')

        try:
            self.auto_start_timer.singleShot(20, lambda: self.copyFilesWithProgress(self.src, self.dest, self.progress, self.copydone))
        except Exception:
            logger_build_ui.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            self.lbl_info.setText('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
        return -1   # self.returnFileCopyStatus

    def closeEvent(self, closeEvent):
        if self._want_to_close:
            super(XWFileCopyProgress, self).closeEvent(closeEvent)
        else:
            closeEvent.ignore()
            self.lbl_info.setText('Abbrechen ist nicht, ist besser so, glaub\'s mir!'.format(''))   # exc_info()[0], exc_info()[1]

    def progress(self, done, total):
        logger_progress = getLogger('XWAYS_Launcher.XWFileCopyProgress.progress{sig}'.format(sig=inspect.signature(self.progress)))
        progress = int(round((done / float(total)) * 100))
        try:
            self.pb.setValue(progress)
        except RuntimeError as re:
            logger_progress.debug('Ein Laufzeitfehler ist aufgetreten: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
            return re.args  # self.returnFileCopyStatus , Fehler?
        except Exception:
            logger_progress.exception('Ein Fehler ist aufgetreten: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
            self.lbl_info.setText('INFO: das war wohl nichts... noch Mal!!')
            # pass
        QApplication.processEvents()

    def copydone(self):
        logger_copydone = getLogger('XWAYS_Launcher.XWFileCopyProgress.copydone{sig}'.format(sig=inspect.signature(self.copydone)))
        logger_copydone.debug('Beginn copydone {0}'.format(''))
        self.pb.setValue(100)
        logger_copydone.debug('Ende copydone: self.pb.value() =  {0}'.format(self.pb.value()))
        self.lbl_info.setText('INFO: wieder ausatmen... Fertig!')
        self._want_to_close = True
        logger_copydone.debug('FileCopyProgress wurde erfolgreich ausgeführt: \nsrc Pfad: {}\ndst Pfad: {}'.format(self.src, self.dest))

        self.close()

    def countfiles(self, _dir):
        logger_countfiles = getLogger('XWAYS_Launcher.XWFileCopyProgress.Ui_frm_XW_DirCopyProgress.countfiles{sig}'.format(sig=inspect.signature(self.countfiles)))
        logger_countfiles.debug('Beginn countfiles {0}'.format(''))

        files = []
        if path.isdir(_dir):
            for filepath, dirs, filenames in walk(_dir):
                files.extend(filenames)
        logger_countfiles.debug('return counted files to copy: {}'.format(len(files)))
        # self.parent().tempVar = len(files)
        return len(files)

    def makeMyDirs(self, dest):
        logger_makeMyDirs = getLogger('.makeMyDirs{sig}'.format(sig=inspect.signature(self.makeMyDirs)))
        logger_makeMyDirs.debug('Beginn makeMyDirs {0}'.format(''))

        try:
            if not path.exists(dest):
                makedirs(dest)
        except OSError as err:
            self.lbl_info.setText('Verzeichnis konnte nicht angelegt werden: : {0}'.format(err))
            logger_makeMyDirs.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            return False
        return True

    @pyqtSlot()
    def copyFilesWithProgress(self, src, dest, callback_progress, callback_copydone):
        logger_copyFilesWithProgress = getLogger('XWAYS_Launcher.XWFileCopyProgress.copyFilesWithProgress{sig}  '.format(sig=inspect.signature(self.copyFilesWithProgress)))
        logger_copyFilesWithProgress.debug('Beginn copyFilesWithProgress {0}'.format(''))
        numFiles = 0
        try:
            numFiles = self.countfiles(src)
        except Exception as err:
            logger_copyFilesWithProgress.exception('Ein Fehler ist in copyFilesWithProgress aufgetreten: {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            self.lbl_info.setText('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            return False

        if numFiles > 0:
            if self.makeMyDirs(dest):
                numCopied = 0
                for pathPath, dirs, filenames in walk(src):
                    for directory in dirs:
                        destDir = pathPath.replace(src, dest)
                        self.makeMyDirs(path.join(destDir, directory))
                    for sfile in filenames:
                        srcFile = path.join(pathPath, sfile)
                        destFile = path.join(pathPath.replace(src, dest), sfile)
                        copyfile(srcFile, destFile)
                        numCopied += 1
                        # if numCopied == self.returnFileCopyStatus +1:
                        #     self.returnFileCopyStatus += 1  # Vorgang dauert an
                        callback_progress(numCopied, numFiles)
                callback_copydone()
            else:
                logger_copyFilesWithProgress.debug('Fehler in copyFilesWithProgress: numFiles in src = {}, src:{}, dest: {}'.format(numFiles, src, dest))
                self.statusbar.showMessage('Fehler beim Kopieren: Anzahl der Dateien gefunden {0};  Quelle : {1}; Ziel: {2}'.format(numFiles, src.replace('\\\\', '\\'), dest.replace('\\\\', '\\')), 4000)
                return False
        else:
            logger_copyFilesWithProgress.debug('Fehler in copyFilesWithProgress: numFiles in src = {}, src:{}, dest: {}'.format(numFiles, src, dest))
            self.statusbar.showMessage('Fehler beim Kopieren: Anzahl der Dateien gefunden {0};  Quelle : {1}; Ziel: {2}'.format(numFiles, src.replace('\\\\', '\\'), dest.replace('\\\\', '\\')), 4000)
            return False
        return True


class XWNewCase(Ui_frm_XW_NewCase):
    close_signal = pyqtSignal()

    def __init__(self, *args):
        logger___init__ = getLogger('XWAYS_Launcher.XW_NewCase.__init__{sig}'.format(sig=inspect.signature(self.__init__)))
        logger___init__.debug('Beginn __init__ {0}'.format(''))
        super(XWNewCase, self).__init__(*args)
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.intBinary = 0
        self.setupUi(glsXWAYSSourcePath, gldictLauncher)
        self.pbtnCaseCreate_abbrechen.clicked.connect(self.handleCloseButton)
        self.pbtnCaseCreate_ausfuehren.clicked.connect(self.handleCreateButton)
        self.comboBox.activated[str].connect(self.combobox_change)
        self.comboBox.currentIndexChanged.connect(self.combobox_change)
        # self.comboBox.currentTextChanged.connect(self.combobox_change)
        self.toolButtonStandardDir.clicked.connect(self.toolButtonStandardDir_pressed)
        self.toolButtonCtrFile.clicked.connect(self.toolButtonCtrFile_pressed)
        self.toolButtonContainer.clicked.connect(self.toolButtonContainer_pressed)
        self.toolButtonExport.clicked.connect(self.toolButtonExport_pressed)
        self.toolButtonFall.clicked.connect(self.toolButtonFall_pressed)
        self.toolButtonHashDB.clicked.connect(self.toolButtonHashDB_pressed)
        self.toolButtonHashDBPDNA.clicked.connect(self.toolButtonHashDBPDNA_pressed)
        self.toolButtonSicherungen.clicked.connect(self.toolButtonSicherungen_pressed)
        self.toolButtonTemp.clicked.connect(self.toolButtonTMP_pressed)
        self.lineEditStandardDir.textChanged.connect(self.lineEditStandardDir_changed)
        self.lineEditFallbezeichnung.textChanged.connect(self.lineEditFallbezeichnung_changed)
        self.lineEditRegExp = QRegExp(r'^(?:[c-zC-Z]\:|\\|[.]+)(\\[a-zA-ZÄÜÖäüö_\-\s0-9\.]+)+')
        # \.\./[^\r\n]   #  ?:[\w]\:|\\)(\\[a-z_\-\s0-9\.] # ^(?:[c-zC-Z]\:|\\)(\\[a-zA-Z_\-\s0-9\.]+)+ # ^(?:[a-zA-Z]\:|\\\\[\S\.]+\\[\S.$]+)\\(?:[\S]+\\)*\w([\w.-])+$
        input_validator = QRegExpValidator(self.lineEditRegExp)
        self.lineEditStandardDir.setValidator(input_validator)
        self.lineEditContainer.setValidator(input_validator)
        self.lineEditExport.setValidator(input_validator)
        self.lineEditFall.setValidator(input_validator)
        self.lineEditHashDB.setValidator(input_validator)
        self.lineEditHashDBPDNA.setValidator(input_validator)
        self.lineEditSicherungen.setValidator(input_validator)
        self.lineEditTemp.setValidator(input_validator)
        self.lineEditFallbezeichnung.setValidator(QRegExpValidator(QRegExp(r'^(?:[a-zA-ZÄÜÖäüö_\-\s0-9]+)+')))
        self.lineEditStandardDir.textChanged.connect(self.check_state)
        self.lineEditContainer.textChanged.connect(self.check_state)
        self.lineEditExport.textChanged.connect(self.check_state)
        self.lineEditFall.textChanged.connect(self.check_state)
        self.lineEditHashDB.textChanged.connect(self.check_state)
        self.lineEditHashDBPDNA.textChanged.connect(self.check_state)
        self.lineEditSicherungen.textChanged.connect(self.check_state)
        self.lineEditTemp.textChanged.connect(self.check_state)
        self.checkBoxStart.stateChanged.connect(lambda:self.checkBox_changed(self.checkBoxStart))
        self.textEditCtrFile.clear()
        # self.comboBox.model().tableViewComboList.setColumnWidth(0, 80)
        # self.comboBox.model().tableViewComboList.setColumnWidth(1, 250)
        self.windowXWFCP = None
        self.lineEditStandardDir_dirty = True
        # self.tempVar = 0
        logger___init__.debug('I am the popup NewCaseDialog {0}. My parent is:  {1}'.format(self, self.nativeParentWidget()))
        logger___init__.debug('Ende __init__ {}'.format(''))

    def closeEvent(self, event):
        logger_closeEvent = getLogger('XWAYS_Launcher.XW_NewCase.closeEvent{sig}'.format(sig=inspect.signature(self.closeEvent)))
        logger_closeEvent.debug('Kopiervorgang beendet mit {}, NewCaseDialog wird zurückgesetzt, Kopierfenster zerstört.'.format(event))
        self.lineEditStandardDir.clear()
        self.lineEditFallbezeichnung.clear()
        # self.windowXWFCP.pb = None
        # self.windowXWFCP = None
        self.on_close()

    def makeMyDirs(self, dest):
        logger_makeMyDirs = getLogger('.makeMyDirs{sig}'.format(sig=inspect.signature(self.makeMyDirs)))
        logger_makeMyDirs.debug('Beginn makeMyDirs: {0}'.format(dest))

        if not path.exists(dest):
            try:
                makedirs(dest)
            except:
                logger_makeMyDirs.exception('Ein Fehler ist aufgetreten: FileCopyProgress: {0}:{1}'.format(exc_info()[1], exc_info()[0]))

    def on_close(self):
        """ Perform on close stuff here """
        self.close_signal.emit()

    def handleCloseButton(self):  # INFO: fertig
        logger_handleCloseButton = getLogger('XWAYS_Launcher.XW_NewCase.handleCloseButton{sig}'.format(sig=inspect.signature(self.handleCloseButton)))
        logger_handleCloseButton.debug('Beginn handleCloseButton {0}'.format(''))
        self.close()

    def handleCreateButton(self):  # TODO:
        logger_handleCreateButton = getLogger('XWAYS_Launcher.XW_NewCase.XW_NewCase.handleCreateButton{sig}'.format(sig=inspect.signature(self.handleCreateButton)))
        logger_handleCreateButton.debug('Beginn handleCreateButton {0}'.format(''))

        if not self.lineEditFallbezeichnung.text():
            self.statusbar.showMessage('Fallbezeichnung nicht angegeben, Ausführung unterbrochen!', 4000)
            logger_handleCreateButton.debug('Fallbezeichnung nicht angegeben, Ausführung unterbrochen! {}'.format(''))
            return
        if self.lineEditStandardDir.hasAcceptableInput():
            if path.isdir(path.abspath(path.splitdrive(self.lineEditStandardDir.text())[0])):
                destination = path.join(self.lineEditStandardDir.text(), gldictLauncher['XWAYSpath'].get(self.strXWApp))
            else:
                self.statusbar.showMessage('Ziellaufwerk nicht vorhanden, Ausführung unterbrochen!', 4000)
                logger_handleCreateButton.debug('Ziellaufwerk nicht vorhanden, Ausführung unterbrochen! {}'.format(''))
                return
        else:
            self.statusbar.showMessage('Programmverzeichnis nicht benannt, Ausführung unterbrochen!', 4000)
            logger_handleCreateButton.debug('Programmverzeichnis nicht angegeben, Ausführung unterbrochen! {}'.format(''))
            return
        if not self.lineEditExport.hasAcceptableInput():
            self.statusbar.showMessage('Export-Verzeichnis nicht benannt, Ausführung unterbrochen!', 4000)
            logger_handleCreateButton.debug('Export-Verzeichnis nicht angegeben, Ausführung unterbrochen! {}'.format(''))
            return
        if not self.lineEditFall.hasAcceptableInput():
            self.statusbar.showMessage('Fall-Verzeichnis nicht benannt, Ausführung unterbrochen!', 4000)
            logger_handleCreateButton.debug('Fall-Verzeichnis nicht angegeben, Ausführung unterbrochen! {}'.format(''))
            return
        if not self.lineEditHashDB.hasAcceptableInput():
            self.statusbar.showMessage('HashDB-Verzeichnis nicht benannt, Ausführung unterbrochen!', 4000)
            logger_handleCreateButton.debug('HashDB-Verzeichnis nicht angegeben, Ausführung unterbrochen! {}'.format(''))
            return
        if not self.lineEditHashDBPDNA.hasAcceptableInput():
            self.statusbar.showMessage('PDNA-Verzeichnis nicht benannt, Ausführung unterbrochen!', 4000)
            logger_handleCreateButton.debug('PDNA-Verzeichnis nicht angegeben, Ausführung unterbrochen! {}'.format(''))
            return
        if not self.lineEditSicherungen.hasAcceptableInput():
            self.statusbar.showMessage('Sicherungen-Verzeichnis nicht benannt, Ausführung unterbrochen!', 4000)
            logger_handleCreateButton.debug('Sicherungen-Verzeichnis nicht angegeben, Ausführung unterbrochen! {}'.format(''))
            return
        if not self.lineEditTemp.hasAcceptableInput():
            self.statusbar.showMessage('Temp-Verzeichnis nicht benannt, Ausführung unterbrochen!', 4000)
            logger_handleCreateButton.debug('Temp-Verzeichnis nicht angegeben, Ausführung unterbrochen! {}'.format(''))
            return
        if not self.lineEditContainer.hasAcceptableInput():
            self.statusbar.showMessage('Container-Verzeichnis nicht benannt, Ausführung unterbrochen!', 4000)
            logger_handleCreateButton.debug('Container-Verzeichnis nicht angegeben, Ausführung unterbrochen! {}'.format(''))
            return

        try:            # INFO: kopieren der Haupt-Programmverzeichnisse
            logger_handleCreateButton.debug('XWFileCopyProgress destination: {0}'.format(destination))
            logger_handleCreateButton.debug('XWFileCopyProgress src: {0}'.format(path.join(self.sXWDirpath, self.comboBox.currentText())))

            if self.intBinary == 0:
                if 'x32' in self.comboBox.currentText():
                    self.intBinary = 32
                else:
                    self.intBinary = 64

            self.windowXWFCP = XWFileCopyProgress(self, src=path.join(self.sXWDirpath, self.comboBox.currentText()), dest=destination)        # , gldictLauncher['XWFquelle'].get(self.strXWApp)
            self.windowXWFCP.exec_()
            # logger_handleCreateButton.debug('FileCopyProgress wurde erfolgreich ausgeführt: \nsrc Pfad: {}\ndst Pfad: {}'.format(path.join(self.sXWDirpath, self.comboBox.currentText()), destination))
            #############################
            # if self.windowXWFCP.returnFileCopyStatus != 100:
            #     logger_handleCreateButton.debug('Kopiervorgang unterbrochen bei {}%, NewCaseDialog wird zurückgesetzt, Kopierfenster manuell zerstört.'.format(self.tempVar))
            #     self.lineEditStandardDir.clear()
            #     self.lineEditFallbezeichnung.clear()
            #     self.windowXWFCP.pb = None
            #     self.windowXWFCP = None
            #     return copyReturn
            ####################
            # # INFO: hier werden die MPlayer und viewer Verzeichnisse für die Investigator Installation in die Zielverzeichnisse verschoben! Hier auf die Programmversion achten!!!!
            # if self.strXWApp == 'AppXWI' and self.sXWDirpath == path.join(glsXWAYSSourcePath, gldictLauncher['XWFquelle'].get(self.strXWApp)):
            #     strSrc = path.join(destination, gldictLauncher['FolderNameViewer32'].get(self.strXWApp).replace('..\\', ''))
            #     # INFO: hier nur der Viewer32, da der 64 im Verzeichnis vom 32 liegt und mitverschoben wird
            #     strDest = self.lineEditStandardDir.text()
            #
            #     if path.exists(path.join(strDest, gldictLauncher['FolderNameViewer32'].get(self.strXWApp).replace('..\\', ''))):
            #         try:
            #             rmtree(path.join(strDest, gldictLauncher['FolderNameViewer32'].get(self.strXWApp).replace('..\\', '')))
            #         except IOError:
            #             logger_handleCreateButton.critical('Bereinigung Viewerverzeichnis: IOError aufgetreten: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
            #             raise
            #     try:
            #         move(strSrc, strDest)  # INFO: mit Überschreiben des evtl. bereits vorhandenen Zielverzeichnisses
            #     except IOError:
            #         logger_handleCreateButton.critical('Kopie Viewerverzeichnis: IOError aufgetreten: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
            #         raise
            #     logger_handleCreateButton.debug('Viewer wurde erfolgreich kopiert: \nsrc Pfad: {}\ndst Pfad: {}'.format(strSrc, strDest))
            #     # move(path.join(destination, gldictLauncher['FolderNameViewer32'].get(self.strXWApp).replace('..\\', '')), self.lineEditStandardDir.text())
            #
            #     if path.exists(path.join(strDest, gldictLauncher['FolderNamePlayer'].get(self.strXWApp).replace('..\\', ''))):
            #         try:
            #             rmtree(path.join(strDest, path.dirname(gldictLauncher['FolderNamePlayer'].get(self.strXWApp).replace('..\\', ''))))
            #         except IOError:
            #             logger_handleCreateButton.critical('Bereinigung Playerverzeichnis: IOError aufgetreten: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
            #             raise
            #
            #     strSrc = path.join(destination, path.dirname(gldictLauncher['FolderNamePlayer'].get(self.strXWApp).replace('..\\', '')))
            #     strDest = path.join(self.lineEditStandardDir.text())
            #     try:
            #         move(strSrc, strDest)   # INFO: mit Überschreiben des evtl. bereits vorhandenen Zielverzeichnisses
            #     except IOError:
            #         logger_handleCreateButton.critical('Kopie Playerverzeichnis: IOError aufgetreten: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
            #         raise
            #     # move(path.join(destination, path.dirname(gldictLauncher['FolderNamePlayer'].get(self.strXWApp).replace('..\\', ''))), self.lineEditStandardDir.text())    # .replace('\\mplayer.exe', '')
            #     logger_handleCreateButton.debug('Player wurde erfolgreich kopiert: \nsrc Pfad: {}\ndst Pfad: {}'.format(strSrc, strDest))
        # except WindowsError as err:
            # logger_handleCreateButton.critical('FileCopyProgress WindowsError aufgetreten: {}\nsrc Pfad: {}\ndst Pfad: {}'.format(err, path.join(self.sXWDirpath, self.comboBox.currentText()), destination))
            # return
        # except IOError as err:
            # logger_handleCreateButton.critical('FileCopyProgress IOError aufgetreten: {}\nsrc Pfad: {}\ndst Pfad: {}'.format(err, path.join(self.sXWDirpath, self.comboBox.currentText()), destination))
            # return
        except Exception:
            msg = QMessageBox()
            msg.setWindowIcon(self.icon)
            # self.icon.addPixmap(QPixmap("./xways.ico"), QIcon.Normal, QIcon.Off)
            # msg.setWindowIcon(self.icon)
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle("Fehler bei der Dateiübertragung")
            msg.setText("FileCopyProgress Fehler aufgetreten: {0}:{1}".format(exc_info()[1], exc_info()[0]))
            logger_handleCreateButton.exception('Ein Fehler ist aufgetreten: FileCopyProgress: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
            logger_handleCreateButton.critical('FileCopyProgress src Pfad: {}; dst Pfad: {}'.format(path.join(self.sXWDirpath, self.comboBox.currentText()), destination))
            msg.setInformativeText("Beim Kopieren der X-Ways Programmdateien ist ein Fehler aufgetreten: {0}\nBitte die Pfade überprüfen! \nsrc Pfad: {1}; \ndst Pfad: {2}".format(exc_info()[1], path.join(self.sXWDirpath, self.comboBox.currentText()), destination))
            msg.setDetailedText('Quell-Pfad: {}\nZiel-Pfad: {}'.format(path.join(self.sXWDirpath, self.comboBox.currentText()), destination))
            msg.setStandardButtons(QMessageBox.Ok)
            retVal = msg.exec()
            return -1

        # INFO: hier werden die MPlayer und viewer Verzeichnisse für die Investigator Installation in die Zielverzeichnisse verschoben! Hier auf die Programmversion achten!!!!
        if self.strXWApp == 'AppXWI' and self.sXWDirpath == path.join(glsXWAYSSourcePath, gldictLauncher['XWFquelle'].get(self.strXWApp)):
            # destination = path.join(self.lineEditStandardDir.text(), gldictLauncher['XWAYSpath'].get(self.strXWApp))
            strSrc = path.join(destination, gldictLauncher['FolderNameViewer32'].get(self.strXWApp).replace('..\\', ''))
            # INFO: hier nur der Viewer32, da der 64 im Verzeichnis vom 32 liegt und mitverschoben wird
            strDest = self.lineEditStandardDir.text()

            if path.exists(path.join(strDest, gldictLauncher['FolderNameViewer32'].get(self.strXWApp).replace('..\\', ''))):
                try:
                    logger_handleCreateButton.debug('bestehendes/altes Viewer-Verzeichnis festgestellt, versuche zu löschen... {0}\\{1}'.format(strDest, gldictLauncher['FolderNameViewer32'].get(self.strXWApp).replace('..\\', '')))
                    rmtree(path.join(strDest, gldictLauncher['FolderNameViewer32'].get(self.strXWApp).replace('..\\', '')))
                    logger_handleCreateButton.debug('bestehendes/altes Viewer-Verzeichnis wurde gelöscht...'.format())
                except:
                    logger_handleCreateButton.exception('Bereinigung Viewerverzeichnis: IOError aufgetreten: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
                    pass
            try:
                logger_handleCreateButton.debug('Versuch das Viewer-Verzeichnis in das Programmverzeichnis zu verschieben: {0}'.format(strSrc))
                move(strSrc, strDest)  # INFO: mit Überschreiben des evtl. bereits vorhandenen Zielverzeichnisses
                logger_handleCreateButton.debug('Viewer-Verzeichnis in das Programmverzeichnis verschoben: {0}'.format(strDest))
            except:
                logger_handleCreateButton.exception('Kopie Viewerverzeichnis: IOError aufgetreten: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
                pass
            logger_handleCreateButton.debug('Viewer wurde erfolgreich kopiert: \nsrc Pfad: {}\ndst Pfad: {}'.format(strSrc, strDest))
            # move(path.join(destination, gldictLauncher['FolderNameViewer32'].get(self.strXWApp).replace('..\\', '')), self.lineEditStandardDir.text())

            strSrc = path.join(destination, path.dirname(gldictLauncher['FolderNamePlayer'].get(self.strXWApp).replace('..\\', '')))
            # strDest = path.dirname(self.lineEditStandardDir.text())
            if path.exists(path.join(strDest, path.dirname(gldictLauncher['FolderNamePlayer'].get(self.strXWApp).replace('..\\', '')))): # INFO: path.dirname da dateiname noch dran hängt
                try:
                    logger_handleCreateButton.debug('bestehendes/altes Player-Verzeichnis festgestellt, versuche zu löschen... {0}\\{1}'.format(strDest, gldictLauncher['FolderNamePlayer'].get(self.strXWApp).replace('..\\', '')))
                    rmtree(path.join(strDest, path.dirname(gldictLauncher['FolderNamePlayer'].get(self.strXWApp).replace('..\\', ''))))
                    logger_handleCreateButton.debug('bestehendes/altes Player-Verzeichnis wurde gelöscht...'.format())
                except:
                    logger_handleCreateButton.exception('Bereinigung Playerverzeichnis: IOError aufgetreten: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
                    pass
            try:
                logger_handleCreateButton.debug('Versuch das Player-Verzeichnis in das Programmverzeichnis zu verschieben: {0}'.format(strSrc))
                move(strSrc, strDest)  # INFO: mit Überschreiben des evtl. bereits vorhandenen Zielverzeichnisses
                logger_handleCreateButton.debug('Player-Verzeichnis in das Programmverzeichnis verschoben: {0}'.format(strDest))
            except:
                logger_handleCreateButton.exception('Kopie Playerverzeichnis: IOError aufgetreten: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
                pass
            # move(path.join(destination, path.dirname(gldictLauncher['FolderNamePlayer'].get(self.strXWApp).replace('..\\', ''))), self.lineEditStandardDir.text())    # .replace('\\mplayer.exe', '')
            logger_handleCreateButton.debug('Player wurde erfolgreich kopiert: \nsrc Pfad: {}\ndst Pfad: {}'.format(strSrc, strDest))

        if self.sXWDirpath != path.join(glsXWAYSSourcePath, gldictLauncher['XWFquelle'].get(self.strXWApp)):
            # INFO: wenn aus dem alternativen pfad kopiert wurde, dann noch zusätzlich den Zusatzpfad drüber kopieren: XWComplementaryFolder
            try:
                logger_handleCreateButton.debug('XWFileCopyProgress destination für XWComplementaryFolder: {0}'.format(destination))
                logger_handleCreateButton.debug('XWFileCopyProgress src: {0}'.format(path.join(glsXWAYSSourcePath, gldictLauncher['XWFquelle'].get(self.strXWApp), gldictLauncher['XWComplementaryFolder'].get(self.strXWApp))))

                self.windowXWFCP = XWFileCopyProgress(self, src=path.join(glsXWAYSSourcePath, gldictLauncher['XWFquelle'].get(self.strXWApp), gldictLauncher['XWComplementaryFolder'].get(self.strXWApp)), dest=destination)
                self.windowXWFCP.exec_()
                if self.strXWApp == 'AppXWI':
                    move(path.join(destination, gldictLauncher['FolderNameViewer32'].get(self.strXWApp).replace('..\\', '')), self.lineEditStandardDir.text())
                    move(path.join(destination, path.dirname(gldictLauncher['FolderNamePlayer'].get(self.strXWApp).replace('..\\', ''))), self.lineEditStandardDir.text())  # .replace('\\mplayer.exe', '')
                logger_handleCreateButton.debug('FileCopyProgress wurde erfolgreich ausgeführt: \nsrc Pfad: {}\ndst Pfad: {}'.format(path.join(self.sXWDirpath, self.comboBox.currentText()), destination))
            except WindowsError as err:
                logger_handleCreateButton.critical('FileCopyProgress WindowsError aufgetreten: {}\nsrc Pfad: {}\ndst Pfad: {}'.format(err, path.join(self.sXWDirpath, self.comboBox.currentText()), destination))
                raise
            except IOError as err:
                logger_handleCreateButton.critical('FileCopyProgress IOError aufgetreten: {}\nsrc Pfad: {}\ndst Pfad: {}'.format(err, path.join(self.sXWDirpath, self.comboBox.currentText()), destination))
                raise

        # INFO: konfigurierte Verzeichnisse werden angelegt
        returnValue = self.CreateDirectorySkeleton(destination, self.strXWApp)
        if returnValue == -1:
            logger_handleCreateButton.debug('CreateDirectorySkeleton konnte nicht ausgeführt werden ReturnCode {}'.format(returnValue))
            return -1
        # fileConfigFile = path.join(self.sXWDirpath, gldictLauncher['XWFquelle'].get(self.strXWApp), self.comboBox.currentText(), gldictLauncher['CFGFileName'].get(self.strXWApp))  # .replace('.\\', '\\'))
        # INFO: die WinHex.cfg wird an die aktuellen Einstellungen angepasst
        fileDestination = path.join(destination, gldictLauncher['CFGFileName'].get(self.strXWApp))      # .replace('.\\', '\\'))
        # INFO: Verwendung der 32bit oder 64bit Programmversion, wichtig für XWAYS-Config ab 19.7
        gliXWAppBinary = self.intBinary
        logger_handleCreateButton.debug('Binärversion der EXE: gliXWAppBinary = self.intBinary -> {}'.format(self.intBinary))
        try:
            defWriteConfigToBinFile(fileDestination, gldictLauncher, gldictCase[self.lineEditFallbezeichnung.text()], self.strXWApp, self.intBinary)  # fileConfigFile,
        except Exception:
            logger_handleCreateButton.exception(
                'Fehler: defWriteConfigToBinFile: fileDestination: {}, gldictLauncher, gldictCase[self.lineEditFallbezeichnung.text()]: {}, self.strXWApp: {}'.format(
                    fileDestination, gldictCase[self.lineEditFallbezeichnung.text()], self.strXWApp))
            logger_handleCreateButton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            pass

        boolStartXwaysAsAdmin = self.checkBoxAdmin.isChecked()
        defWriteCaseSettings(glsCaseSettingsStorage, self.lineEditFallbezeichnung.text(), boolStartXwaysAsAdmin)  # DONE: Funktion implementieren - ruft spezialfunktion(),
        dictReturn = defReadCaseSettings(glsCaseSettingsStorage)
        defUpdateListWidget(sorted(dictReturn.get('SectionList'), key=glPreSortFunc, reverse=glBoolSortReverseOrder), self.parent())
        if self.checkBoxStart.isChecked():
            # INFO: hier wird der AppName für die Ausführung der 32bit oder der 64bit Version in den Pfad geschrieben
            # slistPathToApp = [path.join(destination, 'winhex64.exe')]  # , '']
            slistPathToApp = [path.join(destination, gldictLauncher['XWAYSexe' + str(gliXWAppBinary)].get(self.strXWApp).replace('.\\', '\\'))]
            if self.strXWApp == 'AppXWF':       # INFO: Kommandozeilenparameter gibts derzeit leider nur für XWF
                slistPathToApp.append('NewCase:' + self.lineEditFallbezeichnung.text())
                sCtrFiles = self.textEditCtrFile.toPlainText().replace('\n','')
                listCtrFiles = sCtrFiles.split(';')
                if len(listCtrFiles) > 0:
                    for pathImage in listCtrFiles:
                        slistPathToApp.append('AddImage:' + path.normpath(pathImage.strip(' ')))
                slistPathToApp.append('Override:2')
                # slistPathToApp.append('RVS:~')
                # slistPathToApp.append('auto')
            try:
                logger_handleCreateButton.debug('Programmparameter: {!r}: cwd={!s}'.format(slistPathToApp, destination))
                Popen(slistPathToApp, cwd=destination, shell=True)  # , cwd=destination , stdout=subprocess.PIPE, stderr=PIPE
                # Popen(slistPathToApp2, cwd=destination, shell=True)
                logger_handleCreateButton.debug('Programmstart ausgeführt: {!r}: cwd={!s}'.format(slistPathToApp, destination))
                self.statusbar.showMessage('Programmstart ausgeführt: {!r}: cwd={!s}'.format(slistPathToApp, destination), 4000)
            except WindowsError as err:
                logger_handleCreateButton.exception('Fehler beim Programmstart: os.system({!r}):{!s}'.format(slistPathToApp, err))
                self.statusbar.showMessage('Fehler beim Programmstart: {!r};  {!s}'.format(slistPathToApp, err), 4000)
                logger_handleCreateButton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
        logger_handleCreateButton.debug('Ende handleCreateButton {}'.format(''))
        self.lineEditStandardDir.clear()
        self.lineEditFallbezeichnung.clear()
        self.close()

    def CreateDirectorySkeleton(self, destination, sAppXW):  # INFO: fertig
        """
        Es wird die Verzeichnisstruktur des neuen XWAYS-Ordners angelegt, sowie das classCase Objekt angelegt und mit entsprechenden Werten aus der NewCase-Maske ergänzt.
        :param destination: Zielpfad, worin die Verzeichnisse angelegt werden
        :param sAppXW: Investigator oder Forensics
        :return: -1, wenn Case-Objekt nicht angelegt; IOError as e: wenn bei einer Dateisystemoperation etwas schief gelaufen ist.
        """
        logger_CreateDirectorySkeleton = getLogger('XWAYS_Launcher.XW_NewCase.XW_NewCase.defCreateDirectorySkeleton{sig}'.format(sig=inspect.signature(self.CreateDirectorySkeleton)))
        logger_CreateDirectorySkeleton.debug('Beginn defCreateDirectorySkeleton {}'.format(''))
        try:
            objCaseEntry = classCase(self.lineEditFallbezeichnung.text(), sAppXW)
        except Exception:
            logger_CreateDirectorySkeleton.exception('Fehler: objCaseEntry = classCase(self.lineEditFallbezeichnung.text(), sAppXW): {0}:{1}'.format(exc_info()[1], exc_info()[0]))
            return -1
        objCaseEntry.set([self.lineEditStandardDir.objectName().replace('lineEdit', ''), destination])  # Path wird hinzugefügt
        objCaseEntry.set(['App', gldictLauncher['XWAYSexe' + str(self.intBinary)].get(self.strXWApp)])  # App wird hinzugefügt
        objCaseEntry.set(['OpenContainer', '0'])  # OpenContainer wird hinzugefügt
        objCaseEntry.set(['AppVersion', self.comboBox.currentText().replace('aktuell_', '')])  # AppVersion wird hinzugefügt
        objCaseEntry.set(['erstellt', datetime.now().strftime("%Y-%m-%d %H:%M:%S")])  # Erstellungsdatum wird hinzugefügt
        try:
            strBuf = path.join(self.lineEditExport.text(), gldictLauncher['FolderNameExport'].get(sAppXW))  # self.labelExport.text())       # .replace('.', '', 2)
            objCaseEntry.set([self.lineEditExport.objectName().replace('lineEdit', ''), strBuf])
            if not path.isdir(strBuf):
                try:
                    self.makeMyDirs(strBuf)
                except OSError as e:
                    if e.errno != errno.EEXIST:
                        logger_CreateDirectorySkeleton.debug('e.errno != errno.EEXIST -> {}\nstrBuf = {}'.format(e.errno, strBuf))
                        logger_CreateDirectorySkeleton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                        raise
            strBuf = path.join(self.lineEditContainer.text(), gldictLauncher['FolderNameContainer'].get(sAppXW))  # self.labelContainer.text())     # .replace('.', '', 2)
            objCaseEntry.set([self.lineEditContainer.objectName().replace('lineEdit', ''), strBuf])
            if not path.isdir(strBuf):
                try:
                    self.makeMyDirs(strBuf)
                except OSError as e:
                    if e.errno != errno.EEXIST:
                        logger_CreateDirectorySkeleton.debug('e.errno != errno.EEXIST -> {}\nstrBuf = {}'.format(e.errno, strBuf))
                        logger_CreateDirectorySkeleton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                        raise
            strBuf = path.join(self.lineEditFall.text(), gldictLauncher['FolderNameFall'].get(sAppXW))  # self.labelFall.text())       # .replace('.', '', 2)
            objCaseEntry.set([self.lineEditFall.objectName().replace('lineEdit', ''), strBuf])
            if not path.isdir(strBuf):
                try:
                    self.makeMyDirs(strBuf)
                except OSError as e:
                    if e.errno != errno.EEXIST:
                        logger_CreateDirectorySkeleton.debug('e.errno != errno.EEXIST -> {}\nstrBuf = {}'.format(e.errno, strBuf))
                        logger_CreateDirectorySkeleton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                        raise
            strBuf = path.join(self.lineEditHashDB.text(), gldictLauncher['FolderNameHashDB'].get(sAppXW))     # .replace('.', '', 2)
            objCaseEntry.set([self.lineEditHashDB.objectName().replace('lineEdit', ''), strBuf])
            objCaseEntry.set(['HashDB2', path.join(self.lineEditHashDB.text(), gldictLauncher['FolderNameHashDB2'].get(sAppXW))])  # .replace('.', '', 2)])
            if not path.isdir(strBuf):
                try:
                    self.makeMyDirs(strBuf)
                    self.makeMyDirs(path.join(self.lineEditHashDB.text(), gldictLauncher['FolderNameHashDB2'].get(sAppXW)))   # .replace('.', '', 2))
                except OSError as e:
                    if e.errno != errno.EEXIST:
                        logger_CreateDirectorySkeleton.debug('e.errno != errno.EEXIST -> {}\nstrBuf = {}'.format(e.errno, strBuf))
                        logger_CreateDirectorySkeleton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                        raise
            strBuf = path.join(self.lineEditHashDBPDNA.text(), gldictLauncher['FolderNameHashDBPDNA'].get(sAppXW))       # .replace('.', '', 2)
            objCaseEntry.set([self.lineEditHashDBPDNA.objectName().replace('lineEdit', ''), strBuf])
            objCaseEntry.set(['HashDBFuzzy', path.join(self.lineEditHashDB.text(), gldictLauncher['FolderNameHashDBFuzzy'].get(sAppXW))])  # .replace('.', '', 2)])
            if not path.isdir(strBuf):
                try:
                    self.makeMyDirs(strBuf)
                    self.makeMyDirs(path.join(self.lineEditHashDB.text(), gldictLauncher['FolderNameHashDBFuzzy'].get(sAppXW)))       # .replace('.', '', 2))
                except OSError as e:
                    if e.errno != errno.EEXIST:
                        logger_CreateDirectorySkeleton.debug('e.errno != errno.EEXIST -> {}\nstrBuf = {}'.format(e.errno, strBuf))
                        logger_CreateDirectorySkeleton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                        raise
            strBuf = path.join(self.lineEditSicherungen.text(), gldictLauncher['FolderNameSicherungen'].get(sAppXW))  # self.labelSicherungen.text())     # .replace('.', '', 2)
            objCaseEntry.set([self.lineEditSicherungen.objectName().replace('lineEdit', ''), strBuf])
            if not path.isdir(strBuf):
                try:
                    self.makeMyDirs(strBuf)
                except OSError as e:
                    if e.errno != errno.EEXIST:
                        logger_CreateDirectorySkeleton.debug('e.errno != errno.EEXIST -> {}\nstrBuf = {}'.format(e.errno, strBuf))
                        logger_CreateDirectorySkeleton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                        raise
            strBuf = path.join(self.lineEditTemp.text(), gldictLauncher['FolderNameTemp'].get(sAppXW))  # self.labelTemp.text())           # .replace('.', '', 2)
            objCaseEntry.set([self.lineEditTemp.objectName().replace('lineEdit', ''), strBuf])
            if not path.isdir(strBuf):
                try:
                    self.makeMyDirs(strBuf)
                except OSError as e:
                    if e.errno != errno.EEXIST:
                        logger_CreateDirectorySkeleton.debug('e.errno != errno.EEXIST -> {}\nstrBuf = {}'.format(e.errno, strBuf))
                        logger_CreateDirectorySkeleton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                        raise
            gldictCase[objCaseEntry.sCaseName] = objCaseEntry.dictCase
            logger_CreateDirectorySkeleton.debug('Fall {0} wurde der Liste hinzugefügt\nEinstellungen: {1}'.format(objCaseEntry.sCaseName, gldictCase.get(objCaseEntry.sCaseName)))
        except IOError as e:
            if e.errno != errno.EEXIST:
                logger_CreateDirectorySkeleton.critical('Ein oder mehrere Zielverzeichnisse konnten nicht erstellt werden! Bitte überprüfen!\nFehler: {}'.format(e))
                logger_CreateDirectorySkeleton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                return -1

    def check_state(self):  # INFO: fertig        # , *args, **kwargs
        """Validierung der Eingabe

        Der Inhalt 'LineEdit.text()' wird gegen die korrekte Eingabe eines Verzeichnispfades geprüft
        und der Hintergrund entsprechend farblich gekennzeichnet.
        """
        sender = self.sender()
        validator = sender.validator()
        state = validator.validate(sender.text(), 0)[0]
        if state == QValidator.Acceptable:
            color = '#c4df9b'  # green
        elif state == QValidator.Intermediate:
            color = '#fff79a'  # yellow
        else:
            color = '#f6989d'  # red
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def checkBox_changed(self, cb):
        logger_checkBox_changed = getLogger('XWAYS_Launcher.XW_NewCase.checkBox_changed{sig}'.format(sig=inspect.signature(self.checkBox_changed)))
        logger_checkBox_changed.debug('Beginn checkBox_changed( {0} )'.format(cb.objectName()))

        if cb.objectName() == 'checkBoxStart':
            if cb.isChecked():
                self.textEditCtrFile.setEnabled(True)
                logger_checkBox_changed.debug('checkBox_changed: {0} --> setEnabled'.format(cb.objectName()))
            else:
                self.textEditCtrFile.clear()
                self.textEditCtrFile.setDisabled(True)
                logger_checkBox_changed.debug('checkBox_changed: {0} --> setDisabled'.format(cb.objectName()))

    def lineEditStandardDir_changed(self):  # , sAppXW):   # INFO: fertig
        logger_lineEditStandardDir_changed = getLogger('XWAYS_Launcher.XW_NewCase.lineEditStandardDir_changed{sig}'.format(sig=inspect.signature(self.lineEditStandardDir_changed)))
        logger_lineEditStandardDir_changed.debug('Beginn lineEditStandardDir_changed {0}'.format(''))

        strSubFolder = gldictLauncher['XWCaseSubfolder'].get(self.strXWApp)
        if strSubFolder in self.lineEditStandardDir.text():        #  and
            strSubFolder = ''

        strFallBezeichnung = self.lineEditFallbezeichnung.text()
        if strFallBezeichnung in self.lineEditStandardDir.text():
            strFallBezeichnung = ''

        strDir = path.abspath(path.join(self.lineEditStandardDir.text(), strFallBezeichnung, strSubFolder))  # gldictLauncher['FolderStandardNewCase'].get(self.strXWApp)
            # strDir = path.join(self.lineEditStandardDir.text(), strFallBezeichnung, strSubFolder)
        if self.strXWApp == 'AppXWI':
            for w in self.centralwidget.findChildren(QLineEdit, QRegExp('^lineEdit')):
                if w.objectName() != 'lineEditFallbezeichnung':
                    w.setText(strDir)
        else:
            for w in self.centralwidget.findChildren(QLineEdit, QRegExp('^lineEdit')):
                if w.objectName() != 'lineEditStandardDir' and w.objectName() != 'lineEditFallbezeichnung':
                    if w.text() == '':
                        w.setText(strDir)
                if w.objectName() == 'lineEditTemp' and self.lineEditFallbezeichnung.text() not in self.lineEditTemp.text():
                    self.lineEditTemp.setText(path.join(self.lineEditTemp.text(), self.lineEditFallbezeichnung.text()))

    def lineEditFallbezeichnung_changed(self):  # , sAppXW):   # INFO: fertig
        logger_lineEditFallbezeichnung_changed = getLogger('XWAYS_Launcher.XW_NewCase.lineEditFallbezeichnung_changed{sig}'.format(sig=inspect.signature(self.lineEditStandardDir_changed)))
        logger_lineEditFallbezeichnung_changed.debug('Beginn lineEditFallbezeichnung_changed {0}'.format(''))

        strSubFolder = gldictLauncher['XWCaseSubfolder'].get(self.strXWApp)
        strFallBezeichnung = self.lineEditFallbezeichnung.text()

        if strSubFolder in self.lineEditStandardDir.text():
            strSubFolder = ''

        if strFallBezeichnung in self.lineEditStandardDir.text():
            strFallBezeichnung = ''

        strDir = path.join(gldictLauncher['FolderStandardNewCase'].get(self.strXWApp), strFallBezeichnung)
        if self.strXWApp == 'AppXWI':
            strDir = path.join(strDir, strSubFolder)

            for w in self.centralwidget.findChildren(QLineEdit, QRegExp('^lineEdit')):
                if w.objectName() != 'lineEditFallbezeichnung':
                    w.setText(strDir)
            # self.lineEditStandardDir.setText(path.join(gldictLauncher['FolderStandardNewCase'].get(self.strXWApp), strFallBezeichnung, strSubFolder))
            # self.lineEditContainer.setText(path.join(gldictLauncher['FolderStandardNewCase'].get(self.strXWApp), strFallBezeichnung, strSubFolder))
            # self.lineEditExport.setText(path.join(gldictLauncher['FolderStandardNewCase'].get(self.strXWApp), strFallBezeichnung, strSubFolder))
            # self.lineEditFall.setText(path.join(gldictLauncher['FolderStandardNewCase'].get(self.strXWApp), strFallBezeichnung, strSubFolder))
        # else:
        #     for w in self.centralwidget.findChildren(QLineEdit, QRegExp('^lineEdit')):
        #         if w.objectName() != 'lineEditFallbezeichnung':
        #             if w.text() == '':
        #                 w.setText(strDir)
        #     self.lineEditTemp.setText(path.join(gldictLauncher['FolderNameFillInTemp'].get(self.strXWApp), strFallBezeichnung))
        #     self.lineEditStandardDir.setText(path.join(gldictLauncher['FolderStandardNewCase'].get(self.strXWApp), strFallBezeichnung))

    def combobox_change(self, iSelectedXW):  # INFO: fertig
        logger_combobox_change = getLogger('XWAYS_Launcher.XW_NewCase.combobox_change{sig}  '.format(sig=inspect.signature(self.combobox_change)))
        logger_combobox_change.debug('Beginn combobox_change mit Index:{0}'.format(iSelectedXW))

        try:
            if type(iSelectedXW) == int:
                logger_combobox_change.debug('iSelectedXW mit Index:{0}'.format(iSelectedXW))
            else:
                logger_combobox_change.debug('iSelectedXW:{0}, keine Zahl->return'.format(iSelectedXW))
                return
            # iSelectedXW = int(iSelectedXW)
            # logger_combobox_change.debug('iSelectedXW = int(iSelectedXW): {0}'.format(iSelectedXW))
        except ValueError as valErr:
            logger_combobox_change.debug('iSelectedXW = int(iSelectedXW): {0}, ValueError: {1}'.format(iSelectedXW, valErr))
            logger_combobox_change.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            return
        logger_combobox_change.debug('strSelectedXW = self.comboBox.currentText(): {0}'.format(self.comboBox.itemText(iSelectedXW)))
        self.strXWApp = ''

        if 'x32' in self.comboBox.currentText():
            self.intBinary = 32
        else:
            self.intBinary = 64

        if 'xwf' in self.comboBox.itemText(iSelectedXW).lower():
            logger_combobox_change.debug('"xwf" gefunden in self.comboBox.currentText(): {0}'.format(self.comboBox.itemText(iSelectedXW)))
            self.strXWApp = 'AppXWF'
            self.checkBoxAdmin.setChecked(True)
            self.checkBoxAdmin.setVisible(True)
            self.lineEditStandardDir.setText(gldictLauncher['FolderStandardNewCase'].get(self.strXWApp).replace('#\'_+~*\\=/&%$§\"!@€;:.,}][{', '-').replace("'", "", 2))
            self.lineEditStandardDir.setEnabled(True)
            self.lineEditSicherungen.setEnabled(True)
            self.lineEditHashDB.setEnabled(True)
            self.lineEditExport.setEnabled(True)
            self.lineEditHashDBPDNA.setEnabled(True)
            self.lineEditContainer.setEnabled(True)
            self.lineEditTemp.setEnabled(True)
            self.lineEditFall.setEnabled(True)
            logger_combobox_change.debug('self.checkBoxAdmin isChecked(){0}   isVisible(){1}'.format(self.checkBoxAdmin.isChecked(), self.checkBoxAdmin.isVisible()))
        elif 'xwi' in self.comboBox.itemText(iSelectedXW).lower():
            logger_combobox_change.debug('"xwi" gefunden in strSelectedXW = self.comboBox.currentText(): {0}'.format(self.comboBox.itemText(iSelectedXW)))
            self.strXWApp = 'AppXWI'
            self.checkBoxAdmin.setChecked(False)
            self.checkBoxAdmin.setVisible(False)

            strSubFolder = gldictLauncher['XWCaseSubfolder'].get(self.strXWApp)
            strFallBezeichnung = repr(self.lineEditFallbezeichnung.text()).replace('#\'_+~*\\=/&%$§\"!@€;:.,}][{', '-').replace("'", "", 2)
            strDir = path.join(gldictLauncher['FolderStandardNewCase'].get(self.strXWApp), strFallBezeichnung, strSubFolder).replace('#\'_+~*\\=/&%$§\"!@€;:.,}][{', '-')

            for w in self.centralwidget.findChildren(QLineEdit, QRegExp('^lineEdit')):
                if w.objectName() != 'lineEditFallbezeichnung':
                    w.setText(strDir)

            # if repr(self.lineEditFallbezeichnung.text()).replace('#\'_+~*\\=/&%$§\"!@€;:.,}][{', '-') != "''":

            # self.lineEditStandardDir.setText(strPath)
            # self.lineEditTemp.setText(strPath)
            # self.lineEditSicherungen.setText(strPath)
            # self.lineEditHashDB.setText(strPath)
            # self.lineEditHashDBPDNA.setText(strPath)

            # else:
            #     self.lineEditStandardDir.setText(path.join(gldictLauncher['FolderStandardNewCase'].get(self.strXWApp), strSubFolder).replace('#\'_+~*\\=/&%$§\"!@€;:.,}][{', '-').replace("'", "", 2))
            #     self.lineEditTemp.setText(path.join(gldictLauncher['FolderStandardNewCase'].get(self.strXWApp), strSubFolder).replace('#\'_+~*\\=/&%$§\"!@€;:.,}][{', '-').replace("'", "", 2))
            #     self.lineEditSicherungen.setText(path.join(gldictLauncher['FolderNameSicherungen'].get(self.strXWApp), strSubFolder).replace('#\'_+~*\\=/&%$§\"!@€;:.,}][{', '-'))
            #     self.lineEditHashDB.setText(path.join(gldictLauncher['FolderNameHashDB'].get(self.strXWApp), strSubFolder).replace('#\'_+~*\\=/&%$§\"!@€;:.,}][{', '-'))
            #     self.lineEditHashDBPDNA.setText(path.join(gldictLauncher['FolderNameHashDB_PDNA'].get(self.strXWApp), strSubFolder).replace('#\'_+~*\\=/&%$§\"!@€;:.,}][{', '-'))

            self.lineEditStandardDir.setEnabled(False)
            self.lineEditSicherungen.setEnabled(False)
            self.lineEditHashDB.setEnabled(False)
            self.lineEditExport.setEnabled(False)
            self.lineEditHashDBPDNA.setEnabled(False)
            self.lineEditContainer.setEnabled(False)
            self.lineEditTemp.setEnabled(False)
            self.lineEditFall.setEnabled(False)
            logger_combobox_change.debug('self.checkBoxAdmin isChecked(){0}   isVisible(){1}'.format(self.checkBoxAdmin.isChecked(), self.checkBoxAdmin.isVisible()))
        else:
            logger_combobox_change.debug('Achtung weder "xwf" noch "xwi" gefunden in strSelectedXW = self.comboBox.currentText(): {0}\n Behalte Einstellung strXWApp = "AppXWF" bei!'.format(self.comboBox.itemText(iSelectedXW)))
            self.strXWApp = 'AppXWF'
            self.checkBoxAdmin.setChecked(True)
            self.checkBoxAdmin.setVisible(True)
            self.lineEditStandardDir.setText(gldictLauncher['FolderStandardNewCase'].get(self.strXWApp).replace('#\'_+~*\\=/&%$§\"!@€;:.,}][{', '-'))
            self.lineEditStandardDir.setEnabled(True)
            self.lineEditSicherungen.setEnabled(True)
            self.lineEditHashDB.setEnabled(True)
            self.lineEditExport.setEnabled(True)
            self.lineEditHashDBPDNA.setEnabled(True)
            self.lineEditContainer.setEnabled(True)
            self.lineEditTemp.setEnabled(True)
            self.lineEditFall.setEnabled(True)
            logger_combobox_change.debug('self.checkBoxAdmin isChecked(){0}   isVisible(){1}'.format(self.checkBoxAdmin.isChecked(), self.checkBoxAdmin.isVisible()))

        self.labelHashDB.setText('\\' + gldictLauncher[self.labelHashDB.objectName()].get(self.strXWApp))
        self.labelTemp.setText('\\' + gldictLauncher[self.labelTemp.objectName()][self.strXWApp])
        self.labelContainer.setText('\\' + gldictLauncher[self.labelContainer.objectName()][self.strXWApp])
        self.labelFall.setText('\\' + gldictLauncher[self.labelFall.objectName()][self.strXWApp])
        self.labelExport.setText('\\' + gldictLauncher[self.labelExport.objectName()][self.strXWApp])
        self.labelHashDBPDNA.setText('\\' + gldictLauncher[self.labelHashDBPDNA.objectName()][self.strXWApp])
        self.labelSicherungen.setText('\\' + gldictLauncher[self.labelSicherungen.objectName()][self.strXWApp])
        logger_combobox_change.debug('Ende combobox_change {}'.format(''))
        self.strSelectedXW = self.comboBox.currentText()
        logger_combobox_change.debug('frm_XW_NewCase.setupUi: self.strSelectedXW = self.comboBox.currentText(): {0}'.format(self.strSelectedXW))
        self.sXWDirpath = self.comboBox.model().index(self.comboBox.currentIndex(), 1).data()
        logger_combobox_change.debug('frm_XW_NewCase.setupUi: self.sXWDirpath = self.comboBox.model().index(self.comboBox.currentIndex(), 1).data(): {0}'.format(self.sXWDirpath))

    def toolButtonStandardDir_pressed(self):  # INFO: fertig            #, bUpdate
        options = QFileDialog.Options()
        options |= QFileDialog.ShowDirsOnly
        strDir = path.normpath(QFileDialog.getExistingDirectory(self, "Auswahl Fall-Hauptverzeichnis", gldictLauncher[self.labelStandardDir.objectName()][self.strXWApp], options=options))
        if not strDir or strDir == ".":
            return
        self.lineEditStandardDir.setText(strDir)

    def toolButtonCtrFile_pressed(self):  # INFO: fertig            #, bUpdate
        logger_toolButtonCtrFile_pressed = getLogger('.toolButtonCtrFile_pressed{sig}'.format(sig=inspect.signature(self.toolButtonCtrFile_pressed)))
        logger_toolButtonCtrFile_pressed.debug('Beginn toolButtonCtrFile_pressed {0}'.format(''))

        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        listFile, _ = QFileDialog.getOpenFileNames(self, "Auswahl Ctr-Datei(en)", gldictLauncher[self.labelStandardDir.objectName()][self.strXWApp], options=options)
        if not listFile or listFile[0] == ".":
            return
        if self.textEditCtrFile.toPlainText() != '':
            self.textEditCtrFile.moveCursor(QTextCursor.End)
            self.textEditCtrFile.insertPlainText(';')
            self.textEditCtrFile.moveCursor(QTextCursor.End)
        self.textEditCtrFile.append(';\n'.join(listFile))
        logger_toolButtonCtrFile_pressed.debug('Inhalt von self.textEditCtrFile {0}'.format(self.textEditCtrFile.toPlainText()))

    def toolButtonHashDB_pressed(self):  # INFO: fertig
        options = QFileDialog.Options()
        options |= QFileDialog.ShowDirsOnly
        strDir = path.normpath(QFileDialog.getExistingDirectory(self, "Auswahl HashDB-Verzeichnis", gldictLauncher[self.labelStandardDir.objectName()][self.strXWApp], options=options))
        if not strDir or strDir == ".":
            return
        self.lineEditHashDB.setText(strDir)

    def toolButtonFall_pressed(self):  # INFO: fertig
        options = QFileDialog.Options()
        options |= QFileDialog.ShowDirsOnly
        strDir = path.normpath(QFileDialog.getExistingDirectory(self, "Auswahl Fall-Verzeichnis", gldictLauncher[self.labelStandardDir.objectName()][self.strXWApp], options=options))
        if not strDir or strDir == ".":
            return
        self.lineEditFall.setText(strDir)

    def toolButtonExport_pressed(self):  # INFO: fertig
        options = QFileDialog.Options()
        options |= QFileDialog.ShowDirsOnly
        strDir = path.normpath(QFileDialog.getExistingDirectory(self, "Auswahl Export-Verzeichnis", gldictLauncher[self.labelStandardDir.objectName()][self.strXWApp], options=options))
        if not strDir or strDir == ".":
            return
        self.lineEditExport.setText(strDir)

    def toolButtonHashDBPDNA_pressed(self):  # INFO: fertig
        options = QFileDialog.Options()
        options |= QFileDialog.ShowDirsOnly
        strDir = path.normpath(QFileDialog.getExistingDirectory(self, "Auswahl HashDB_PDNA-Verzeichnis", gldictLauncher[self.labelStandardDir.objectName()][self.strXWApp], options=options))
        if not strDir or strDir == ".":
            return
        self.lineEditHashDBPDNA.setText(strDir)

    def toolButtonSicherungen_pressed(self):  # INFO: fertig
        options = QFileDialog.Options()
        options |= QFileDialog.ShowDirsOnly
        strDir = path.normpath(QFileDialog.getExistingDirectory(self, "Auswahl Sicherungen-Verzeichnis", gldictLauncher[self.labelStandardDir.objectName()][self.strXWApp], options=options))
        if not strDir or strDir == ".":
            return
        self.lineEditSicherungen.setText(strDir)

    def toolButtonContainer_pressed(self):  # INFO: fertig
        options = QFileDialog.Options()
        options |= QFileDialog.ShowDirsOnly
        strDir = path.normpath(QFileDialog.getExistingDirectory(self, "Auswahl Container-Verzeichnis", gldictLauncher[self.labelStandardDir.objectName()][self.strXWApp], options=options))
        if not strDir or strDir == ".":
            return
        self.lineEditContainer.setText(strDir)

    def toolButtonTMP_pressed(self):  # INFO: fertig
        options = QFileDialog.Options()
        options |= QFileDialog.ShowDirsOnly
        strDir = path.normpath(QFileDialog.getExistingDirectory(self, "Auswahl TMP-Verzeichnis", gldictLauncher[self.labelStandardDir.objectName()][self.strXWApp], options=options))
        if not strDir or strDir == ".":
            return
        self.lineEditTemp.setText(strDir)


class myList(QListWidget):
    # dropped = pyqtSignal(list)

    def __init__(self, parent):
        super().__init__(parent)
        self.setSelectionMode(self.SingleSelection)
        self.setDragDropMode(QAbstractItemView.InternalMove)
        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
            # event.acceptProposedAction()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            # event.setDropAction(Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        logger_dropEvent = getLogger('XWAYS_Launcher.myList.dropEvent{sig}'.format(sig=inspect.signature(self.dropEvent)))
        logger_dropEvent.debug('Beginn dropEvent {0}'.format(''))
        # sNewItem = ''
        aListEntries = []
        if event.mimeData().hasUrls and len(event.mimeData().urls()) > 0:
            for url in event.mimeData().urls():
                sPath = url.toLocalFile()
                if path.isfile(sPath):

                    sNewItem = readRegfile(sPath)[0][0].replace(glsRegRootString + '\\' + glsRegXWFPath + '\\', '')
                    priv_flags = win32security.TOKEN_ADJUST_PRIVILEGES | win32security.TOKEN_QUERY
                    hToken = win32security.OpenProcessToken(win32api.GetCurrentProcess(), priv_flags)
                    privilege_id = win32security.LookupPrivilegeValue(None, "SeBackupPrivilege")
                    win32security.AdjustTokenPrivileges(hToken, 0, [(privilege_id, win32security.SE_PRIVILEGE_ENABLED)])
                    try:
                        system("reg import " + sPath)
                        logger_dropEvent.debug('Eintrag importieren: Erfolg:   system("reg import " + sPath) erfolgreich : sCmd: {0}, sPath: {1}'.format("['reg', 'import', sPath]", sPath))
                        aListEntries = defReadREG_CaseEntries()
                        logger_dropEvent.debug('aListEntries: {}\ngldictCase: {}'.format(aListEntries, gldictCase))
                        self.nativeParentWidget().statusbar.showMessage('Fall-Einstellungen importiert: {}: aus der Datei={}'.format(sNewItem, sPath), 4000)
                    except WindowsError as err:
                        logger_dropEvent.debug('Eintrag importieren: Fehler{}:   system("reg import " + sPath) fehlgeschlagen: Pfad: {}'.format(err, "['reg', 'import', sPath]", sPath))
                        logger_dropEvent.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                        self.nativeParentWidget().statusbar.showMessage('Fall-Einstellungen konnten nicht importiert werden: {}: cwd={}'.format(err, sPath), 4000)
                        pass
                    except Exception:
                        logger_dropEvent.exception('Ein Fehler ist beim Importieren der Fall-Sicherung aufgetreten: exc_info(): {0}:{1}; SPath: {2}'.format(exc_info()[1], exc_info()[0], sPath))
                        pass

            try:
                event.setDropAction(Qt.CopyAction)
                defUpdateListWidget(sorted(aListEntries, key=glPreSortFunc, reverse=glBoolSortReverseOrder), self.nativeParentWidget())
                event.acceptProposedAction()
            except Exception:
                logger_dropEvent.exception('Ein Fehler ist aufgetreten: sPath: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
                pass
        else:
            event.ignore()


class XWLauncher(Ui_frm_XW_Launcher):
    def __init__(self, *args):
        logger___init__ = getLogger('XWAYS_Launcher.XWLauncher.__init__{sig}'.format(sig=inspect.signature(self.__init__)))
        logger___init__.debug('Beginn __init__ {0}'.format(''))
        super(XWLauncher, self).__init__(*args)
        self.setAttribute(Qt.WA_DeleteOnClose)
        global gldictCase
        self.List1 = myList(self)   # List1 wird hier initialisiert, wegen custom myList
        self.verticalLayout_3.addWidget(self.List1)
        self.setupUi()
        self.pbtnAppClose.clicked.connect(self._handleCloseButton)
        self.pbtnCaseCreate.clicked.connect(self._handleNewCaseButton)
        self.pbtnCaseOpen.clicked.connect(self._handleOpenCaseButton)
        self.pbtnCaseRemove.clicked.connect(self._handleRemoveCaseButton)
        self.pbtnCaseSave.clicked.connect(self._handleCaseSaveButton)
        self.pbtnXDoku.clicked.connect(self._handleXWAYSDokumentation)
        self.actionHilfe.triggered.connect(self._handleMenuHilfe)
        self.actionEinstellungen.triggered.connect(self._handleMenuEinstellungen)
        self.actionRemoveLauncher.triggered.connect(self._handleMenuRemoveLauncher)
        self.actionUpdateLauncher.triggered.connect(self._handleMenuUpdate)
        self.actionInfo.triggered.connect(self._handleMenuInfoBox)
        self.actionColorSchema.triggered.connect(self._handleMenuColorSchema)
        self.actionUpdateCaseToolTips.triggered.connect(self._handleUpdateCaseToolTips)
        self.actionUpdatePersonalToolTips.triggered.connect(self._handleUpdatePersonalToolTips)
        self.List1.itemClicked.connect(self.onListWidgetItemClicked)
        self.Label_FolderCase.installEventFilter(self)
        self.Label_FolderContainer.installEventFilter(self)
        self.Label_FolderExport.installEventFilter(self)
        self.Label_FolderHashDB.installEventFilter(self)
        self.Label_FolderSave.installEventFilter(self)
        self.Label_FolderTemp.installEventFilter(self)
        self.Label_XWAYSPath.installEventFilter(self)

        self.localCaseDict = gldictCase
        self.sInfoBoxText = self.sInfoBoxText + "\n\n-Speicherort der LOG-Datei: " + glfileLogFile + "\n\nBei Fehlermeldungen und Abstürzen des Launchers, diese Logdatei mit senden!"
        self.windowXWNewCase = None
        self.windowXWConfig = None
        self.windowXWColorSchema = None

    def onListWidgetItemClicked(self):
        if 'xwf' in self.localCaseDict[self.List1.currentItem().text()].get('AppVersion'):
            self.strXWApp = 'AppXWF'
        if 'xwi' in self.localCaseDict[self.List1.currentItem().text()].get('AppVersion'):
            self.strXWApp = 'AppXWI'

    def eventFilter(self, source, event):
        if event.type() == QEvent.MouseButtonDblClick:
            self._handleLabelDoubleClick(source)
        return super(XWLauncher, self).eventFilter(source, event)

    def _handleLabelDoubleClick(self, source):
        logger_handleLabelDoubleClick = getLogger('XWAYS_Launcher.XWLauncher._handleLabelDoubleClick{sig}'.format(sig=inspect.signature(self._handleLabelDoubleClick)))
        logger_handleLabelDoubleClick.debug('Beginn _handleLabelDoubleClick {0}'.format(''))
        strLabelName = source.objectName()  # This is what you need
        # self.statusbar.showMessage('Pfad {0} wurde geklickt: {1}'.format(strLabelName.replace('Label_Folder', ''), source.text()), 4000)
        logger_handleLabelDoubleClick.debug('Pfad-Label {0} wurde geklickt: {1}'.format(strLabelName, source.text()))
        if source.text() != '-':
            try:
                # popen(sFilePath)
                # DETACHED_PROCESS = 0x00000008
                Popen(('explorer', source.text().replace('\\\\', '\\')), cwd=source.text(), shell=False)
                # subprocess.Popen(listString, cwd=strPfad)
                logger_handleLabelDoubleClick.debug('Programmstart ausgeführt: explorer {0}: cwd={1}'.format(source.text().replace('\\\\', '\\'), source.text()))
                self.statusbar.showMessage('Programmstart ausgeführt: explorer {0}: cwd={1}'.format(source.text().replace('\\\\', '\\'), source.text()), 4000)
                # subprocess.call(aString)
            except WindowsError as err:
                logger_handleLabelDoubleClick.debug('Fehler beim Programmstart: explorer {0}; {1}'.format(source.text().replace('\\\\', '\\'), err))
                logger_handleLabelDoubleClick.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                self.statusbar.showMessage('Fehler beim Programmstart: explorer {0};  WindowsError : {1}'.format(source.text().replace('\\\\', '\\'), err), 4000)
            except Exception:
                logger_handleLabelDoubleClick.debug('Fehler beim Programmstart: Popen({0}, cwd={1})'.format('(explorer {})'.format(source.text().replace('\\\\', '\\')), source.text().replace('\\\\', '\\')))
                logger_handleLabelDoubleClick.exception('Ein Fehler ist aufgetreten: XWLauncher Exception: {0}:{1}'.format(exc_info()[0], exc_info()[1]))
        else:
            logger_handleLabelDoubleClick.debug('Pfad {0} wurde geklickt: {1}'.format(strLabelName.replace('Label_Folder', ''), source.text().replace('\\\\', '\\')))

    def _handleNewCaseButton(self):
        logger_handleNewCaseButton = getLogger('XWAYS_Launcher.XWLauncher.handleNewCaseButton{sig}  '.format(sig=inspect.signature(self._handleNewCaseButton)))
        logger_handleNewCaseButton.debug('Beginn handleNewCaseButton {0}'.format(''))

        if self.windowXWNewCase is None:
            self.windowXWNewCase = XWNewCase(self)
            # self.windowXWNewCase.setupUi()
        # print("self.windowXWNewCase.show()")
        self.windowXWNewCase.show()
        # print("after self.windowXWNewCase.show()")
        self.windowXWNewCase.close_signal.connect(self.windowXWNewCase_popup_closed)

    def _handleCloseButton(self):
        logger_handleCloseButton = getLogger('XWAYS_Launcher.XWLauncher.handleCloseButton{sig}'.format(sig=inspect.signature(self._handleCloseButton)))
        logger_handleCloseButton.debug('XWAYS_Launcher wird über den "Anwendung Schließen"-Button beendet {0}'.format(''))
        self.close()

    def _handleOpenCaseButton(self):
        logger__handleOpenCaseButton = getLogger('XWAYS_Launcher.XWLauncher._handleOpenCaseButton{sig}'.format(sig=inspect.signature(self._handleOpenCaseButton)))
        logger__handleOpenCaseButton.debug('Beginn _handleOpenCaseButton {}'.format(''))

        try:
            if not self.List1.selectedItems():
                logger__handleOpenCaseButton.debug('Keinen Eintrag zum ausführen, vielleicht nichts ausgewählt!: {!s}'.format(''))
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("Fehler beim Entfernen der Falleinträge")
                msg.setText("Bitte in der Fallliste einen Fall-Eintrag zum ausführen auswählen,\ndann erneut versuchen!")
                msg.setStandardButtons(QMessageBox.Ok)
                return msg.exec_()
            else:
                logger__handleOpenCaseButton.debug('Los geht es mit: {!s}'.format(self.List1.currentItem().text()))
        except SyntaxError as synterr:
            logger__handleOpenCaseButton.debug('Wie eine Fata Morgana: error:{!s}\nWahrscheinlich kein eintrag zum löschen ausgewählt'.format(synterr))
            logger__handleOpenCaseButton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle("Fehler beim Entfernen der Falleinträge")
            msg.setText("Bitte in der Fallliste einen Eintrag zum Entfernen auswählen,\ndann erneut versuchen!")
            msg.setStandardButtons(QMessageBox.Ok)
            return synterr

        strPfad = gldictCase[self.List1.currentItem().text()]['StandardDir']        # Pfad']
        strApp = path.split(gldictCase[self.List1.currentItem().text()]['App'])[1]
        listString = [path.join(strPfad, strApp), '']
        sFilePath = path.join(strPfad, strApp)
        try:
            # popen(sFilePath)
            # DETACHED_PROCESS = 0x00000008
            Popen(listString, cwd=strPfad, shell=True)
            # subprocess.Popen(listString, cwd=strPfad)
            logger__handleOpenCaseButton.debug('Programmstart ausgeführt: {!r}: cwd={!s}'.format(sFilePath, strPfad))
            self.statusbar.showMessage('Programmstart ausgeführt: {!r}: cwd={!s}'.format(sFilePath, strPfad), 4000)
            # subprocess.call(aString)
        except WindowsError as err:
            logger__handleOpenCaseButton.debug('Fehler beim Programmstart: subprocess.Popen({!r}):{!s}'.format(sFilePath, err))
            logger__handleOpenCaseButton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            self.statusbar.showMessage('Fehler beim Programmstart: {!r};  {!s}'.format(sFilePath, err), 4000)

    def _handleRemoveCaseButton(self):
        logger__handleRemoveCaseButton = getLogger('XWAYS_Launcher.XWLauncher._handleRemoveCaseButton{sig}'.format(sig=inspect.signature(self._handleRemoveCaseButton)))
        logger__handleRemoveCaseButton.debug('Beginn _handleRemoveCaseButton {}'.format(''))
        try:
            if not self.List1.selectedItems():
                logger__handleRemoveCaseButton.debug('Keine Einträge zum entfernen, vielleicht nichts ausgewählt!: {!s}'.format(''))
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("Fehler beim Entfernen der Falleinträge")
                msg.setText("Bitte in der Fallliste einen Eintrag zum Entfernen auswählen,\ndann erneut versuchen!")
                msg.setStandardButtons(QMessageBox.Ok)
                return msg.exec_()
            else:
                logger__handleRemoveCaseButton.debug('Fall-Einstellungen zum Löschen ausgewählt: {!s}'.format(self.List1.currentItem().text()))
        except SyntaxError as synterr:
            logger__handleRemoveCaseButton.debug('Entfernen der Fall-Einstellungen : fehlgeschlagen - {!s}\nWahrscheinlich kein eintrag zum löschen ausgewählt'.format(synterr))
            logger__handleRemoveCaseButton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle("Fehler beim Entfernen der Falleinträge")
            msg.setText("Bitte in der Fallliste einen Eintrag zum Entfernen auswählen,\ndann erneut versuchen!")
            msg.setStandardButtons(QMessageBox.Ok)
            return synterr
        defDeleteCaseSettings(glsCaseSettingsStorage, self.List1.currentItem().text())  # ist standardmäßig drin->  , boolDelAsAdmin=True)
        self.statusbar.showMessage('Fall-Einstellungen entfernt: {}'.format(self.List1.currentItem().text()), 4000)
        logger__handleRemoveCaseButton.debug('Fall-Einstellungen entfernt: {}'.format(self.List1.currentItem().text()))
        self.List1.clear()
        self.List1.addItems(sorted(gldictCase.keys(), key=glPreSortFunc, reverse=glBoolSortReverseOrder))

    def _handleXWAYSDokumentation(self):
        """
        ruft die XWAYS-Handbuch/Dokumentation der installierten X-Ways Version des selektierten Falls auf.
        Args:

        """
        logger_handleXWAYSDokumentation = getLogger('XWAYS_Launcher.XWLauncher._handleXWAYSDokumentation{sig}'.format(sig=inspect.signature(self._handleXWAYSDokumentation)))
        logger_handleXWAYSDokumentation.info('Beginn _handleXWAYSDokumentation {0}'.format(''))
        try:
            if not self.List1.selectedItems():
                logger_handleXWAYSDokumentation.debug('Kein Eintrag ausgewählt um die Dokumentation aufzurufen!: {0!s}'.format(''))
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("Fehler beim Aufrufen der Dokumentation")
                msg.setText("Bitte in der Fall-Liste einen Eintrag auswählen,\ndann erneut versuchen!")
                msg.setStandardButtons(QMessageBox.Ok)
                return msg.exec_()
            # else:
                # logger_handleXWAYSDokumentation.debug('Dokumentation aufrufen: {0!s}'.format(gldictLauncher['XWDokumentationLocation'].get(self.strXWApp)))
        except SyntaxError as synterr:
            logger_handleXWAYSDokumentation.debug('Dokumentation aufrufen: Fehler:{0!s}\nWahrscheinlich kein Eintrag ausgewählt'.format(synterr))
            logger_handleXWAYSDokumentation.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle("Fehler beim Aufrufen der Dokumentation")
            msg.setText("Bitte in der Fall-Liste einen Eintrag auswählen,\ndann erneut versuchen!")
            msg.setStandardButtons(QMessageBox.Ok)
            return synterr

        strPfad = gldictCase[self.List1.currentItem().text()]['StandardDir']  # Pfad']
        lstHelpFiles = [gldictLauncher['XWDokumentationLocation'].get(self.strXWApp)+"*", "manual*"]
        lstFile = []
        for strHelpFile in lstHelpFiles:
            try:
                lstFile = glob.glob(path.join(strPfad, strHelpFile))
            except:
                logger_handleXWAYSDokumentation.exception(
                    'Ein Fehler ist aufgetreten: : {0}:{1}\nHilfe-Datei mit Namensbestandteil "{2}" nicht gefunden'.format(exc_info()[0], exc_info()[1], strHelpFile))
            if lstFile:
                # strFile = path.basename(lstFile[0])
                break
        else:
            logger_handleXWAYSDokumentation.debug('Dokumentation nicht gefunden')
            self.statusbar.showMessage('Dokumentation nicht gefunden: {!r}'.format(strPfad), 4000)
            return
        strFile = path.basename(lstFile[0])
        logger_handleXWAYSDokumentation.debug('Dokumentation aufrufen: Datei {0!s} gefunden'.format(strFile))
                # Info: zu debug Zwecken die Variable strFile aufgedröselt und zur Veranschaulichung alles wieder neu zusammengesetzt
        listString = [path.join(strPfad, strFile), '']   # = path.basename(lstFile[0])
        sFilePath = path.join(strPfad, strFile)
        if path.isfile(sFilePath):
            try:
                Popen(listString, cwd=strPfad, shell=True)
                logger_handleXWAYSDokumentation.debug('Programmstart ausgeführt: {!r}: cwd={!s}'.format(sFilePath, strPfad))
                self.statusbar.showMessage('Programmstart ausgeführt: {!r}: cwd={!s}'.format(sFilePath, strPfad), 4000)
            except WindowsError as err:
                logger_handleXWAYSDokumentation.debug('Fehler beim Programmstart: subprocess.Popen({!r}):{!s}'.format(sFilePath, err))
                logger_handleXWAYSDokumentation.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                self.statusbar.showMessage('Fehler beim Programmstart: {!r};  {!s}'.format(sFilePath, err), 4000)
        else:
            logger_handleXWAYSDokumentation.debug('Fehler beim Programmstart: Hilfe-Datei konnte nicht gefunden werden({!r})'.format(sFilePath))
            self.statusbar.showMessage('Datei {!r} nicht gefunden!'.format(sFilePath), 4000)

    def _handleCaseSaveButton(self, boolInvestigator=False):
        logger_handleCaseSaveButton = getLogger('XWAYS_Launcher.XWLauncher._handleCaseSaveButton{sig} '.format(sig=inspect.signature(self._handleCaseSaveButton)))
        logger_handleCaseSaveButton.debug('Beginn _handleCaseSaveButton {0}'.format(' '))
        try:
            if not self.List1.selectedItems():
                logger_handleCaseSaveButton.debug('Keine Einträge zum Sichern, vielleicht nichts ausgewählt!: {0!s}'.format(''))
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("Fehler beim Sichern der Fall-Einträge")
                msg.setText("Bitte in der Fall-Liste einen Eintrag auswählen,\ndann erneut versuchen!")
                msg.setStandardButtons(QMessageBox.Ok)
                return msg.exec_()
            else:
                logger_handleCaseSaveButton.debug('Eintrag sichern: {0!s}'.format(self.List1.currentItem().text()))
        except SyntaxError as synterr:
            logger_handleCaseSaveButton.debug('Eintrag sichern: Fehler:{0!s}\nWahrscheinlich kein Eintrag zum Sichern ausgewählt'.format(synterr))
            logger_handleCaseSaveButton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle("Fehler beim Sichern des Fall-Eintrags")
            msg.setText("Bitte in der Fall-Liste einen Eintrag zum Sichern auswählen,\ndann erneut versuchen!")
            msg.setStandardButtons(QMessageBox.Ok)
            return synterr
        # global gliXWApp64
        global glsCaseSettingsStorage

        priv_flags = win32security.TOKEN_ADJUST_PRIVILEGES | win32security.TOKEN_QUERY
        hToken = win32security.OpenProcessToken(win32api.GetCurrentProcess(), priv_flags)
        privilege_id = win32security.LookupPrivilegeValue(None, "SeBackupPrivilege")
        win32security.AdjustTokenPrivileges(hToken, 0, [(privilege_id, win32security.SE_PRIVILEGE_ENABLED)])

        if boolInvestigator:  # derzeit immer false
            glsRegPath = glsRegXWIPath
        else:
            glsRegPath = glsRegXWFPath  # glsRegXWFPath wird derzeit standardmäßig angenommen

        sArg = glsRegRootString + '\\' + glsRegPath + '\\' + self.List1.currentItem().text()
        sPfad = path.join(self.Label_FolderCase.text(), 'Falleinstellungen_' + self.List1.currentItem().text() + '.reg')
        try:
            aKey = winreg.OpenKey(glsRegRoot, glsRegPath + '\\' + self.List1.currentItem().text(), 0, winreg.KEY_ALL_ACCESS)
            aKey2 = winreg.OpenKey(glsRegRoot, 'SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers', 0, winreg.KEY_ALL_ACCESS)
            try:
                with open(sPfad, 'w', encoding='utf-16') as regFile:
                    regFile.write('Windows Registry Editor Version 5.00\n\n')
                    regFile.write('['+sArg + ']\n')
                    for i in range(winreg.QueryInfoKey(aKey)[1]):
                        regFile.write('"{0}"="{1}"\n'.format((winreg.EnumValue(aKey, i)[0]).replace('\\', '\\\\'), (winreg.EnumValue(aKey, i)[1]).replace('\\', '\\\\')))
                    regFile.write('\n')
                    logger_handleCaseSaveButton.debug('Fall-Eintrag gesichert: sArg: {0}, Pfad: {1}'.format(sArg, sPfad))
                    try:
                        if winreg.QueryValueEx(aKey2, path.join(self.Label_XWAYSPath.text(), self.Label_XWAYSApp.text()))[0] == 'RUNASADMIN':
                            regFile.write('[HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers]\n')
                            regFile.write("\"{}\"".format(path.join(self.Label_XWAYSPath.text(), self.Label_XWAYSApp.text())).replace('\\', '\\\\') + "=\"RUNASADMIN\"\n")
                            # regFile.write("\"{}\"".format(self.Label_XWAYSPath.text() + self.Label_XWAYSApp.text()).replace('\\', '\\\\') + "=\"RUNASADMIN\"\n")
                            regFile.write('\n')
                            logger_handleCaseSaveButton.debug('RUNASADMIN-Eintrag gesichert: sArg: {0}, Pfad: {1}'.format(sArg, sPfad))
                    except WindowsError as werr:
                        logger_handleCaseSaveButton.debug('RUNASADMIN-Eintrag sichern: Registry-Zugriff gescheitert mit Fehler {0}, REG-Key: {reg}'.format(werr, reg=(path.join(self.Label_XWAYSPath.text(), self.Label_XWAYSApp.text()))))
                        logger_handleCaseSaveButton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                        pass

                    regFile.close()
                    logger_handleCaseSaveButton.debug('Falleinstellungen gesichert:  Pfad: {0}'.format(sPfad))
                    self.statusbar.showMessage('Fall-Einstellungen gesichert: {}: Exportdatei ={}'.format(self.List1.currentItem().text(), sPfad), 4000)
            except Exception:
                    logger_handleCaseSaveButton.exception('RegEintrag sichern: Dateizugriff gescheitert mit Fehler {0} : {1},\n sPfad: {2}'.format(exc_info()[0], exc_info()[1], sPfad))
                    self.statusbar.showMessage('Fehler bei der Sicherung der Fall-Einstellungen für {}: Fehler={}: Exportdatei ={}'.format(self.List1.currentItem().text(), exc_info()[0], sPfad), 4000)
                    pass

        except EnvironmentError as err:
            logger_handleCaseSaveButton.debug('Fehler beim Öffnen der Registry: {0}: sArg: {1}, sPfad: {2}'.format(err, sArg, sPfad))
            logger_handleCaseSaveButton.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            self.statusbar.showMessage('Fall-Einstellungen konnten nicht gesichert werden: {}, {}, : Exportdatei={}'.format(self.List1.currentItem().text(), err, sPfad), 4000)
            pass
        except Exception:
            logger_handleCaseSaveButton.exception('Unbekannter Fehler: {0}, viel Spaß beim Suchen!'.format(exc_info()[0], exc_info()[1]))
            pass

    def _handleMenuEinstellungen(self):  # INFO: fertig
        logger_handleMenuEinstellungen = getLogger('XWAYS_Launcher.XWLauncher.handleMenuEinstellungen{sig}  '.format(sig=inspect.signature(self._handleMenuEinstellungen)))
        logger_handleMenuEinstellungen.debug('Beginn handleMenuEinstellungen {0}'.format(''))

        if self.windowXWConfig is None:
            self.windowXWConfig = MainWindow(self)  # XWConfig(self)
            logger_handleMenuEinstellungen.debug('self.windowXWConfig.settingsTree.refresh() initialisiert {0}'.format(self.windowXWConfig))
        else:
            logger_handleMenuEinstellungen.debug('Aufruf self.windowXWConfig.settingsTree.refresh() ausgeführt {0}'.format(self.windowXWConfig))
            self.windowXWConfig.settingsTree.refresh()
            logger_handleMenuEinstellungen.debug('self.windowXWConfig.settingsTree.refresh() ausgeführt {0}'.format(self.windowXWConfig))
        logger_handleMenuEinstellungen.debug('Aufruf self.windowXWConfig.show() {0}'.format(self.windowXWConfig))
        self.windowXWConfig.show()
        # self.windowXWConfig.close_signal.connect(self.windowXWConfig_popup_closed)

    def _handleMenuRemoveLauncher(self):
        logger__handleMenuRemoveLauncher = getLogger('._handleMenuRemoveLauncher{sig}  '.format(sig=inspect.signature(self._handleMenuRemoveLauncher)))
        logger__handleMenuRemoveLauncher.debug('Beginn _handleMenuRemoveLauncher {0}'.format(''))

        defDeleteREG_Case(glsRegXWLKey, False, False)
        logger__handleMenuRemoveLauncher.debug('Launchereinstellungen wurden entfernt. Launcher wird beendet! Danke für den Fisch!'.format(''))
        self.close()

    def _handleMenuHilfe(self):
        self.sHTML = """
<!DOCTYPE html><html lang="de"><!DOCTYPE html>
<head>
<title>X-Ways_Launcher: Bedienung der App und Hilfestellung</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta content="de" http-equiv="Content-Language">
<style type="text/css"><!--
* {font-family:"Segoe UI";}
.accordion { background-color: # ccc;    color: # 444;    cursor: pointer;    padding: 18px;    width: 100%;    border: none;    text-align: justify;    outline: none;    font-size: 20px;    transition: 0.4s;}
.active, .accordion:hover {background-color: # aaa; color: # fff;  }
.accordion:after { content: "\\002B";color: # 777;font-weight: bold;float: right;margin-left: 5px;}
.active:after {content: "\\2212"; color: # fff;}
.panel {   padding: 0 18px;   background-color: # eee;    color: black;  max-height: 0; overflow: hidden;  transition: max-height 0.2s ease-out;}
.tab {margin-left: 18px; margin-right: 18px;}
.css-treeview {font-size: 16px; list-style: none;}
.css-treeview ul, .css-treeview li{background: # eee; margin-left: 15px; padding-left: 5px; list-style: none; position: relative;}
.css-treeview li::before{content: " "; position: absolute; width: 1px; background-color: # 000; top: 5px; bottom: -12px; left: -10px;}
.css-treeview li:not(:first-child):last-child::before{display: none;}
.css-treeview li:only-child::before{display: list-item; content: " "; position: absolute; width: 1px; background-color: # 000;  top: 5px; bottom: 7px; left: -10px; height: 7px;}
.css-treeview li::after{content: " "; position: absolute; width: 10px; left: -10px; height: 1px; background-color: # 000; top: 12px;}
.css-treeview ul > .css-treeview li:first-child::before {top: 12px;}
body { text-align: justify;   background-color: # eee; margin: 0; padding: o;}
h1 { text-align: center; color: # 000; }
h2 {   text-align: justify;   color: # 555;       background-color: # eee;}
h3 {   text-align: justify;   color: # 666;       background-color: # eee;}
h4 {   text-align: justify;   color: # 777;       background-color: # eee;}
p {    text-align: justify;   color: black;      background-color: # eee;}
dl {  width: 100%; overflow: hidden;  background-color: # eee;  padding: 0;  margin: 0; width: 100%}
.w3-card-4 {box-shadow:0 4px 10px 0 rgba(0,0,0,0.2), 0 4px 20px 0 rgba(0,0,0,0.19); margin: auto;}
.QPushButton { padding: 10px; font-size: 20px; border: 1px solid grey; border-radius: 6px; color: white;  background: linear-gradient(# a6a6a6, # 7f7f7f 8%, # 717171 39%, # 626262 40%, # 4c4c4c 90%, # 333333 100%);} 
--></style>
</head><body>
<header style="color: # eee; margin: auto; width: 50%; text-align: center;"> Polizeipräsidium Ludwigsburg, Kriminalpolizeidirektion Böblingen, K5 ITB</header>
<h1 class="tab">X-Ways Launcher</h1>
<h2 class="tab">Bedienung der App und Hilfestellung</h2>
<p class="tab">INFO: Der XWAYS-Launcher nimmt keine Änderungen an den Falldaten/Asservaten vor. Ziel ist es, die umfänglichen Einrichtungsschritte für den Anwender zu übernehmen,
 jedoch die notwendige Freiheit zu gestatten, die Einstellungen nach eigenen Wünschen vorzunehmen!</p>
<button class="accordion">Installation</button>
<div class="panel">
    <h3>Konfiguration der XWAYS-Programmordner XWF/XWI</h3>
    <div>
        <h4>am Beispiel von XWAYS Forensics (für ITB):</h4>
        <p>Zumeist ist im Netzwerk der ITB oder im FEN bereits ein Ordner zur Verteilung/Bereitstellung hilfreicher Software angelegt. Empfehlenswert ist die Ablage des gesamten Programmpackets des XWAYS-Launchers in einem Subverzeichnis innerhalb dieser Verzeichnisstruktur.</p>
        <div class="w3-card-4" style="min-width: 15cm; max-width: 19cm;">
            <div class="css-treeview">
                <ul>
                    <li>[08 - Verschiedene Tools]
                    <ul>
                        <li>[...]</li>
                        <li>[XWAYS-Launcher]
                        <ul>
                            <li>[log]</li>
                            <li>[WinPython-64Bit-3.6.3.0Qt5]</li>
                            <li>[XWF-Versionen]
                            <ul>
                                <li>[xwf_19.7 SR-7 x64]</li>
                                <li>[xwf_19.8 SR-3 x64]</li>
                                <li>[Zusatzordner zur Installation Forensics]</li>
                            </ul>
                            </li>
                            <li>[XWI-Versionen] (optional, bzw. für Auswerterechner, dann ohne den XWF-Ordner)
                            <ul>
                                <li>[xwi_19.7 SR-7 x64]</li>
                                <li>[xwi_19.8 SR-3 x64]</li>
                                <li>[Zusatzordner zur Installation Investigator]</li>
                            </ul>
                            </li>
                            <li>frm_xw_config.py</li>
                            <li>frm_xw_launcher.py</li>
                            <li>frm_xw_newcase.py</li>
                            <li>X_Ways_Launcher.pyw</li>
                            <li>xways.ico</li>
                            <li>X-Ways_Launcher.lnk</li>
                        </ul>
                        </li>
                    </ul>
                    </li>
                </ul>
            </div>
        </div>
        <ul style="list-style-type: circle">
            <li>Der Launcher-Programmordner wird auf einem Netzlaufwerk abgelegt, zu dem die Sachbearbeiter Zugriffsrechte besitzen. </li>
            <li>Der Launcher-Link wird an den entsprechenden Laufwerksbuchstaben angepasst, auf den beim Benutzer das Netzlaufwerk verbunden wurde.</li>
            <li>Nun kann dieser Link von jedem Benutzer auf dessen Desktop kopiert werden.</li>
            <li>Mit dem Launcher-Icon auf dem Desktop kann der Launcher gestartet werden, es bedarf keiner weiterer Dateien auf dem Benutzer-PC.</li>
        </ul>
        <h4>am Beispiel von XWAYS Investigator (für Ermittler):</h4>
        Da es sich hierbei meist um Auswerterechner ohne Internetanbindung handeln wird, empfiehlt sich folgende Vorgehensweise:
        <ul style="list-style-type: circle">
            <li>Der [<em>XWAYS_Launcher</em>] Ordner wird unter <em>C:/Programme/</em> abgespeichert,</li>
            <li>danach kann der Unterordner [XWF-Versionen] bei Nichtbenutzung gelöscht werden, oder mit einer kleinen Modifikation der Bezeichnung _deaktiviert_ werden.</li>
            <li>Der Launcher-Link wird an den entsprechenden Laufwerksbuchstaben und Ablagepfad angepasst.</li>
            <li>Nun kann dieser Link in den Ordner c:/Users/Desktop kopiert werden und steht jedem Benutzer zur Verfügung. Dies ist notwendig, da vorzugsweise das Laufwerk C:
             für den normalen User gesperrt ist.</li>
            <li>Mit dem Launcher-Icon auf dem Desktop kann der Launcher gestartet werden, es bedarf keiner weiterer Dateien auf dem Benutzer-PC.</li>
        </ul>
    </div>
    <div>
        <h4>Hinzufügen neuer Forensics Versionen</h4>
        <ul style="list-style-type: circle">
            <li>In das [<em>XWF-Versionen</em>] Subverzeichnis wird die aktuelle Version in einem Verzeichnis mit dem Präfix <i>xwf_</i> der Versionsnummer und der gewünschten Bit-Version (x64/x32) abgespeichert/extrahiert,</li>
            <li>Der Inhalt des Verzeichnisses [<em>Zusatzordner zur Installation Forensics</em>], darin enthaltener Viewer und Player sowie ergänzende Templates, 
            werden gänzlich in das eben erstellte Verzeichnis eingefügt,</li>
            <li>die vorkonfigurierte Datei <i>WinHex.cfg</i> wird bei diesem Vorgang ebenfalls hineinkopiert, oder durch Aufruf von <i>xwforensics64.exe</i> 
            direkt aus dem neu angelegten <em>xwf_...</em>-Verzeichnis manuell konfiguriert,</li>
            <li>der nun nicht mehr aktuelle Forensics-Programmordner wird umbenannt und verliert das Präfix <i>aktuell_</i>, wenn nicht bereits programmatisch geschehen.</li>
        </ul>
        <h4>Hinzufügen neuer Investigator Versionen</h4>
        <p>mit schreibendem Zugriff auf Laufwerk C:</p>
        <ul style="list-style-type: circle">
            <li>In das [<em>XWI-Versionen</em>] Subverzeichnis wird die aktuelle Version in einem Verzeichnis mit dem Präfix <i>xwf_</i> der Versionsnummer und der gewünschten Bit-Version (x64/x32) abgespeichert/extrahiert,</li>
            <li>Der Inhalt des Verzeichnisses [<b>Zusatzordner zur Installation Investigator</b>], darin enthaltener Viewer und Player sowie ergänzende Dateien, werden gänzlich in das eben erstellte Verzeichnis eingefügt,</li>
            <li>die vorkonfigurierte Datei <i>WinHex.cfg</i> wird bei diesem Vorgang ebenfalls hineinkopiert, oder durch Aufruf von <i>xwinvestigator64.exe</i> direkt aus dem neu angelegten <b>xwi...</b>-Verzeichnis manuell konfiguriert,</li>
            <li>der nun nicht mehr aktuelle Investigator-Programmordner wird umbenannt und verliert das Präfix <i>aktuell_</i>, wenn nicht bereits programmatisch geschehen.</li>
        </ul>
        <p>mit schreibgeschütztem Laufwerk C:<br><span style="font-style: italic">in diesem Fall besteht die Option, ein zusätzliches Verzeichnis mit der Bezeichnung "X-Ways_Launcher" und einem Subordner "XWI-Versionen" neu anzulegen. (Einstellung konfigurierbar im Menu/Einstellungen/XWAlternativeSourceFolder)</span></p>
        <ul style="list-style-type: circle">
            <li>In das [<b>XWI-Versionen</b>] Subverzeichnis wird die aktuelle Version in einem Verzeichnis mit dem Präfix <i>xwf_</i> und der Versionsnummer abgespeichert/extrahiert,</li>
            <li>ein eventuell bereits vorhandener, nicht mehr aktueller Investigator-Programmordner wird umbenannt und verliert das Präfix <i>aktuell_</i></li>
            <li>Der Launcher übernimmt hier die Aufgabe, den Inhalt des Verzeichnisses [<b>Zusatzordner zur Installation Investigator</b>], den darin enthaltenen Viewer und Player, sowie ergänzende Dateien in das neue Arbeits-Verzeichnis einzufügen, sobald ein neuer Fall angelegt wird.</li>
            <li>Die vorkonfigurierte Datei <i>WinHex.cfg</i> wird bei diesem Vorgang ebenfalls vom Launcher hineinkopiert, kann jedoch im Nachhinein durch Aufruf von <i>xwinvestigator64.exe</i> direkt aus dem neu erstellten <b>xwi_...</b>-Verzeichnis manuell konfiguriert werden.</li>
        </ul>
        Der Inhalt des Verzeichnisses [<b>Zusatzordner zur Installation Investigator</b>] vom Laufwerk C:, darin enthaltener Viewer und Player sowie ergänzende Dateien, werden dann vom XWAYS_Launcher bei Bedarf/Erstellung eines neuen Falls programmseitig eingefügt. Die vorkonfigurierte Datei <i>WinHex.cfg</i> wird bei diesem Vorgang ebenfalls hineinkopiert und kann durch Aufruf 
        von <i>xwinvestigator64.exe</i> in diesem Fall aus dem "Arbeits"-Verzeichnis bei Bedarf manuell konfiguriert werden. </div>
</div>
<br><button class="accordion">Hauptfenster</button>
<div class="panel">
    <p></p>
    <button class="QPushButton">Fall öffnen&nbsp;</button>
    <ul style="list-style-type: circle">
        <li>Entsprechend der gewählten XWAYS-App wird der, aus der Liste der aktiven Fälle selektierte Fall mit dessen Einstellungen geöffnet.</li>
    </ul>
    <button class="QPushButton">Fall neu anlegen&nbsp;</button>
    <ul style="list-style-type: circle">
        <li>Das Unterfenster "Fall neu anlegen" wird aufgerufen, darin können die Einstellungen für einen neuen Fall vorgenommen, Programmordner kopiert und eventuell gleich mit der Programmausführung begonnen werden.</li>
    </ul>
    <button class="QPushButton">Falleinstellungen sichern&nbsp;</button>
    <ul style="list-style-type: circle">
        <li>Fallname und die Position der Verzeichnisse werden in der <i>Falleinstellungen_Fallname.reg</i> Datei im <i>Fall</i>-Ordner abgelegt</li>
        <li>die <i>Falleinstellungen_Fallname.reg</i> Datei kann dann zur Wiederherstellung entweder in das Aktive-Fälle-Fenster des Launchers gezogen werden, oder direkt aus dem Dateimanager per Maus-Rechts-Klick in die Registry importiert werden (wobei der Launcher über das Menü aktualisiert werden muss!)</li>
    </ul>
    <button class="QPushButton">Falleinstellungen entfernen&nbsp;</button>
    <ul style="list-style-type: circle">
        <li>Fallname wird aus der Liste entfernt und Falleinstellungen werden aus der Registry entfernt.</li>
    </ul>
    <button class="QPushButton">Launcher beenden&nbsp;</button>
    <ul style="list-style-type: circle">
        <li>nicht mehr, aber auch nicht weniger!</li>
    </ul>
</div>
<br><button class="accordion">Menu-Leiste</button>
<div class="panel">
    <h3>Einstellungen</h3>
    <p>Über den Menu-Punkt <b>Einstellungen</b> (Alt+E) wird die Möglichkeit geboten, kleinere Anpassungen des Programm-Verhaltens vorzunehmen. Die Value-Spalte (rechts) beinhaltet die änderbaren Werte für ausgewählte Einstellungen des XWAYS-Launcher:</p>
    <table>
        <tr><td style="min-width: 5%;">&nbsp;</td><td style="min-width: 20%;">AppXWI=....;</td><td style="float: left; min-width: 65%">→ kennzeichnet eine Einstellung für den XWAYS-Investigator,</td></tr>
        <tr><td style="min-width: 5%;">&nbsp;</td><td style="min-width: 20%;">AppXWF=....;</td><td style="float: left; min-width: 65%">→ kennzeichnet eine Einstellung für den XWAYS-Forensics,</td></tr>
        <tr><td style="min-width: 5%;">&nbsp;</td><td style="min-width: 20%;">FilePos=....;</td><td style="float: left; min-width: 65%">→ gibt die Position der Einstellung in der Datei <i>WinHex.cfg</i> an.</td></tr>
    </table>
    <p>Mit einem Doppelklick auf die gewünschte Zeile in der rechten Spalte lassen sich die Einstellungen bearbeiten. Sobald die Bearbeitung mit <strong>[Return]</strong> oder einem Mausklick außerhalb des Bearbeitungsfeldes beendet wird, werden die gemachten Änderungen umgehend in die Registry geschrieben und sind ab der nächsten Programmausführung, oder Aktualisierung des Launchers 
    aktiv! </p>
    <h3>Launcher aktualisieren</h3>
    <p>Wurden Änderungen der Fall-Einstellungen außerhalb des Funktionsumfangs des Launchers während dessen Ausführung vorgenommen, bietet der Menu-Punkt <b>Launcher aktualisieren</b> (Alt+A) die Möglichkeit die geänderten Werte neu einzulesen. </p>
    <ul style="list-style-type: circle">
        <li>Fall-Einstellungen werden im Hauptfenster aktualisiert,</li>
        <li>Launcher-Einstellungen werden erneut eingelesen.</li>
    </ul>
    <p></p>
    <h3>Hilfe</h3>
    <p>Diese Hilfeseite kann über den Menu-Punkt <b>Hilfe</b> (Alt+H) aufgerufen werden und steht zur Offline-Dokumentation über den Rechte-Maustaste-Klick zur Verfügung. Die Hilfeseite kann sowohl gedruckt, als auch in Form einer PDF abgespeichert werden.</p>
    <h3>Info</h3>
    <p>Der Menu-Punkt <b>Info</b> (Alt+I) gibt einen kurzen Überblick der Programmbestimmung und gibt Auskunft über Erreichbarkeit und Identität des verantwortlichen Software-Herstellers.</p>
    <h3>Launcher entfernen</h3>
    <p>Der Menu-Punkt <b>Launcher entfernen</b> (Strg+Alt+R) veranlasst die Entfernung: </p>
    <ul style="list-style-type: circle">
        <li>der Fall-Einstellungen</li>
        <li>und der Launcher-Einstellungen.</li>
    </ul>
    Es empfiehlt sich zuvor für jeden aktiven Fall die <b>[Falleinstellungen</b> zu<b> sichern]</b>. Sollten Launcher-Einstellungen beschädigt oder fehlerhaft modifiziert sein, so ist dies der einfachste Weg, die Standard-Einstellungen wieder her zu stellen!
    <p></p>
</div>
<br><button class="accordion">Fall neu anlegen</button>
<div class="panel">
    <p>Mit Hilfe dieses Dialogs bietet sich die Möglichkeit bereits bestehende Verzeichnisse (z.Bsp. Hash-Datenbanken) einzubinden und bequem notwendige Verzeichnisse am gewünschten Ort und nach vereinbarter Namenskonvention anzulegen, ohne dazu lokal eine "Ausgangs"-Kopie aller Arbeitsverzeichnisse vorzuhalten: </p>
    <ul style="list-style-type: circle">
        <li>Die X-Ways Programmversion kann im Kombinationsfeld ausgewählt werden, oder der bereits vorselektierte Eintrag beibehalten werden. Es werden alle Verzeichnisse aufgelistet, die eine ausführbare X-Ways Investigator/Forensics Programmdatei beinhalten. <i>aktuell-</i>-benannten Ordner erscheinen dabei in den ersten Zeilen der Auswahlliste.</li>
        <li>Die Fallbezeichnung sollte sich nach der vereinbarten Namenskonvention der Dienststelle richten, um schnelles Wiederauffinden und verwechslungsfreies Weitergeben der angelegten Fall-Daten zu gewährleisten.</li>
        <li>Fall-Programmverzeichnis ist der Ort, an den der X-Ways-Programmordner kopiert wird. Dieser wird entsprechend den Launcher-Einstellungen standardmäßig in <i>10_XWF</i> für die Forensics-Version, und <i>10_Programm_XWI</i> umbenannt. Die nachfolgenden Verzeichnis-Felder werden zunächst mit der gemachten Auswahl für das Fall-Programmverzeichnis vorbelegt, können dann 
        aber in der Folge angepasst werden.</li>
        <li>Der in das Verzeichnis-Feld für das HashDB-Verzeichnis geschriebene Pfad, wird für die Erstellung des HashDB2-Verzeichnisses herangezogen.</li>
        <li>Der in das Verzeichnis-Feld für das HashDB_PDNA-Verzeichnis geschriebene Pfad, wird ebenfalls für die Erstellung des FuZzYDoc-Hash-Verzeichnisses herangezogen.</li>
    </ul>
    <p></p>
    <p>Ein Maus-Links-Klick auf einen der rechts stehenden <b>[...]</b>-Buttons öffnet ein Verzeichnisauswahl-Dialog, zur bequemeren Verzeichniswahl. Noch nicht vorhandene Verzeichnisse können dann entweder mit Hilfe des Dialogs erstellt werden, oder nach der Auswahl manuell im Verzeichnis-Feld nachergänzt werden.</p>
    <p>Bevor die Verzeichnisse angelegt werden können, muss sichergestellt werden, dass alle Dialogfelder befüllt und keine gelb-markiert sind, der Launcher gibt darüber entsprechende Auskunft. Grün-markierte Felder sind syntaktisch richtig belegt und können so im Dateisystem angelegt werden.</p>
</div>
<br><button class="accordion">nützliche Infos</button>
<div class="panel">
    <h3>Fall anlegen:</h3>
    <ul style="list-style-type: circle">
        <li>Es wird ein bereits vorkonfiguriertes XWAYS-Verzeichnis mit Standardeinstellungen in den neuen Ziel-Ordner kopiert.</li>
        <li>Die vorgenommenen Verzeichniseinstellungen werden in die <i>WinHex.cfg</i> im Ziel-XWAYS-Ordner geschrieben, somit entfällt für den Benutzer die Notwendigkeit, für jeden Fall manuell im Menu die gewünschten Einstellungen vorzunehmen!</li>
    </ul>
    <p>Auszug aus der Dokumentation zu XWAYS Forensics: (Übersetzung) </p>
    <ul style="list-style-type: circle">
        <li>Wenn möglich, Fall-Daten und Disk-Images nicht auf demselben physikalischen Speichermedium ablegen,</li>
        <li>Wenn möglich, temporäre Dateien und Disk-Images nicht auf demselben physikalischen Speichermedium ablegen</li>
        <li>Wenn möglich, Disk-Images auf einem RAID-Laufwerk ablegen, um höhere Lesegeschwindigkeit zu erreichen.</li>
    </ul>
    Es wird Seitens des Herstellers empfohlen, Fall-Daten und Temp-Verzeichnis auf jeweils separaten SSD-Laufwerken und die Disk-Images auf dem RAID-Volume einzurichten!
    <p></p>
</div>
<br><button class="accordion">sonstige Infos</button>
<div class="panel">
    <h3>Vom Launcher angelegte Errorlog-Dateien</h3>
    <p>Bei der Vielzahl an Konfigurationen für Auswerte-PCs und Software-Versionsständen besteht die Notwendigkeit der Fehlerprotokollierung für den Launcher. Die Logdateien geben wertvolle Auskunft über den letzten Inhalt der aktuellen Programmparameter und der Fehler, die bei der Ausführung des Launchers aufgetreten sind. <br>Diese Logdateien werden aktuell im AppData-Verzeichnis 
    des ausführenden Benutzers abgelegt. Pro Tag wird einmalig eine Logdatei angelegt, darin die Log-Meldungen abgelegt. Die Log-Dateien reichen 30 Tage zurück, bevor sie automatisch gelöscht werden.</p>
</div>
<footer style="color: # eee; text-align: center;">Ansprechpartner: Helmut Schmidt, schmi171@polzei.bwl.de, 2018</footer>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;for (i = 0; i < acc.length; i++) {acc[i].addEventListener("click", function(){this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight)
        {panel.style.maxHeight = null;}
    else {panel.style.maxHeight = panel.scrollHeight + "px";}});}
</script>
</body></html>

        """
        self.view = HelpfileWebView(self.sHTML)  # QWebEngineView()
        self.view.setWindowIcon(self.icon)
        self.view.setWindowTitle('XWays-Launcher Hilfe')
        self.view.show()

    def _handleMenuUpdate(self):
        logger_handleMenuUpdate = getLogger('XWAYS_Launcher.XWLauncher._handleMenuUpdate{sig}  '.format(sig=inspect.signature(self._handleMenuUpdate)))
        logger_handleMenuUpdate.debug('Beginn _handleMenuUpdate {0}'.format(''))

        try:
            self.updateInternals()
            self.statusbar.showMessage('Launcher-Einstellungen wurden aktualisiert!'.format(''), 4000)
            logger_handleMenuUpdate.debug('Launcher-Einstellungen wurden aktualisiert! {0}'.format(''))
        except Exception:
            self.statusbar.showMessage('Launcher-Einstellungen wurden nicht aktualisiert! Es trat ein Fehler auf: {}'.format(exc_info()[0]), 4000)
            logger_handleMenuUpdate.exception('Launcher-Einstellungen wurden nicht aktualisiert! Es trat ein Fehler auf: {0}'.format(exc_info()[0]))
            return

    def _ClearListWidget(self):  # INFO: fertig
        self.List1.clear()

    def _handleMenuInfoBox(self):
        logger_handleMenuInfoBox = getLogger('XWAYS_Launcher.XWLauncher._handleMenuInfoBox{sig}'.format(sig=inspect.signature(self._handleMenuInfoBox)))
        logger_handleMenuInfoBox.debug('Beginn _handleMenuInfoBox {0}'.format(''))
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowIcon(self.icon)
        msg.setText(self.sInfoBoxText + '\n\n-Version: ' + __version__.replace('$', ''))
        msg.setWindowTitle(self.sInfoBoxTitle)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.setTextInteractionFlags(Qt.TextSelectableByMouse)
        logger_handleMenuInfoBox.debug(msg.exec_())
        logger_handleMenuInfoBox.debug('Ende _handleMenuInfoBox')

    def _handleMenuColorSchema(self):
        logger_handleMenuInfoBox = getLogger('XWAYS_Launcher.XWLauncher._handleMenuColorSchema{sig}'.format(sig=inspect.signature(self._handleMenuColorSchema)))
        logger_handleMenuInfoBox.debug('Beginn _handleMenuColorSchema {0}'.format(''))
        if self.windowXWColorSchema is None:
            self.windowXWColorSchema = XWColorSchemaDlg(self)
            self.windowXWColorSchema.setFixedSize(550, 400)
        self.windowXWColorSchema.show()
        # self.windowXWColorSchema.close_signal.connect(self.windowXWNewCase_popup_closed)

    def _handleUpdateCaseToolTips(self):
        logger__handleUpdateCaseToolTips = getLogger('._handleUpdateCaseToolTips{sig}'.format(sig=inspect.signature(self._handleUpdateCaseToolTips)))
        logger__handleUpdateCaseToolTips.debug('Beginn _handleUpdateCaseToolTips {0}'.format(''))

        try:
            if not self.List1.selectedItems():
                logger__handleUpdateCaseToolTips.debug('Kein Fall als Ziel für ToolTips-Aktualisierung, vielleicht nichts ausgewählt!: {0!s}'.format(''))
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("Fehler beim aktualisieren der Tooltips")
                msg.setText("Bitte in der Fall-Liste einen Eintrag auswählen,\ndann erneut versuchen!")
                msg.setStandardButtons(QMessageBox.Ok)
                return msg.exec_()
            else:
                logger__handleUpdateCaseToolTips.debug('ToolTips aktualisieren: {0!s}'.format(self.List1.currentItem().text()))
        except SyntaxError as synterr:
            logger__handleUpdateCaseToolTips.debug('ToolTips aktualisieren: Fehler:{0!s}\nWahrscheinlich kein Fall(Verzeichnis) zum aktualisiseren ausgewählt'.format(synterr))
            logger__handleUpdateCaseToolTips.exception('Ein Fehler ist aufgetreten: {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle("Fehler beim aktualisieren der ToolTips")
            msg.setText("Bitte in der Fall-Liste eine Installation auswählen, deren ToolTips aktualisiert werden sollen,\ndann erneut versuchen!")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
            return synterr
        sZielPfad = path.join(self.Label_XWAYSPath.text())
        sQuellPfad = path.join(gldictLauncher['XWToolTipLocation']['AppXWF'], 'Tooltips.txt')
        try:
            copy(sQuellPfad, sZielPfad)
            self.statusbar.showMessage('Tooltips.txt wurde gesichert nach : {0}'.format(sZielPfad), 4000)
            logger__handleUpdateCaseToolTips.debug('ToolTips erfolgreich gesichert: \nQuelle:{0!s}\nZiel: {1}'.format(sQuellPfad, sZielPfad))
        except:
            self.statusbar.showMessage('Tooltips.txt konnte nicht gesichert werden: {0}:{1}'.format(exc_info()[0], exc_info()[1]), 4000)
            logger__handleUpdateCaseToolTips.exception('Tooltips.txt konnte nicht gesichert werden, ein Fehler ist aufgetreten: {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            pass

    def _handleUpdatePersonalToolTips(self):
        logger__handleUpdatePersonalToolTips = getLogger('._handleUpdatePersonalToolTips{sig}'.format(sig=inspect.signature(self._handleUpdatePersonalToolTips)))
        logger__handleUpdatePersonalToolTips.debug('Beginn _handleUpdatePersonalToolTips {0}'.format(''))

        try:
            if not self.List1.selectedItems():
                logger__handleUpdatePersonalToolTips.debug('Keine ToolTips zum sichern, vielleicht nichts ausgewählt!: {0!s}'.format(''))
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("Fehler beim sichern der Tooltips")
                msg.setText("Bitte in der Fall-Liste einen Eintrag auswählen,\ndann erneut versuchen!")
                msg.setStandardButtons(QMessageBox.Ok)
                return msg.exec_()
            else:
                logger__handleUpdatePersonalToolTips.debug('ToolTips sichern: {0!s}'.format(self.List1.currentItem().text()))
        except SyntaxError as synterr:
            logger__handleUpdatePersonalToolTips.debug('ToolTips sichern: Fehler:{0!s}\nWahrscheinlich kein Fall(Verzeichnis) zum sichern ausgewählt'.format(synterr))
            logger__handleUpdatePersonalToolTips.exception('Ein Fehler ist aufgetreten: {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle("Fehler beim Sichern der ToolTips")
            msg.setText("Bitte in der Fall-Liste eine Installation auswählen, deren ToolTips gesichert werden sollen,\ndann erneut versuchen!")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
            return synterr
        sQuellPfad = path.join(self.Label_XWAYSPath.text(), 'Tooltips.txt')
        sZielPfad = path.join(gldictLauncher['XWToolTipLocation']['AppXWF'])
        if not path.exists(sZielPfad):
            makedirs(sZielPfad)
        try:
            copy(sQuellPfad, sZielPfad)
            self.statusbar.showMessage('Tooltips.txt wurde gesichert nach : {0}'.format(sZielPfad), 4000)
            logger__handleUpdatePersonalToolTips.debug('ToolTips erfolgreich gesichert: \nQuelle:{0!s}\nZiel: {1}'.format(sQuellPfad, sZielPfad))
        except:
            self.statusbar.showMessage('Tooltips.txt konnte nicht gesichert werden: {0}:{1}'.format(exc_info()[0], exc_info()[1]), 4000)
            logger__handleUpdatePersonalToolTips.exception('Tooltips.txt konnte nicht gesichert werden, ein Fehler ist aufgetreten: {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            pass

    def updateInternals(self):
        dictReturn = defReadCaseSettings(glsCaseSettingsStorage)
        self.List1.clear()
        self.List1.addItems(dictReturn['SectionList'])
        return dictReturn

    def windowXWConfig_popup_closed(self):
        """ Cleanup the popup widget here """
        logger_windowXWConfig_popup_closed = getLogger('XWAYS_Launcher.XWLauncher.windowXWConfig_popup_closed{sig}  '.format(sig=inspect.signature(self.windowXWConfig_popup_closed)))
        # logger_windowXWConfig_popup_closed.debug('Beginn  windowXWConfig_popup_closed: {0}'.format(''))
        self.windowXWConfig = None
        logger_windowXWConfig_popup_closed.debug('Ende windowXWConfig_popup_closed:  XWConfig instanz geschlossen'.format(''))

    def windowXWNewCase_popup_closed(self):
        """ Cleanup the popup widget here """
        logger_windowXWNewCase_popup_closed = getLogger('XWAYS_Launcher.XWLauncher.windowXWNewCase_popup_closed{sig}  '.format(sig=inspect.signature(self.windowXWNewCase_popup_closed)))
        # logger_windowXWNewCase_popup_closed.debug('Beginn  windowXWNewCase_popup_closed: {0}'.format(''))
        # TODO: beim vorzeitigen Abbruch des Kopiervorgangs und einem darauf folgenden manuellen Schließen des NewCase-Fensters wird ein RuntimeError im modul XWFileCopyProgress.progress() aufgeworfen:
        # INFO: XWAYS_Launcher.XWFileCopyProgress.progress(done, total): DEBUG    Zeile:486          Ein Fehler ist aufgetreten: wrapped C/C++ object of type QProgressBar has been deleted:<class 'RuntimeError'>
        # INFO: Fehler ist damit verbunden, dass die GarbageCollection beim Löschen von NewCase, das Child FileCopyProgress nicht richtig aufräumt, hir muss nachgeforscht werden!
        self.windowXWNewCase = None
        logger_windowXWNewCase_popup_closed.debug('Ende windowXWNewCase_popup_closed:  XWNewCase instanz geschlossen'.format(''))


class HelpfileWebView(PyQt5.QtWebEngineWidgets.QWebEngineView):
    def __init__(self, sHTML, parent=None):
        super(HelpfileWebView, self).__init__(parent)
        self.sHilfe = sHTML
        self.setZoomFactor(1)
        self.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.page().setHtml(self.sHilfe)
        self.printer = QPrinter(QPrinter.HighResolution)
        self.printer.setPageSize(QPrinter.A4)
        self.printer.setOrientation(QPrinter.Portrait)
        self.print_action = QAction("Drucken", self)
        self.printPDF_action = QAction("PDF erstellen", self)
        self.print_action.triggered.connect(self.print_helppage)
        self.printPDF_action.triggered.connect(self.printPDF_helppage)
        self.addAction(self.print_action)
        self.addAction(self.printPDF_action)

    def print_helppage(self):
        """Print the webpage to a printer.
        Callback for the print action.
        Should show a print dialog and print the webpage to the printer.
        """
        logger_print_helppage = getLogger('XWAYS_Launcher.HelpfileWebView.print_helppage{sig}'.format(sig=inspect.signature(self.print_helppage)))
        logger_print_helppage.debug('Beginn print_webpage {0}'.format(''))

        if self.printer.isValid():
            doc = QTextDocument()
            doc.setHtml(self.sHilfe.replace('background-color: # eee', 'background-color: # fff', 20).replace('<button class="accordion">', '<h2>', 20).replace('</button>', '</h2>', 20).replace('<h2>nützliche', '<br><br><br><h2>nützliche', 1))
            doc.print_(self.printer)
            logger_print_helppage.debug('Ende print_helppage nach print_-Befehl{0}'.format(''))
            return True
        else:
            logger_print_helppage.debug('Ende print_helppage ohne zu drucken{0}'.format(''))
            return False

    def printPDF_helppage(self):
        logger_printPDF_webpage = getLogger('XWAYS_Launcher.HelpfileWebView.printPDF_helppage{sig}  '.format(sig=inspect.signature(self.printPDF_helppage)))
        logger_printPDF_webpage.debug('Beginn printPDF_helppage {0}'.format(''))
        options = QFileDialog.Options()
        fn, xx = QFileDialog.getSaveFileName(self.nativeParentWidget(), "Export PDF", 'D:/', "PDF files (*.pdf);;All Files (*)", options=options)
        if fn:
            if not fn.endswith('.pdf'):
                fn += '.pdf'
            self.printer.setOutputFileName(fn)
            self.printer.setOutputFormat(QPrinter.PdfFormat)
            logger_printPDF_webpage.debug('printPDF_helppage nach {0}'.format(self.printer.outputFileName()))

            strTempHtml = self.sHilfe.replace('background-color: # eee;', 'background-color: # fff;', 20)
            strTempHtml = strTempHtml.replace('<button class="accordion">', '<h2>', 20)
            strTempHtml = strTempHtml.replace('<button class="QPushButton">', '<h3>', 20)
            strTempHtml = strTempHtml.replace('&nbsp;</button>', '</h3>', 20)
            strTempHtml = strTempHtml.replace('</button>', '</h2>', 20)
            strTempHtml = strTempHtml.replace('<header style="color: # eee;', '<header style="color: # 777;')
            strTempHtml = strTempHtml.replace('<footer style="color: # eee;', '<footer style="color: # 777;')
            strTempHtml = strTempHtml.replace('<h2>nützliche', '<br><br><h2>nützliche', 1)
            doc = QTextDocument()
            doc.setHtml(strTempHtml)
            doc.print_(self.printer)
        else:
            logger_printPDF_webpage.debug('printPDF_helppage Dateiname unvollständig: {0}'.format(self.printer.outputFileName()))
            return


class Switch:
    def __init__(self, argument): self._val = argument

    def __enter__(self): return self

    def __exit__(self, ttype, argument, traceback): return False  # Allows traceback to occur

    def __call__(self, cond, *mconds): return self._val in (cond,) + mconds

    # Note there is no break needed here
    # This switch also supports multiple conditions (in one line)
    # with Switch(datetime.today().weekday()) as case:
    #     if case(0):
    #         # Basic usage of switch
    #         print("I hate mondays so much.")
    #         # Note there is no break needed here
    #     elif case(1,2):
    #         # This switch also supports multiple conditions (in one line)
    #         print("When is the weekend going to be here?")
    #     elif case(3,4): print("The weekend is near.")
    #     else:
    #         # Default would occur here
    #         print("Let's go have fun!") # Didn't use case for example purposes


class Abbr(object):
    def __init__(self, **kwargs):
        self.abbrs = kwargs
        self.store = {}

    def __enter__(self):
        for key, value in self.abbrs:
            try:
                self.store[key] = globals()[key]
            except KeyError:
                pass
            globals()[key] = value

    def __exit__(self, *args, **kwargs):
        for key in self.abbrs:
            try:
                globals()[key] = self.store[key]
            except KeyError:
                del globals()[key]

    # usage:
    # with Abbr(h=buildingList[foundIndex].height, g=gravitationalConstant):
    #     fallTime = sqrt(2 * h / g)
    #     endSpeed = sqrt(2 * h * g)
    # print("Fall time:", fallTime)
    # print("End speed:", endSpeed)


def _count_file(fn):
    with open(fn, 'rb') as f:
        return _count_file_object(f)


def _count_file_object(f):
    total = 0
    for line in f:
        total += len(line)
    return total

# def defUnzipMember(zip_filepath,filename, dest):
#     with open(zip_filepath, 'rb') as f:
#         import zipfile
#         zf = zipfile.ZipFile(f)
#         zf.extract(filename, dest)
#     fn = path.join(dest, filename)
#     return _count_file(fn)
#
#
# def defUnzipUpdate(fn, dest):
#     with open(fn, 'rb') as f:
#         import zipfile
#         zf = zipfile.ZipFile(f)
#         futures = []
#         with concurrent.futures.ProcessPoolExtractor() as executor:
#             for member in zf.infolist():
#                 futures.append(executor.submit(defUnzipMember, fn, member.filename, dest,))
#             total = 0
#             for future in concurrent.futures.as_completed(futures):
#                 total += future.result()
#     return total


def defEasyUnzipUpdate(fn, dest, verbose=False):
    logger_defEasyUnzipUpdate = getLogger('XWAYS_Launcher.defEasyUnzipUpdate{sig}'.format(sig=inspect.signature(defEasyUnzipUpdate)))
    logger_defEasyUnzipUpdate.debug('Beginn defEasyUnzipUpdate(fn:{0}, dest:{1}, verbose:{2})'.format(fn, dest, verbose))

    import zipfile
    if verbose:
        zf = zipfile.ZipFile(fn)
        # uncompress_size = sum((file.file_size for file in zf.infolist()))
        # extracted_size = 0
        for file in zf.infolist():
            # extracted_size += file.file_size
            # logger_defEasyUnzipUpdate.debug('Update: {0}% entpackt)'.format(extracted_size*100/uncompress_size))
            zf.extract(file, dest)
    else:
        with open(fn, 'rb') as f:
            try:
                zf = zipfile.ZipFile(f)
                zf.extractall(dest)
                zf.close()
            except zipfile.BadZipFile as e:
                logger_defEasyUnzipUpdate.critical('ZIP-Datei nicht gültig: {}'.format(e))
                try:
                    remove(fn)
                except OSError as e:
                    logger_defEasyUnzipUpdate.critical('ZIP-Datei konnte nicht entfernt werden: {}'.format(e))
    # ByteSumme des ZIP-Inhalts berechnen (zu Testzwecken)
    total = -1
    # for root, dirs, files in walk(dest):
    #     for file_ in files:
    #         fn = path.join(dest, file_)
    #         total += _count_file(fn)
    return total        # total files extracted


def defShowUpdateInfo(strXWUpdateFileName):
    msg = QMessageBox()
    msg.setWindowModality(Qt.NonModal)
    msg.setIconPixmap(QPixmap(__src_dir__+"/xways.ico"))
    # frm_XW_Launcher.setWindowIcon(self.icon)
    msg.setIcon(QMessageBox.Information)
    msg.setWindowTitle("X-Ways Update")
    msg.setText("Es sind neue X-Ways Versionen vorhanden!\nUpdates werden entpackt und zur Verfügung gestellt.\n{}".format(''))
    # logger_handleCreateButton.exception('Ein Fehler ist aufgetreten: FileCopyProgress: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
    # logger_handleCreateButton.critical('FileCopyProgress src Pfad: {}; dst Pfad: {}'.format(path.join(self.sXWDirpath, self.comboBox.currentText()), destination))
    msg.setInformativeText(
        "Während des Updates könnte es bei der Programmausführung zu geringfügigen Verzögerungen kommen!\n"
        "Diese werden von der Geschwindigkeit des Ziellaufwerks und dem Umfang des Updates bedingt!".format(''))
    msg.setDetailedText('aktuelles Update: {}'.format(strXWUpdateFileName))
    msg.setStandardButtons(QMessageBox.Ok)
    retVal = msg.exec()
    return retVal


def getUpdate(verbose=False):

    """Verarbeitet neue XWF/I Programmversionen im ZIP Format

    Die ZIP mit der zur Installation anstehenden XWAYS Version werden im Stammverzeichnis abgelegt und beim
    Neustart des Launchers erkannt und extrahiert.
    Dabei wird zwischen XWF und XWI und der jeweiligen Programmversion identifiziert.
    Die Zusatzinhalte werden nach der DEkomprimierung hinzugefügt.
    ZIP Datei wird anschließend entfernt.
    """
    logger_getUpdate = getLogger('XWAYS_Launcher.getUpdate{sig}'.format(sig=inspect.signature(getUpdate)))
    logger_getUpdate.debug('Beginn getUpdate( verbose: {0})'.format(verbose))
    zip_file = ''
    unzip_dest = ''
    former_aktuell = ''
    deleteDir = ''

    for dirName, dirsList, files in walk(__src_dir__, topdown=True):
        for file_ in files:
            if file_.endswith('zip'):
                zip_file = path.join(dirName, file_)
                defShowUpdateInfo(file_)
                if file_.rfind('xwf') == 0:
                    unzip_dest = path.join(__src_dir__, gldictLauncher['XWFquelle'].get('AppXWF'))
                elif file_.rfind('xwi') == 0:
                    unzip_dest = path.join(__src_dir__, gldictLauncher['XWFquelle'].get('AppXWI'))
                elif file_.rfind('delete_') == 0:
                    if 'xwf' in file_:
                        xwver = 'AppXWF'
                    if 'xwi' in file_:
                        xwver = 'AppXWF'
                    deleteDir = path.join(__src_dir__, gldictLauncher['XWFquelle'].get(xwver), file_.replace('delete_', '').replace('.zip', ''))
                    try:
                        rmtree(deleteDir)
                        logger_getUpdate.debug('Verzeichnis gelöscht: {}'.format(deleteDir))
                    except OSError:
                        logger_getUpdate.debug('Fehler beim Löschen des Verzeichnisses {}: {}'.format(exc_info()[0], exc_info()[1]))
                        pass
                    try:
                        remove(file_)
                        logger_getUpdate.debug('Datei gelöscht: {}'.format(file_))
                    except OSError:
                        logger_getUpdate.debug('Fehler beim Löschen der Datei {}: {}'.format(exc_info()[0], exc_info()[1]))
                        pass
                    unzip_dest = ''
                else:
                    unzip_dest = ''
                    logger_getUpdate.debug('ZIP Datei nicht berücksichtigt: {}'.format(zip_file))
                    continue
                if unzip_dest != '':
                    for dirNameXW in listdir(unzip_dest):
                        # umbenennen des früheren aktuell_Verzeichnisses gemäß Standardbenennung
                        if glDirectoryPrefix in dirNameXW:
                            renamePath = path.join(unzip_dest, dirNameXW)
                            try:
                                rename(renamePath, renamePath.replace(glDirectoryPrefix, ''))
                            except:
                                logger_getUpdate.debug('Fehler beim Umbenennen: {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            else:
                # logger_getUpdate.debug('Keine Update Dateien gefunden'.format(verbose))
                unzip_dest = ''
                pass

            if unzip_dest != '':

                strUnzipZielPfad = path.join(unzip_dest, path.basename(zip_file.replace('.zip', '')))
                intTotalUnzippedFiles = defEasyUnzipUpdate(zip_file, strUnzipZielPfad, verbose)
                logger_getUpdate.debug('{0} Bytes aus ZIP Datei: {1} \nnach {2} entpackt.'.format(intTotalUnzippedFiles, zip_file, strUnzipZielPfad))

                # Info: es werden nicht beliebige zip gelöscht, nur die mit xwf_/xwi_ im Dateinamen, denn nur dann wird zip_dest befüllt.
                if path.isfile(zip_file):
                    try:
                        remove(zip_file)
                    except:
                       pass

    return unzip_dest, zip_file


def defWriteConfigToBinFile(destinationFilename, listSettings, listCase, xwversion='AppXWF', intBinVer=64):  # INFO: fertig
    # INFO: ab V19.7 ist der Standardpfad x64/x32 für den Viewer immer '.'='.\\x64\\viewer' bzw. = '.\\Viewer', dies bei abweichenden Pfaden beachten und durch relative Pfade abfangen: '..\\..\\..\\12_viewer\\x64\\viewer'
    # Info: der Viewer-Pfad befindet sich ab 19.7 an der Position des 32bit Verzeichnisses, die Position des 64bit Verzeichnisses wird weder vom XWF noch vom XWI verwendet.
    logger_defWriteConfigToBinFile = getLogger('XWAYS_Launcher.defWriteConfigToBinFile{sig}'.format(sig=inspect.signature(defWriteConfigToBinFile)))
    logger_defWriteConfigToBinFile.debug('Beginn defWriteConfigToBinFile("{0}")'.format(destinationFilename))
    try:
        f = open(destinationFilename, 'r+b')  # Open for reading and writing binary filestream
        logger_defWriteConfigToBinFile.debug('Erfolgreiches Öffnen und Lesen({0}) der Datei {1}'.format(f.writable(), destinationFilename))
    except IOError as ioerr:
        logger_defWriteConfigToBinFile.exception('Fehler {0} beim Öffnen und Lesen der Datei {1}'.format(ioerr, destinationFilename))
        logger_defWriteConfigToBinFile.exception('Ein Fehler ist aufgetreten: {0}:{1}'.format(exc_info()[0], exc_info()[1]))
        return ioerr

    sTermChar = listSettings['TerminationChar'][xwversion]
    # intXWVersion = int(defDateieigenschaften_auslesen(path.join(path.dirname(destinationFilename), gldictLauncher['XWAYSexe'+str(intBinVer)].get(xwversion))).get['Version'])
    floatXWVersion = 0
    try:
        strPath = path.join(path.dirname(destinationFilename), gldictLauncher['XWAYSexe' + str(intBinVer)].get(xwversion))
        fixedInfo = win32api.GetFileVersionInfo(strPath, '\\')
        fileVersion = '%d.%d' % (fixedInfo['FileVersionMS'] /65536, fixedInfo['FileVersionMS'] %65536)      # , fixedInfo['FileVersionLS'] /65536, fixedInfo['FileVersionLS'] %65536)
        floatXWVersion = float(fileVersion)
        logger_defWriteConfigToBinFile.debug('FileVersion of EXE: {}'.format(fileVersion))
    except:
        logger_defWriteConfigToBinFile.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))

    for Key, Setting in listSettings.items():
        logger_defWriteConfigToBinFile.debug('Key: {0}, Setting: {1}'.format(Key, Setting))

        # TODO: sollte es sich bei der strXWApp/xwversion um eine 64bit Variante ab Version 19.7 handeln, dann gldictLauncher[FolderNameViewer64][strXWApp]= '.'    #FolderNameViewer64,FolderNameViewer32
        # TODO: Unterscheidung, welche Bit-Version 32/64 installiert wird, soll sich aus dem Suffix des Quellinstallationspfades ergeben: 'xwf_19.7SR-0x64', diese Unterscheidung noch Implementieren!
        if Key == 'FolderNameViewer64':
            if intBinVer == 64:
                if floatXWVersion >= 19.7:
                    if xwversion == 'AppXWF':
                        if gldictLauncher[Key].get(xwversion) == '.\\x64\\viewer':
                            gldictLauncher[Key][xwversion] = '.'
                    # if xwversion == 'AppXWI':
                        # if gldictLauncher[Key].get(xwversion) == '..\\12_viewer\\x64\\viewer':
                            # gldictLauncher[Key][xwversion] = '..\\..\\..\\12_viewer\\x64\\viewer'
                    gldictLauncher[Key]['FilePos'] = gldictLauncher['FolderNameViewer32']['FilePos']
                # sonst weiter wie bisher
            else:
                continue        # wenn keine 64bit Anwendung
        if Key == 'FolderNameViewer32':
            if intBinVer == 32:
                if floatXWVersion >= 19.7:
                    if xwversion == 'AppXWF':
                        if gldictLauncher[Key].get(xwversion) == '.\\viewer':
                            gldictLauncher[Key][xwversion] = '.'
                    # if xwversion == 'AppXWI':
                        # if gldictLauncher[Key].get(xwversion) == '..\\12_viewer':
                            # gldictLauncher[Key][xwversion] = '..\\..\\..\\12_viewer'
                # sonst weiter wie bisher
            else:
                continue        # wenn keine 32bit Anwendung
            # XWF32 verwendet ab 19.7 den '.'-Viewerpfad und wechselt selbstständig in das 32bit Verzeichnis
            # da 32 und 64bit die selbe Position in der Configurationsdatei für den Pfad verwenden, wird hier keine Änderung mehr vorgenommen.

        if Setting['FilePos'] != '0' and Setting['FilePos'] != 0:  # nur bei Keys mit einer Filepos geht es hier weiter.
            fieldlength = len(Setting[xwversion])  # fieldlength = len(listSettings['NameFolder' + folder[0]][xwversion])
            logger_defWriteConfigToBinFile.debug('fieldlength = len(Setting[xwversion]): {0}, Setting[xwversion]: {1}'.format(fieldlength, Setting[xwversion]))
            sCasePath = ''
            sCodec = 'utf-16_le'
            try:
                if fieldlength < 96:  # INFO: meistens wird utf16 mit 192 bytes speicherplatz verwendet, außer bei FolderNameFavMediaPlayer
                    try:  # wenn es in der listCase einen entsprechenden eintrag gibt, dann wird dieser zugewiesen
                        if Key.replace('FolderName', '') in listCase:
                            sCasePath = listCase[Key.replace('FolderName', '')]
                            logger_defWriteConfigToBinFile.debug('sCasePath = listCase[Key.replace("FolderName", "")]: {0}, Key.replace("FolderName", ""): {1}'.format(sCasePath, Key.replace("FolderName", "")))
                        else:
                            sCasePath = Setting[xwversion]
                            logger_defWriteConfigToBinFile.debug('Key.replace("FolderName", "") {0} nicht in listCase[]: {1} mit ListSettings.Key:{2} -> verwende Werte aus gldictSettings'.format(Key.replace('FolderName', ''), sCasePath, Key))
                    except KeyError as kerr:  # sollte was schiefgehen, wird der Standardpfad verwendet.
                        if sCasePath == '':
                            sCasePath = Setting[xwversion]
                            logger_defWriteConfigToBinFile.exception('Error: {0}, sCasePath nicht in listCase[]: {1} mit Key:{2} -> verwende Werte aus gldictSettings'.format(kerr, sCasePath, Key))
                            logger_defWriteConfigToBinFile.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
                    if Key == 'FolderNameHashDB2':
                        sCasePath = listCase['HashDB'].replace(listSettings['FolderNameHashDB'][xwversion].replace('..\\', ''), listSettings['FolderNameHashDB2'][xwversion].replace('..\\', ''))
                        logger_defWriteConfigToBinFile.info('sCasePath: {0} mit Key:{1}'.format(sCasePath, Key))
                    if Key == 'FolderNameHashDBFuzzy':
                        sCasePath = listCase['HashDBPDNA'].replace(listSettings['FolderNameHashDBPDNA'][xwversion].replace('..\\', ''), listSettings['FolderNameHashDBFuzzy'][xwversion].replace('..\\', ''))
                        logger_defWriteConfigToBinFile.info('sCasePath: {0} mit Key:{1}'.format(sCasePath, Key))
                    if Key == 'FolderNameFavMediaPlayer':  # VavPlayer ist nicht in der listCase, hat aber einen Configfile eintrag, deshalb standard-einstellung, jedoch als utf-8/ascii, nicht als utf16
                        sCasePath = listSettings[Key][xwversion]  # .encode()
                        sCodec = 'utf-8'
                        logger_defWriteConfigToBinFile.info('sCasePath = listSettings[Key][xwversion]: {0} mit Key:{1} und sCodec: {2}'.format(sCasePath, Key, sCodec))
                    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
                    if Setting[xwversion] != '':
                        try:
                            sStr = sCasePath.encode(sCodec, 'unicode-escape')
                            logger_defWriteConfigToBinFile.info('sStr = sCasePath.encode(sCodec, "unicode-escape"): {0} mit sCodec:{1}'.format(sStr, sCodec))
                        except ValueError as verr:
                            logger_defWriteConfigToBinFile.debug('Fehler: {0}\n sStr = sCasePath.encode("utf-16_le", "unicode-escape") konnte nicht für den sCasePath {1} ausgeführt werden.\n sStr = b"" wurde für den Key {2} gesetzt'.format(
                                    verr, sCasePath, Key))
                            logger_defWriteConfigToBinFile.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                            sStr = b''
                    else:
                        sStr = b''

                    iStart = int(Setting['FilePos'])
                    if sCodec == 'utf-8':
                        sStr += b'\x00' * (95 - len(sStr))
                        logger_defWriteConfigToBinFile.info("sStr += b'\\x00\\x00' * (95 - len(sStr)): {0} mit sCodec:{1}".format(sStr, sCodec))
                    else:
                        sStr += sTermChar
                        logger_defWriteConfigToBinFile.info('sStr += sTermChar.encode(sCodec): {0} mit sCodec:{1}'.format(sStr, sCodec))
                    try:
                        f.seek(iStart)
                        f.write(sStr)
                        logger_defWriteConfigToBinFile.info('ConfigFile an der Position {0} geändert: Key: {1}, sCasePath: {2}, sStr: {3}'.format(iStart, Key, sCasePath, sStr))
                    except IOError as err:
                        logger_defWriteConfigToBinFile.debug('Fehler: {0}\nConfigFile konnte nicht an der Position {1} geändert werden \n Key: {2}, sCasePath: {3}, sStr: {4}'.format(err, iStart, Key, sCasePath, sStr))
                        logger_defWriteConfigToBinFile.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                else:
                    logger_defWriteConfigToBinFile.debug('Stringlänge überschreitet die maximale Länge: {0} von reservierten 96 Zeichen (utf-16)'.format(fieldlength))
                    return fieldlength
            except Exception:
                logger_defWriteConfigToBinFile.exception('Fehler zuletzt bei der Position {0} Key: {1}, sCasePath: {2}, sStr davor: {3}'.format(Setting['FilePos'], Key, sCasePath, sStr))
                logger_defWriteConfigToBinFile.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
    logger_defWriteConfigToBinFile.debug('Ende defWriteConfigToBinFile {0}'.format(''))
    f.close()


# def is_admin():
#     try:
#         from win32comext.shell.shell import IsUserAnAdmin
#         return IsUserAnAdmin()
#     except AttributeError:
#         return False


# def run_as_admin(argv=None, debug=False):
#     from win32comext.shell.shell import ShellExecuteEx, IsUserAnAdmin
#     from win32comext.shell import shellcon
#     from win32con import SW_SHOWNORMAL
#     if argv is None and IsUserAnAdmin():
#         return True
# 
#     import sys
#     if argv is None:
#         argv = sys.argv
#     if hasattr(sys, '_MEIPASS'):
#         # Support pyinstaller wrapped program.
#         arguments = argv[1:]
#     else:
#         # arguments = argv
#         argument_line = ' '.join(sys.argv)
#     # executable = sys.executable
#     if debug:
#         print('Command line: ', executable, argument_line)
# 
#     ret = ShellExecuteEx(nShow=SW_SHOWNORMAL,
#                          fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
#                          lpVerb='runas',
#                          lpFile=executable,
#                          lpParameters=" ".join(['"%s"' % (x,) for x in sys.argv]))  # ShellExecuteEx(None, u"runas", executable, argument_line, None, 1)
#     if int(ret['hInstApp']) <= 32:
#         return False
#     return None


def defDeleteREG_Settings(boolInvestigator=False):
    logger_defDeleteREG_Settings = getLogger('XWAYS_Launcher.defDeleteREG_Settings{sig}'.format(sig=inspect.signature(defDeleteREG_Settings)))
    logger_defDeleteREG_Settings.debug('Beginn defDeleteREG_Settings {0}'.format(''))

    if boolInvestigator:
        sRegPath = glsRegXWIPath
    else:
        sRegPath = glsRegXWFPath

    aKeyValue = winreg.OpenKey(glsRegRoot, sRegPath, 0, winreg.KEY_ALL_ACCESS)
    try:
        infokey = winreg.QueryInfoKey(aKeyValue)
        for regValueIndex in range(infokey[1]):
            try:
                winreg.DeleteValue(aKeyValue, winreg.EnumValue(aKeyValue, regValueIndex)[0])
            except OSError:
                logger_defDeleteREG_Settings.exception('regValueIndex: {0} von {1} Values wurden entfernt'.format(regValueIndex, infokey[1]))
                logger_defDeleteREG_Settings.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                pass
    except Exception:
        aKeyValue.Close()
        logger_defDeleteREG_Settings.exception('Fehler beim Löschen des Registry-Schlüssels:{}'.format('HKLM_CURRENT_USER' + '\\' + sRegPath))
        logger_defDeleteREG_Settings.exception('Ein Fehler ist aufgetreten: {0}:{1}'.format(exc_info()[0], exc_info()[1]))


def defDeleteREG_Case(sSection, boolDelAsAdmin, boolInvestigator=False):
    # INFO: sSection kann entweder ein Fallname sein, ode "LauncherSettings" wobei dann die Einstellungen (Values gelöscht werden.
    logger_defDeleteREG_Case = getLogger('XWAYS_Launcher.defDeleteREG_Case{sig}'.format(sig=inspect.signature(defDeleteREG_Case)))
    logger_defDeleteREG_Case.debug('Beginn defDeleteREG_Case {0}'.format(''))

    # INFO: aKeyValue ist keine liste
    # INFO: glsRegXWFPath = "SOFTWARE\X-Ways AG\X-Ways Forensics\X-Ways_Launcher"
    # INFO: glsRegXWIPath = "SOFTWARE\X-Ways AG\X-Ways Investigator\X-Ways_Launcher"
    # TODO: bisher werden auch investigator Fälle unter dem Forensics-Key abgelegt

    if boolInvestigator:
        sRegPath = glsRegXWIPath
    else:
        sRegPath = glsRegXWFPath

    aKeyValue = winreg.OpenKey(glsRegRoot, sRegPath, 0, winreg.KEY_ALL_ACCESS)
    # INFO: hier erfolgt das Löschen aller Einstellungen in sRegPath, bei einzelnen werten auf die UpdateREG_Settings zugreifen!!!!
    if sSection == glsRegXWLKey:  # INFO: sRegPath = key der Launchersettings
        aKeyValue = winreg.OpenKey(glsRegRoot, glsRegXWPath, 0, winreg.KEY_ALL_ACCESS)

        try:
            defDeleteREG_SubKeys2(aKeyValue, sSection)
        except Exception:
            aKeyValue.Close()
            logger_defDeleteREG_Case.exception('Fehler beim Löschen des Registry-Schlüssels:{}'.format(sSection))
            logger_defDeleteREG_Case.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
    else:
        defDeleteREG_SubKeys2(aKeyValue, sSection)  # returns keylist=[] with deleted sub/keys
        if boolDelAsAdmin:
            key = winreg.OpenKey(glsRegRoot, 'Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers', 0, winreg.KEY_ALL_ACCESS)
            retVal = 'Registry Eintrag wurde nicht gefunden'
            try:
                retVal = winreg.DeleteValue(key, path.join(gldictCase[sSection]['StandardDir'], gldictCase[sSection]['App']))
            except WindowsError as winErr:
                logger_defDeleteREG_Case.debug(
                    'Fehler beim Löschen des Registry KeyValues:{} ; Fehler: {}; retVal: {}'.format(path.join(gldictCase[sSection]['StandardDir'], gldictCase[sSection]['App']) + "=RUNASADMIN", winErr, retVal))
                # logger_defDeleteREG_Case.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                pass    # INFO: bei Investigator Installationen wird kein RUNASADMIN eingetragen, da nur USER-Berechtigungen möglich sind; somit gibt es auch nichts zu löschen und es kann weiter gemacht werden
            winreg.CloseKey(key)
        del gldictCase[sSection]  # Case wird aus der Sammlung gelöscht
    return


def defDeleteREG_SubKeys2(key0, key1, key2=''):
    logger_defDeleteREG_SubKeys2 = getLogger('XWAYS_Launcher.defDeleteREG_SubKeys2{sig}'.format(sig=inspect.signature(defDeleteREG_SubKeys2)))
    logger_defDeleteREG_SubKeys2.debug('Beginn defDeleteREG_SubKeys2 {0}'.format(''))

    if key2 == '':
        currentkey = key1
    else:
        currentkey = key1 + '\\' + key2

    openkey = winreg.OpenKey(key0, currentkey, 0, winreg.KEY_ALL_ACCESS)
    infokey = winreg.QueryInfoKey(openkey)
    for x in range(infokey[0]):
        subkey = winreg.EnumKey(openkey, 0)
        try:
            winreg.DeleteKey(openkey, subkey)
            logger_defDeleteREG_SubKeys2.debug('gelöschter Key {0};  {1}'.format(currentkey, subkey))
        except Exception:
            logger_defDeleteREG_SubKeys2.exception('Fehler beim Löschen des Key {0};  {1}'.format(currentkey, subkey))
            logger_defDeleteREG_SubKeys2.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            defDeleteREG_SubKeys2(key0, currentkey, subkey)
    winreg.DeleteKey(openkey, '')
    openkey.Close()
    logger_defDeleteREG_SubKeys2.debug('gelöschter Key {}'.format(currentkey))
    return


def defDeleteReg(self, strPath):        # TODO: noch anpassen falls nötig, RAW_CODE, nicht in gebrauch!!!
    # delete a key and subkeys, if necessary
    k = self
    ps = strPath.split('/')
    for p in ps:
        if p in k.keys:
            if p == ps[-1]:
                k2 = k.parent
                if k.keys:  # delete subkeys
                    for k3 in k:
                        self.delete(strPath + '/' + str(k3))
                else:  # without subkeys
                    winreg.DeleteKey(k2.wrk, ps[-1])
            else:
                k = k.keys[p]
    return k


def defDeleteINI_Case(sSection):
    logger_defDeleteINI_Case = getLogger('XWAYS_Launcher.defDeleteINI_Case{sig}'.format(sig=inspect.signature(defDeleteINI_Case)))
    logger_defDeleteINI_Case.debug('Beginn defDeleteINI_Case {0}'.format(''))
    # hier die gldictCase ändern: den Fall sSection löschen
    # dann _WriteINI_Settings aufrufen

    del gldictCase[sSection]  # Case wird aus der Sammlung gelöscht
    iReturn = defWriteINI_Settings()
    return iReturn


def defDeleteCaseSettings(sStorage, sSection, boolDelAsAdmin=True):
    logger_defDeleteCaseSettings = getLogger('XWAYS_Launcher.defDeleteCaseSettings{sig}'.format(sig=inspect.signature(defDeleteCaseSettings)))
    logger_defDeleteCaseSettings.debug('Beginn defDeleteCaseSettings {0}'.format(''))

    # Parameter 'boolInvestigator=glboolXWAppInvestigator' derzeit ohne benutzung, da alle Cases in einem RegKey  "X-WAYS Forensics" gespeichert werden
    with Switch(sStorage) as case:
        if case('ini'):
            if sSection == 'LauncherSettings':
                return defDeleteINI_Case(sSection)  # wenn sich was ändern sollte, hier die angepasste Routine für die Settings in der INI einsetzen
            else:
                return defDeleteINI_Case(sSection)
        elif case('reg'):
            logger_defDeleteCaseSettings.debug("Registryeinträge werden entfernt!   sSection: {}, boolDelAsAdmin: {}".format(sSection, boolDelAsAdmin))
            return defDeleteREG_Case(sSection, boolDelAsAdmin)  # boolInvestigator immer =false, standardmäßig= False
        else:
            logger_defDeleteCaseSettings.debug("Speicherort nicht gefunden: {}".format(sStorage))


def defWriteCaseSettings(sStorage, sSection='', boolAsAdmin=False, boolInvestigator=False):
    logger_defWriteCaseSettings = getLogger('XWAYS_Launcher.defWriteCaseSettings{sig}'.format(sig=inspect.signature(defWriteCaseSettings)))
    logger_defWriteCaseSettings.debug('Beginn defWriteCaseSettings {0}'.format(''))

    returnValue = 0
    if sStorage == 'reg':
        if sSection == '':  # ohne Section sind damit die Einstellungen des Launchers "settings" gemeint
            logger_defWriteCaseSettings.debug('Beginn defWriteREG_Settings(boolInvestigator=False) -> Launchereinstellungen werden geschrieben'.format(''))
            returnValue = defWriteREG_Settings(boolInvestigator)
        else:
            logger_defWriteCaseSettings.debug(
                'Beginn defWriteREG_CaseEntries(sSection, boolAsAdmin, boolInvestigator) sSection: {}; boolAsAdmin: {}  -> Falleinstellungen werden geschrieben'.format(
                    sSection, boolAsAdmin))
            returnValue = defWriteREG_CaseEntries(sSection, boolAsAdmin, boolInvestigator)
    elif sStorage == 'ini':
        logger_defWriteCaseSettings.debug('Aufruf defWriteINI_Settings(sSection, boolInvestigator=False) sSection:{}'.format(sSection))
        # TODO: hier die INI behandeln!
    else:
        logger_defWriteCaseSettings.debug('Unbekannte Parameter übergeben an defWriteCaseSettings sStorage:{}, sSection:{}, boolAsAdmin: {}'.format(sStorage, sSection, boolAsAdmin))
    return returnValue


def defWriteREG_Settings(boolInvestigator=False):
    logger_defWriteREG_Settings = getLogger('XWAYS_Launcher.defWriteREG_Settings{sig}'.format(sig=inspect.signature(defWriteREG_Settings)))
    logger_defWriteREG_Settings.debug('Beginn defWriteREG_Settings {0}'.format(''))

    """hier mit for Schleife die Werte aus der gldictLauncher übernehmen """
    if boolInvestigator:
        glsRegPath = glsRegXWIPath
    else:
        glsRegPath = glsRegXWFPath
    try:
        count = 0
        winreg.CreateKey(glsRegRoot, glsRegPath)
        registry_key = winreg.OpenKey(glsRegRoot, glsRegPath, 0, winreg.KEY_ALL_ACCESS)
        for name, value in gldictLauncher.items():
            if name in gldictLauncher['listREGSettings']['AppXWF']:
                logger_defWriteREG_Settings.info('name {} value {}'.format(name, value))
                if name == 'FolderDictionary':
                    winreg.SetValueEx(registry_key, name, 0, winreg.REG_SZ, ';'.join('{}={}'.format(key, ''.join('{}'.format(item) for item in str(val)).replace(';', ' ')) for key, val in value.items()))
                else:
                    winreg.SetValueEx(registry_key, name, 0, winreg.REG_SZ, ';'.join('{}={}'.format(key, val) for key, val in value.items()).replace('=;', "='';"))
                count += 1
        winreg.CloseKey(registry_key)
        return count
    except WindowsError as err:
        logger_defWriteREG_Settings.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
        return err


def defWriteREG_CaseEntries(sSection='', boolAdmin=False, boolInvestigator=False):
    logger_defWriteREG_CaseEntries = getLogger('XWAYS_Launcher.defWriteREG_CaseEntries{sig}'.format(sig=inspect.signature(defWriteREG_CaseEntries)))
    logger_defWriteREG_CaseEntries.debug('Beginn defWriteREG_CaseEntries {0}'.format(''))

    if boolAdmin:  # Wenn Admin gesetzt ist, dann wird ein zusätzlicher Schlüssel für die jeweilige exe (sSection)  in die Registry geschrieben
        try:
            glsRegPath = 'Software\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Layers'
            winreg.CreateKey(glsRegRoot, glsRegPath)
            registry_key = winreg.OpenKey(glsRegRoot, glsRegPath, 0, winreg.KEY_ALL_ACCESS)
            winreg.SetValueEx(registry_key, path.join(gldictCase[sSection]['StandardDir'], path.split(gldictCase[sSection]['App'])[1]), 0, winreg.REG_SZ, 'RUNASADMIN')   # 'Pfad'
            winreg.CloseKey(registry_key)
            logger_defWriteREG_CaseEntries.debug('RUNASADMIN wurde für den Fall {} erfolgreich gesetzt'.format(sSection))
        except Exception:
            logger_defWriteREG_CaseEntries.exception('RUNASADMIN konnte für den Fall {} nicht gesetzt werden {}:{}'.format(sSection, exc_info()[1], exc_info()[0]))
            return -2

    if boolInvestigator:
        glsRegPath = glsRegXWIPath
    else:
        glsRegPath = glsRegXWFPath
    try:
        count = 0
        winreg.CreateKey(glsRegRoot, glsRegPath + '\\' + sSection)
        registry_key = winreg.OpenKey(glsRegRoot, glsRegPath + '\\' + sSection, 0, winreg.KEY_ALL_ACCESS)
        logger_defWriteREG_CaseEntries.debug('für den Fall {} wurden folgende Key: value Einträge'.format(sSection))
        for name, value in gldictCase[sSection].items():
            winreg.SetValueEx(registry_key, name, 0, winreg.REG_SZ, value)
            count += 1
            logger_defWriteREG_CaseEntries.debug('(Zeile {}) Key: {} mit value: {}'.format(count, name, value))
        winreg.CloseKey(registry_key)
        logger_defWriteREG_CaseEntries.debug('erfolgreich in die Registry geschrieben'.format(''))
        return count
    except Exception:
        logger_defWriteREG_CaseEntries.exception('Für den Fall {} konnten die Einstellungen nicht gesetzt werden! Fehler: {}:{}'.format(sSection, exc_info()[1], exc_info()[0]))
        return -1


def defWriteINI_Settings():
    logger_defWriteINI_Settings = getLogger('XWAYS_Launcher.defWriteINI_Settings{sig}'.format(sig=inspect.signature(defWriteINI_Settings)))
    logger_defWriteINI_Settings.debug('Beginn defWriteINI_Settings {0}'.format(''))

    config = ConfigParser()
    config.add_section('LauncherSettings')
    # TODO: hier mit for Schleife die Werte aus der gldictLauncher mit config.set übernehmen
    # TODO: hier mit for Schleife die Werte aus der gldictCase mit config.set übernehmen
    for section in gldictCase:
        config.add_section(section)
        for key in section:
            for value in key:
                config.set(section, key, value)

    logger_defWriteINI_Settings.debug('Write Settings to users{!s})'.format(glsXWAYSLauncherINI))
    with open(path.join(path.expanduser('~'), glsXWAYSLauncherINI), 'w') as configfile:
        iReturn = config.write(configfile)

    return iReturn


def defFind(aSearchlist, elem):  # INFO: funktionsfähig
    return [[i for i, x in enumerate(aSearchlist) if x == e] for e in elem]


def readRegfile(filename, encoding='utf-16'):
    logger_readRegfile = getLogger('XWAYS_Launcher.readRegfile{sig}'.format(sig=inspect.signature(readRegfile)))
    logger_readRegfile.debug('Beginn readRegfile {0}'.format(''))

    import re
    with open(filename, encoding=encoding) as f:
        data = f.read()
    # get rid of non-section strings in the beginning of .reg file
    data = re.sub(r'^[^\[]*\n', '', data, flags=re.S)
    logger_readRegfile.debug('data = re.sub(r\'^[^\[]*\\n\', '', data, flags=re.S): data: {0}'.format(data))
    cfg = ConfigParser(strict=False)
    # dirty hack for "disabling" case-insensitive keys in "configparser"
    cfg.optionxform = str
    cfg.read_string(data)
    data = []
    # iterate over sections and keys and generate `data` for pandas.DataFrame
    for s in cfg.sections():
        logger_readRegfile.debug('for s in cfg.sections(): s= {0}'.format(s))
        if not cfg[s]:
            logger_readRegfile.debug('if not cfg[s]:'.format())
            data.append([s, None, None, None])
            logger_readRegfile.debug('data.append([s, None, None, None]): {0}'.format(cfg[s]))
        for key in cfg[s]:
            logger_readRegfile.debug('key: {0}'.format(key))
            tp = val = None
            if cfg[s][key]:
                # take care of value type
                if ':' in cfg[s][key] and not key == '"erstellt"':
                    logger_readRegfile.debug('if \':\' in cfg[s][key]: {0}'.format(cfg[s][key]))
                    tp, val = cfg[s][key].split(':')
                    logger_readRegfile.debug('tp, val: {0},{1}'.format(tp, val))
                else:
                    val = cfg[s][key].replace('"', '').replace(r'\\\n', '')
                    logger_readRegfile.debug('val = cfg[s][key].replace(\'\"\', \'\').replace(r\'\\\\n\', \'\'): {0}'.format(val))

            data.append([s, key.replace('"', ''), tp, val])
    logger_readRegfile.debug('return data: {0}'.format(data))
    return data


def defReadREG_CaseEntries(boolInvestigator=False):  # INFO: funktionsfähig
    logger_defReadREG_CaseEntries = getLogger('XWAYS_Launcher.defReadREG_CaseEntries{sig}'.format(sig=inspect.signature(defReadREG_CaseEntries)))
    logger_defReadREG_CaseEntries.debug('Beginn defReadREG_CaseEntries {0}'.format(''))
    # TODO: es sollte beides eingelesen werden, ohne Unterscheidung, ob investigator case oder forensics case, derzeit da alles unter Forensics, wird auch alles aus Forensics ausgelesen DONE
    # glsRegXWFPath = "SOFTWARE\X-Ways AG\X-Ways Forensics\X-Ways_Launcher"
    # glsRegXWIPath = "SOFTWARE\X-Ways AG\X-Ways Investigator\X-Ways_Launcher"
    # INFO: bisher werden auch investigator Fälle unter dem Forensics-Key abgelegt
    """ Falleinstellungen werden in die globale Liste eingelesen
    
        Unter dem Registry Pfad 'glsRegXWFPath' werden die Fallnamen ausgelesen und in 'aSection' gespeichert
        Dann wird ein classCase-Objekt mit dem Fallnamen erzeugt 'objRegCaseEntry = classCase(aSection[i])'  und mit 'objRegCaseEntry.set(aValue)' werden die Falldaten eingelesen
        Zuletzt wird der globalen 'gldictCase' mit  'gldictCase[objRegCaseEntry.sCaseName] = objRegCaseEntry.dictCase' eine Dictionary übergeben {sCaseName: {dictCase}}
        Einlesevorgang dauert in der For-Schleife an: 'for caseName in objRegCaseEntry.dictCase as Cases:'
    """
    node = ''
    aSection = []  # Liste der Fälle
    global gldictCase
    if boolInvestigator:
        glsRegPath = glsRegXWIPath
    else:
        glsRegPath = glsRegXWFPath
    try:
        aKeyValue = winreg.OpenKey(glsRegRoot, glsRegPath)
    except WindowsError as winerr:
        logger_defReadREG_CaseEntries.debug('Fehler {0} beim Zugriff auf den Schlüssel {1}'.format(winerr, glsRegPath))
        return {}
    gldictCase.clear()  # gldictCase leren
    for i in range(winreg.QueryInfoKey(aKeyValue)[0]):  # Index 0 number of subkeys, 1 = number of values, 2 = key last modified (longint)
        try:
            node = winreg.EnumKey(aKeyValue, i)
            aSection.append(winreg.EnumKey(aKeyValue, i))  # sReturn[1] == [value]
        except EnvironmentError:
            logger_defReadREG_CaseEntries.debug('Fehler bei {}: node = {}'.format(i, node))
            logger_defReadREG_CaseEntries.debug('aSection: {}'.format(aSection))
            logger_defReadREG_CaseEntries.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
            winreg.CloseKey(aKeyValue)
            return aSection
    winreg.CloseKey(aKeyValue)

    if len(aSection) == 0:
        logger_defReadREG_CaseEntries.debug('len(aSection): {0}'.format(len(aSection)))
        return aSection
        # TODO: hier noch Fehlerbehandlung implementieren
    else:
        for sCaseName in aSection:  # for i in range(0, len(aSection)):  # build global Case List with Folders
            if boolInvestigator:
                glsRegPath = glsRegXWIPath
            else:
                glsRegPath = glsRegXWFPath
            try:
                aKeyValue = winreg.OpenKey(glsRegRoot, glsRegPath + "\\" + sCaseName)
            except WindowsError as winerr:
                logger_defReadREG_CaseEntries.debug('Fehler {0} beim Zugriff auf den Schlüssel {sREGPath}'.format(winerr, sREGPath=glsRegPath + "\\" + sCaseName))
                return aSection
            sAppXW = ''
            for f in range(0, winreg.QueryInfoKey(aKeyValue)[1]):  # finde heraus, obs investigator oder forensics fall
                try:
                    aValue = winreg.EnumValue(aKeyValue, f)  # Check if no error occurred.
                    if 'xwforensics' in aValue[1]:  # aValue[0] == 'App' and ('xwforensics' in aValue[1]):
                        sAppXW = 'AppXWF'
                        break
                    elif 'xwinvestigator' in aValue[1]:  # aValue[0] == 'App' and ('xwinvestigator' in aValue[1]):
                        sAppXW = 'AppXWI'
                        break
                except OSError or TypeError as err:
                    logger_defReadREG_CaseEntries.debug('Es wird ein Leerer Wert eingelesen: {}\nFehler: {}'.format(sCaseName, err))
                    logger_defReadREG_CaseEntries.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))

            objRegCaseEntry = classCase(sCaseName, sAppXW)  # Kontainerobjekt mit variablen entsprechend dem Parameter aAppXW (invest oder forens)
            for f in range(0, winreg.QueryInfoKey(aKeyValue)[1]):  # len(objRegCaseEntry.dictCase)):
                try:
                    aValue = winreg.EnumValue(aKeyValue, f)  # Check if no error occurred.
                    objRegCaseEntry.set(aValue)
                except OSError or TypeError as err:
                    logger_defReadREG_CaseEntries.debug('Es wird ein leerer Wert eingelesen: {}\nFehler: {}'.format(sCaseName, err))
                    logger_defReadREG_CaseEntries.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
                    objRegCaseEntry.set({'Err': ''})
            # wir bauen uns eine nette dictionary aus den ergebnissen Fallname:Objekt
            gldictCase[objRegCaseEntry.sCaseName] = objRegCaseEntry.dictCase  # gldictCase.append(objRegCaseEntry)
    winreg.CloseKey(aKeyValue)
    logger_defReadREG_CaseEntries.debug('gldictCase= {}'.format(gldictCase))
    return aSection


def defReadREG_Settings(boolInvestigator=False):  # Settings werden in globale Dictionary geschrieben
    logger_defReadREG_Settings = getLogger('XWAYS_Launcher.defReadREG_Settings{sig}'.format(sig=inspect.signature(defReadREG_Settings)))
    logger_defReadREG_Settings.debug('Beginn defReadREG_Settings {0}'.format(''))
    # aktueller Registrypfad: glsRegXWFPath = "SOFTWARE\X-Ways AG\X-Ways Forensics\X-Ways_Launcher"
    # global gliXWApp64
    global glsCaseSettingsStorage
    a_reg = winreg.ConnectRegistry(None, glsRegRoot)  # glsRegRoot = "HKEY_CURRENT_USER"

    if boolInvestigator:  # derzeit immer false
        glsRegPath = glsRegXWIPath
    else:
        glsRegPath = glsRegXWFPath  # glsRegXWFPath wird derzeit standardmäßig angenommen

    # TODO: wenn in REG kein Key='XWLauncherID' existiert, dann XWLauncherID = 0 und dictLauncher = gldictLauncher.copy(), sonst XWLauncherID = reg.XWLauncherID und glsCaseSettingsStorage = 'reg'
    try:
        hKey = winreg.OpenKey(a_reg, glsRegPath)
        result = dict(substring.split('=') for substring in (winreg.QueryValueEx(hKey, "XWLauncherID")[0]).split(';'))
        logger_defReadREG_Settings.debug('X-Ways Launcher Registry_Settings Version {0}'.format(result['AppXWF']))
    except WindowsError as Error:
        logger_defReadREG_Settings.debug('SubKey "XWLauncherID" nicht in den REG-Settings gefunden, Standard-Einstellungen "gldictLauncher" werden vorgenommen.\n gemeldeter Fehler: {!s}'.format(Error))
        glsCaseSettingsStorage = "reg"  # ?????????
        return -1  # es wurden keine Einstellungen gefunden, es wird die Zahl 0 zurückgeliefert

    # gliXWApp64 = 64
    if result['AppXWF'] == __version__:         # if str.split(result[0], ';')[1][7:] == __version__:

        # dictLauncher = gldictLauncher.copy()  # habe mich entschieden ohne kopien zu arbeiten und beackere nun die globale dictionary

        # INFO: an dieser Stelle gibt es in der Registry Einstellungen für den Launcher, diese Einstellungen werden ausgelesen und "gldictLauncher" entsprechend angepasst
        subkeyCount, valuesCount, modtime = winreg.QueryInfoKey(hKey)
        aValue = []
        maxIndex = range(winreg.QueryInfoKey(hKey)[1])
        for subkeyindex in maxIndex:  # für alle SubKeys mit Subvalues im X-WAYS_Launcher Key
            try:
                aValue = winreg.EnumValue(hKey, subkeyindex)
            except WindowsError as Error:
                logger_defReadREG_Settings.debug('Windows-Fehler beim aktualisieren der gldictLauncher.\n gemeldeter Fehler: {!s}\nInhalt aValue: {!r}'.format(Error, aValue))
                continue
            # DONE: hier die dictionary gldictLauncher traversieren und an passender Stelle das tuple einfügen
            aSubValues = aValue[1].split(';')
            sTempSubkey = '='
            sTempSubvalue = '='
            for subVal in aSubValues:
                try:
                    sTempSubkey = subVal.split('=')[0]
                    sTempSubvalue = subVal.split('=')[1]
                    if sTempSubkey != '=':
                        if sTempSubvalue == "''" or sTempSubvalue == '""' or sTempSubvalue == '=':
                            sTempSubvalue = ''
                        gldictLauncher[aValue[0]][sTempSubkey] = sTempSubvalue
                except IndexError as indErr:
                    logger_defReadREG_Settings.debug(
                        'Index-Fehler beim aktualisieren der gldictLauncher["{}"].\n gemeldeter Fehler: {!s}\ngldictLauncher[aValue[0]][sTempSubkey] = sTempSubvalue:\n aValue[0]: {!r}, sTempSubkey: {!r}, sTempSubvalue: {!r}'.format(
                            subkeyindex, indErr, aValue[0], sTempSubkey, sTempSubvalue))
                except KeyError as keyErr:
                    logger_defReadREG_Settings.debug(
                        'Schlüssel-Fehler beim aktualisieren der gldictLauncher["{}"].\n gemeldeter Fehler: {!s}\ngldictLauncher[aValue[0]][sTempSubkey] = sTempSubvalue:\n aValue[0]: {!r}, sTempSubkey: {!r}, sTempSubvalue: {!r}'.format(
                            subkeyindex, keyErr, aValue[0], sTempSubkey, sTempSubvalue))
                # DONE: in Abhängigkeit vom ValueTag{AppXWF/AppXWI,FilePos} wird der Subkey ausgelesen und in die Dictionary übertragen
        return maxIndex.stop  # gliXWApp64
    else:
        # wenn die __version__ beim ersten Durchlauf nicht übereinstimmt oder nicht existiert, Settings löschen und neu schreiben
        logger_defReadREG_Settings.debug('__version__ stimmt beim ersten Start nicht überein oder der Eintrag existiert nicht{0}'.format(''))
        defDeleteREG_Settings()
        # logger_defReadREG_Settings.debug('defDeleteREG_Settings() {0}'.format(''))

        retVal = defWriteREG_Settings(boolInvestigator)
        logger_defReadREG_Settings.debug('ret = defWriteREG_Settings(boolInvestigator): Werte neu geschrieben: ret={0}'.format(retVal))
        return 0    # damit wird eine Neuauslesung in der Funktion defReadCaseSettings() veranlasst...


def defReadINI_CaseEntries():  # TODO: alles
    iReturn = -1
    return iReturn


def defReadINI_Settings(sSection='LauncherSettings'):  # TODO: alles

    configINI = ConfigParser()
    options = configINI.options(sSection)

    with open(path.join(path.expanduser('~'), glsXWAYSLauncherINI)) as configfile:
        configINI.read_file(configfile)

    for option in options:
        configfile[option] = configINI.get(sSection, option)

    configINI.read('')
    print(configINI['CaseSettings']['path'])  # -> "/path/name/"
    configINI['CaseSettings']['path'] = '/var/shared/'  # update
    configINI['CaseSettings']['default_message'] = 'Hey! help me!!'  # create

    iReturn = -1
    return iReturn


def defReadCaseSettings(sStorage=glsCaseSettingsStorage):  # INFO: funktionsfähig
    logger_defReadCaseSettings = getLogger('XWAYS_Launcher.defReadCaseSettings{sig}'.format(sig=inspect.signature(defReadCaseSettings)))
    logger_defReadCaseSettings.debug('Beginn defReadCaseSettings {0}'.format(''))

    aSectionReturn = {}
    iSettingsReturn = -1
    with Switch(sStorage) as case:
        if case('ini'):
            iSettingsReturn = defReadINI_Settings()
            aSectionReturn = defReadINI_CaseEntries()
        elif case('reg'):
            # wenn Erststart, oder __version__ nicht passend, dann iSettingsReturn=0
            iSettingsReturn = defReadREG_Settings(glboolXWAppInvestigator)
            aSectionReturn = defReadREG_CaseEntries(glboolXWAppInvestigator)
        else:
            logger_defReadCaseSettings.debug("Speicherort nicht gefunden: {}".format(sStorage))

        if iSettingsReturn == 0:
            iSettingsReturn = defReadREG_Settings(glboolXWAppInvestigator)
    dictReturn = {'SectionList': aSectionReturn, 'SettingsCount': iSettingsReturn}
    return dictReturn


def defUpdateListWidget(aSection, objWindow):
    objWindow.List1.clear()
    objWindow.List1.addItems(aSection)
    # objWindow.List1.clear()
    # objWindow.List1.addItems(aSection)


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # base_path = getattr(sys, '_MEIPASS', path.dirname(path.abspath(__file__)))
        base_path = sys._MEIPASS
    except Exception:
        base_path = path.abspath('.')

    return path.join(base_path, relative_path)


def main():
    logger_main = getLogger('XWAYS_Launcher.main{sig}'.format(sig=inspect.signature(main)))
    logger_main.debug('******************************************************************************************************************'.format(''))
    logger_main.debug('*************************************X-Ways_Launcher: Programm gestartet.*****************************************'.format(''))
    logger_main.debug('******************************************************************************************************************'.format(''))
    logger_main.debug('Beginn main(){}'.format(''))
    logger_main.debug('Programmversion: {}'.format(__version__))
    logger_main.debug('Logging nach: {}'.format(glfileLogFile))
    # iXWApp64 = 0
    # dictLauncher ={}
    # sSourceDir = ''  # alle Dateien aus diesem Verzeichnis sollen kopiert werden
    # sDestDir = ''  # die Quelldateien sollen (mit Unterverzeichnissen) in dieses Verzeichnis kopiert werden
    global glsCaseSettingsStorage  # reg oder ini
    global app
    app = QApplication(argv)
    strStyle = """/*@colorschema: @black0;
    @black0: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #a6a6a6, stop: 0.08 #7f7f7f, stop: 0.39999 #717171, stop: 0.4 #626262, stop: 0.9 #4c4c4c, stop: 1 #333333);
    @blue0: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #c6e6f5, stop: 0.08 #87bbd0, stop: 0.39999 #336a7e, stop: 0.4 #255555, stop: 0.9 #002d3c, stop: 1 #333333); */
    QFrame{ background: transparent; font: "Segoe UI";}
    *[Pflichtfeld="True"] {background-color: orange;}
    QApplication{ background: transparent; font: "Segoe UI"; font-size: 14px; /*zentral für alle Widgets*/}
    QWidget{background-color: WIDGETBG; font: "Segoe UI"; font-size: 14px; /*zentral für alle Widgets*/}
    QMenu{background-color: MENUBG; margin: 2px; /* some spacing around the menu */}
    QMenu::item { padding: 2px 25px 2px 20px; border: 1px solid transparent; /* reserve space for selection border */}
    QMenu::item:selected { border: 1px solid transparent; background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #FAFBFE, stop: 1 #DCDEF1);}
    QMenu::icon:checked { /* appearance of a 'checked' icon */ background: gray; border: 1px inset gray; position: absolute; top: 1px; right: 1px; bottom: 1px; left: 1px;}
    QMenu::separator { height: 2px; background: lightgray; margin-left: 10px; margin-right: 5px;}
    QMenu::indicator { width: 13px; height: 13px;}
    QMenuBar { background-color: MENUBAR; color: LABELcolor; }
    QMenuBar::item { spacing: 3px; /* spacing between menu bar items */ padding: 1px 4px; background: transparent; border-radius: 4px;}
    QMenuBar::item:selected { /* when selected using mouse or keyboard */ background: #a8a8a8;}
    QMenuBar::item:pressed { background: #888888;}
    QLineEdit { border: 1px solid grey; border-radius: 6px; background-color: LEBG; padding-left: 10px; selection-background-color: rgb(100, 100, 100);}
    /* QPushButton { font-size: 14px; border: 1px solid grey; border-radius: 6px; color: white;  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #a6a6a6, stop: 0.08 #7f7f7f, stop: 0.39999 #717171, stop: 0.4 #626262, stop: 0.9 #4c4c4c, stop: 1 #333333);} */
    QPushButton { font-size: 14px; border: 1px solid grey; border-radius: 6px; color: white;  background: PBTNBG;}
    QPushButton:pressed { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #dadbde, stop: 1 #f6f7fa);}
    QPushButton:flat {  border: none; /* no border for a flat push button */}
    QPushButton:default { border-color: navy; /* make the default button prominent */}
    QToolButton { /* all types of tool button */ border: 1px solid grey; border-radius: 6px; color: white; background: TBBG;}
    QToolButton[popupMode="1"] { /* only for MenuButtonPopup */ padding-right: 20px; /* make way for the popup button */}
    QToolButton:pressed { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #dadbde, stop: 1 #f6f7fa);}
    /* the subcontrols below are used only in the MenuButtonPopup mode */
    QToolButton::menu-button { border: 1px solid gray; border-top-right-radius: 6px; border-bottom-right-radius: 6px; width: 16px;}
    QCheckBox{ selection-background-color: rgb(255, 255, 255); color: LABELcolor;}
    QStatusBar{ color: black;background: STATBAR;}
    QComboBox { border: 1px solid gray; border-radius: 6px; padding: 1px 18px 1px 10px; }
    QComboBox:editable, QComboBox:!editable, QComboBox::drop-down:editable { background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #E1E1E1, stop: 0.4 #DDDDDD, stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);}
    /* QComboBox gets the "on" state when the popup is open */
    QComboBox:!editable:on, QComboBox::drop-down:editable:on {background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  stop: 0 #D3D3D3, stop: 0.4 #D8D8D8, stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1); }
    QComboBox:on { /* shift the text when the popup opens */ padding-top: 3px; padding-left: 4px;}
    QComboBox::drop-down { subcontrol-origin: padding; subcontrol-position: top right; border-width: 1px; border-color: darkgray; border-style: solid; border-radius: 3px;  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  stop: 0 #D3D3D3, stop: 0.4 #D8D8D8, stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);}
    QComboBox QAbstractItemView { border-radius: 6px; border: 1px solid darkgray; selection-background-color: lightgray; background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #E1E1E1, stop: 0.4 #DDDDDD, stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);}
    QListWidget {border: 1px solid grey; border-radius: 6px; padding: 5px; background-color: LISTBG; color: LISTCOLOR;}
    QTextEdit { border: 1px solid grey; border-radius: 6px; background-color: LEBG; padding-left: 10px; selection-background-color: rgb(100, 100, 100);}
    QListView { show-decoration-selected: 1; /* make the selection span the entire width of the view */ font-size: 20px; padding-left: 15px; padding-right: 15px;}
    QListView::item:alternate {background: #EEEEEE;}
    QListView::item:selected {border-radius: 6px; border: 0px solid #6a6ea9;}
    QListView::item:selected:!active {border-radius: 6px; background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ABAFE5, stop: 1 #8588B2);}
    QListView::item:selected:active {border-radius: 6px; background: LVisa;}
    QListView::item:hover {border-radius: 6px; background: LVih;}
    QGroupBox { color: GBOX2; border: 1px solid GBOX2;/*#8f8f91;*/ border-radius: 6px; padding-top: 13px; padding-left: 8px; background-color: GBBG;}
    QGroupBox::title { border: 1px solid GBOX1; border-radius: 6px; color: GBOX1; font-style: italic; font-weight: bold; subcontrol-origin: margin; subcontrol-position:top center; padding:0 3px;}
    QLabel { color: LABELcolor; background-color: transparent;}
    QToolTip { background-color: #454545; color: white; border: black solid 1px}
    QTreeView { alternate-background-color: lightgrey; background: darkgrey; show-decoration-selected: 1;}
    QTreeView::item { border: 1px solid #d9d9d9; border-top-color: transparent; border-bottom-color: transparent;}
    QTreeView::item:hover { background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #e7effd, stop: 1 #cbdaf1); border: 1px solid #bfcde4;}
    QTreeView::item:selected { border: 1px solid #567dbc;}
    QTreeView::item:selected:active{ background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #6ea1f1, stop: 1 #567dbc);}
    QTreeView::item:selected:!active { background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #6b9be8, stop: 1 #577fbf);}
    QProgressBar { border: 1px solid grey; border-radius: 6px; text-align: center;}
    QProgressBar::chunk { background-color: darkgrey; width: 10px; margin: 0.5px;}"""
    # try:
    #     stylefile = str(open("styles.css", "r").read())
    #     logger_main.debug('styles.css konnte erfolgreich eingelesen werden: {}'.format('hurra!'))
    #     strStyle = stylefile
    # except OSError as oserr:
    #     logger_main.debug('styles.css konnte nicht eingelesen werden: {}; Wende interne Einstellungen an!'.format(oserr))
    #     try:
    #         open("styles.css", "w").write(strStyle)
    #     except OSError as oserr:
    #         logger_main.debug('styles.css konnte nicht geschrieben werden: {}; Wende interne Einstellungen an!'.format(oserr))
    #     logger_main.debug('styles.css wird erstellt: {}; interne Einstellungen werden geschrieben!'.format(oserr))

    black0_0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #a6a6a6, stop: 0.08 #7f7f7f, stop: 0.39999 #717171, stop: 0.4 #626262, stop: 0.9 #4c4c4c, stop: 1 #333333)"
    blue1_0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #846fd7, stop: 0.08 #6549d7, stop: 0.39999 #3415b0, stop: 0.4 #150d84, stop: 0.9 #1c0772, stop: 1 #1c0772)"
    blue1_1 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #888dd9, stop: 1 darkblue)"  # #5a5e99
    blue1_2 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 darkblue, stop: 1 #888dd9)"
    grey0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #f6f7fa, stop: 1 #dadbde)"
    grey0_1 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #f6f7fa, stop: 1 grey)"    # #7a7b7e
    grey0_2 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #c6c7c8, stop: 1 grey)"  # #7a7b7e
    red0_0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff0000, stop: 1 #3e0000)"
    red1 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #c6e6f5, stop: 0.08 #87bbd0, stop: 0.39999 #336a7e, stop: 0.4 #255555, stop: 0.9 #002d3c, stop: 1 #333333)"
    fire0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #f9d800, stop: 0.18 #fc8607, stop: 0.39999 #ff4502, stop: 0.6 #f91818, stop: 0.9 #0a5dc9, stop: 1 #333333)"
    menu0_0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #f6f7fa, stop: 1 #dadbde)"
    menu0_1 = "qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 lightgray, stop:1 darkgray)"
    menu1_0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #dd65bf, stop: 1 #bc008d)"
    menu1_1 = "qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 lightblue, stop:1 darkblue)"
    lineeditbg0_0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #f6f7fa, stop: 1 #dadbde)"
    lineeditbg1_0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 lightblue, stop: 1 #dadbde)"
    statusbar0_0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  stop: 0 #f3f3f3, stop: 0.3 #d8d8d8, stop: 0.7 #bDbDbD, stop: 1.0 #919191)"
    statusbar1_0 = "qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 darkblue, stop:1 lightblue)"
    listvievitemhover0_0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #FAFBFE, stop: 1 #DCDEF1)"
    listviewitemhover1_0 = "qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 white, stop: 1 #888dd9)"
    toolbuttonbg0_0 = "qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4, radius: 1.35, stop: 0 #fff, stop: 1 #888)"
    toolbuttonbg1_0 = "qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4, radius: 1.35, stop: 0 lightblue, stop: 1 darkblue)"

    dictColorTemplates = {'dark': {'PBTNBG': black0_0, 'LISTBG': grey0_1, 'WIDGETBG': 'grey', 'GBOX2': 'grey', 'GBOX1': 'grey', 'LVisa': red0_0, 'GBBG': grey0_2,
                                   'MENUBG': menu0_0, 'MENUBAR': menu0_1, 'LEBG': lineeditbg0_0, 'STATBAR': statusbar0_0, 'LISTCOLOR': 'black', 'LVih': listvievitemhover0_0,
                                   'LABELcolor': 'black', 'TBBG': toolbuttonbg0_0},
                          'green': {},
                          'blue': {'PBTNBG': blue1_0, 'LISTBG': blue1_1, 'WIDGETBG': 'darkblue', 'GBOX2': 'white', 'GBOX1': 'white', 'LVisa': blue1_2, 'GBBG': blue1_1,
                                   'MENUBG': menu1_0, 'MENUBAR': menu1_1, 'LEBG': lineeditbg1_0, 'STATBAR': statusbar1_0, 'LISTCOLOR': 'blue', 'LVih': listviewitemhover1_0,
                                   'LABELcolor': 'white', 'TBBG': toolbuttonbg1_0}
                          }

    # TODO: existiert die xwayslauncher.ini ? lese einstellungen und fälle, wenn nicht, lese registry, wenn nicht schreibe in registry die standardwerte und weiter...
    configINI = ConfigParser()
    try:
        with open(path.join(path.expanduser('~'), glsXWAYSLauncherINI), 'r') as fileXWAYSLauncherINI:
            configINI.read(fileXWAYSLauncherINI)
            if 'SETTINGS' in configINI:  # configINI.has_section('SETTINGS'):  # ist die ini mit der Sektion SETTINGS befüllt?
                glsCaseSettingsStorage = 'ini'
                logger_main.debug('glsCaseSettingsStorage = {0}'.format(glsCaseSettingsStorage))
            else:
                glsCaseSettingsStorage = 'reg'
                logger_main.debug('glsCaseSettingsStorage = {0}'.format(glsCaseSettingsStorage))
    except FileNotFoundError:
        glsCaseSettingsStorage = 'reg'
        logger_main.debug('Fehler beim Einlesen der INI: {0}:{1}'.format(FileNotFoundError, exc_info()[1]))
        # logger_main.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))
        logger_main.debug('Lese alternativ die Registry')
    dictReturn = defReadCaseSettings(glsCaseSettingsStorage)  # dann CaseList aus INI/REG lesen
    if dictReturn['SettingsCount'] <= 0:
        defWriteCaseSettings(glsCaseSettingsStorage)  # wenn keine Settings gefunden werden, dann interne Settings in registry schreiben
        dictReturn = defReadCaseSettings(glsCaseSettingsStorage)  # dann CaseList aus INI/REG lesen
    logger_main.debug('SectionList={SectionList}, SettingsCount={SettingsCount}'.format(**dictReturn))

    try:
        logger_main.debug('Versuche Styles anzuwenden: {0}\n'.format(''))
        # app.setStyleSheet(stylefile.read())
        strColorTemplate = gldictLauncher['XWColorSchema'].get('AppXWF')  # INFO: Anwendung des Farbtemplates
        for widget, color in dictColorTemplates[strColorTemplate].items():
            # logger_main.debug('{0}: {1}'.format(widget, color))
            strStyle = strStyle.replace(widget, color)

        # logger_main.debug('strStyle: {0}\n{1}'.format(strStyle, blue0))
        app.setStyleSheet(strStyle)
        logger_main.debug('styles konnten erfolgreich angewandt werden: {0}'.format(''))
    except Exception:
        logger_main.debug('styles  konnten nicht angewandt werden: {0}'.format(exc_info()[0]))
        logger_main.exception('Ein Fehler ist aufgetreten: : {0}:{1}'.format(exc_info()[0], exc_info()[1]))

    window = XWLauncher()
    defUpdateListWidget(sorted(dictReturn['SectionList'], key=glPreSortFunc, reverse=glBoolSortReverseOrder), window)
    window.setAttribute(Qt.WA_QuitOnClose)
    window.show()
    getUpdate(verbose=False)
    exit(app.exec_())


if __name__ == '__main__':
    ret = True  # ret = True zum debuggen
    # ret = run_as_admin()
    if ret is True:
        print('I have admin privilege.')
        main()
    elif ret is None:
        # print('I am elevating to admin privilege.')
        exit()
    else:
        print('Error(ret={}): cannot elevate privilege.'.format(ret))
