# -*- coding: utf-8 -*-
import inspect
from sys import _getframe, exc_info
import logging

from PyQt5.QtCore import QSize, Qt, QRect
from PyQt5.QtGui import QIcon, QCursor, QPixmap
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QLabel, QHBoxLayout, QSizePolicy, QPushButton, QStatusBar, QMenuBar, QMenu, QAction, QGroupBox


__version__ = '$1.0.2019.05.16.1915 indev python3.6-pyqt5.10$'
__date__ = '25.11.2017'
__author__ = 'Unkraut77 (https://gitlab.com/no_more_secrets/)'

try:
    _encoding = QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QApplication.translate(context, text, disambig)
# https://stackoverflow.com/questions/48121711/drag-and-drop-within-pyqt5-treeview


class Ui_frm_XW_Launcher(QMainWindow):
    localCaseDict = {}
    sInfoBoxText = '- X-WAYS Launcher dient dem Überblick und Organisation bei der forensischen Untersuchung digitaler Spuren mittels XWAYS\n\n- Die Programm- und Fallverzeichnisse werden dem Wunsch des Sachbearbeiters entsprechend kopiert bzw. angelegt und die jeweilige gewählte Arbeitskopie der X-WAYS Anwendung gestartet.\n\n- Bei Fragen und Anregungen:\n\tHelmut Schmidt, KD BB/K5, schmi171@polizei.bwl.de'
    sInfoBoxTitle = 'X-WAYS Launcher Info'
    strXWApp = ''

    def __init__(self, *args):              # parent=None):
        # QWidget.__init__(self, parent)
        logger___init__ = logging.getLogger('XWAYS_Launcher.frm_xw_launcher.__init__{sig}'.format(sig=inspect.signature(self.__init__)))
        logger___init__.debug('Beginn frm_xw_launcher.__init__ {0}'.format(''))
        super().__init__(*args)
        self.actionInfo = QAction()  # frm_XW_Launcher
        self.actionEinstellungen = QAction()  # frm_XW_Launcher
        self.actionHilfe = QAction()  # frm_XW_Launcher
        self.actionRemoveLauncher = QAction()  # frm_XW_Launcher
        self.actionUpdateLauncher = QAction()
        self.actionUpdateCaseToolTips = QAction()   # self.actionUpdateAllToolTips = QAction()
        self.actionUpdatePersonalToolTips = QAction()
        self.actionColorSchema = QAction()
        self.statusbar = QStatusBar()  # frm_XW_Launcher
        self.menubar = QMenuBar()  # frm_XW_Launcher
        self.menuMenu = QMenu(self.menubar)
        self.centralwidget = QWidget()  # frm_XW_Launcher
        self.groupBox = QGroupBox(self.centralwidget)
        self.verticalLayout = QVBoxLayout(self.groupBox)
        self.Label_XWAYSPath = QLabel(self.groupBox)
        self.label_15 = QLabel(self.groupBox)
        self.Label_XWAYSApp = QLabel(self.groupBox)
        self.Label_FolderHashDB = QLabel(self.groupBox)
        self.label_11 = QLabel(self.groupBox)
        self.Label_FolderTemp = QLabel(self.groupBox)
        self.label_9 = QLabel(self.groupBox)
        self.Label_FolderSave = QLabel(self.groupBox)
        self.label_7 = QLabel(self.groupBox)
        self.Label_FolderExport = QLabel(self.groupBox)
        self.label_5 = QLabel(self.groupBox)
        self.Label_FolderContainer = QLabel(self.groupBox)
        self.label_3 = QLabel(self.groupBox)
        self.Label_FolderCase = QLabel(self.groupBox)
        self.label = QLabel(self.groupBox)
        self.label_13 = QLabel(self.groupBox)
        self.pbtnAppClose = QPushButton(self.centralwidget)
        self.pbtnCaseRemove = QPushButton(self.centralwidget)
        self.pbtnCaseSave = QPushButton(self.centralwidget)
        self.pbtnCaseCreate = QPushButton(self.centralwidget)
        self.label_2 = QLabel(self.centralwidget)
        self.verticalLayout_3 = QVBoxLayout(self.centralwidget)
        self.pbtnCaseOpen = QPushButton(self.centralwidget)
        self.pbtnXDoku = QPushButton(self.centralwidget)
        self.verticalLayout_2 = QVBoxLayout()
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout_3 = QHBoxLayout()
        self.icon = QIcon()
        logger___init__.debug('Ende frm_xw_launcher.__init__ {}'.format(''))

    def setupUi(self):          # , frm_XW_Launcher
        logger_setupUi = logging.getLogger('XWAYS_Launcher.frm_xw_launcher.setupUi{sig}'.format(sig=inspect.signature(self.setupUi)))
        logger_setupUi.debug('Beginn frm_xw_launcher.setupUi {0}'.format(''))
        #frm_XW_Launcher.setStyleSheet(open("styles.qss", "r").read())
        # localCaseList = ''
        self.setObjectName("frm_XW_Launcher")        # frm_XW_Launcher
        self.setCentralWidget(self.centralwidget)        # frm_XW_Launcher
        self.resize(950, 750)        # frm_XW_Launcher
        self.setContextMenuPolicy(Qt.NoContextMenu)        # frm_XW_Launcher
        self.icon.addPixmap(QPixmap("./xways.ico"), QIcon.Normal, QIcon.Off)
        self.setWindowIcon(self.icon)        # frm_XW_Launcher
        self.setAutoFillBackground(False)        # frm_XW_Launcher
        self.statusbar.setObjectName("statusbar")
        self.setStatusBar(self.statusbar)        # frm_XW_Launcher
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_2.setMinimumSize(QSize(0, 25))
        self.label_2.setMaximumSize(QSize(5555, 35))
        self.label_2.setBaseSize(QSize(0, 30))
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_3.addWidget(self.label_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.List1.setMinimumSize(QSize(250, 0))
        self.List1.viewport().setProperty("cursor", QCursor(Qt.PointingHandCursor))
        # self.List1.setContextMenuPolicy(Qt.NoContextMenu)
        self.List1.setObjectName("List1")
        self.List1.setCursor(QCursor(Qt.PointingHandCursor))
        self.horizontalLayout.addWidget(self.List1)
        self.verticalLayout_2.setObjectName("verticalLayout_2")

        self.pbtnXDoku.setMinimumSize(QSize(100, 30))
        self.pbtnXDoku.setMaximumSize(QSize(500, 100))
        self.pbtnXDoku.setBaseSize(QSize(150, 30))
        self.pbtnXDoku.setCursor(QCursor(Qt.PointingHandCursor))
        self.pbtnXDoku.setContextMenuPolicy(Qt.NoContextMenu)
        self.pbtnXDoku.setObjectName("pbtnXhelp")
        self.pbtnXDoku.setCursor(QCursor(Qt.PointingHandCursor))
        self.pbtnXDoku.setToolTip('X-Ways Hilfe (PDF) aufrufen!')
        self.horizontalLayout_3.addWidget(self.pbtnXDoku)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.pbtnCaseOpen.setMinimumSize(QSize(100, 30))
        self.pbtnCaseOpen.setMaximumSize(QSize(5500, 100))
        self.pbtnCaseOpen.setBaseSize(QSize(150, 30))
        self.pbtnCaseOpen.setCursor(QCursor(Qt.PointingHandCursor))
        self.pbtnCaseOpen.setContextMenuPolicy(Qt.NoContextMenu)
        self.pbtnCaseOpen.setObjectName("pbtnCaseOpen")
        self.pbtnCaseOpen.setCursor(QCursor(Qt.PointingHandCursor))
        self.pbtnCaseOpen.setToolTip('X-Ways ausführen!')
        self.verticalLayout_2.addWidget(self.pbtnCaseOpen)
        self.pbtnCaseCreate.setMinimumSize(QSize(150, 30))
        self.pbtnCaseCreate.setMaximumSize(QSize(5500, 100))
        self.pbtnCaseCreate.setBaseSize(QSize(150, 30))
        self.pbtnCaseCreate.setCursor(QCursor(Qt.PointingHandCursor))
        self.pbtnCaseCreate.setContextMenuPolicy(Qt.NoContextMenu)
        self.pbtnCaseCreate.setObjectName("pbtnCaseCreate")
        self.pbtnCaseCreate.setCursor(QCursor(Qt.PointingHandCursor))
        self.pbtnCaseCreate.setToolTip('Erstellen neuer Fall-Einstellungen')
        self.verticalLayout_2.addWidget(self.pbtnCaseCreate)
        self.pbtnCaseRemove.setMinimumSize(QSize(100, 30))
        self.pbtnCaseRemove.setMaximumSize(QSize(5500, 100))
        self.pbtnCaseRemove.setBaseSize(QSize(150, 30))
        self.pbtnCaseRemove.setCursor(QCursor(Qt.PointingHandCursor))
        self.pbtnCaseRemove.setContextMenuPolicy(Qt.NoContextMenu)
        self.pbtnCaseRemove.setObjectName("pbtnCaseRemove")
        self.pbtnCaseRemove.setCursor(QCursor(Qt.PointingHandCursor))
        self.pbtnCaseRemove.setToolTip('Registry-Einträge werden entfernt, Fall-Dateien bleiben erhalten!')
        self.verticalLayout_2.addWidget(self.pbtnCaseRemove)
        self.pbtnCaseSave.setMinimumSize(QSize(100, 30))
        self.pbtnCaseSave.setMaximumSize(QSize(5500, 100))
        self.pbtnCaseSave.setBaseSize(QSize(150, 30))
        self.pbtnCaseSave.setCursor(QCursor(Qt.PointingHandCursor))
        self.pbtnCaseSave.setContextMenuPolicy(Qt.NoContextMenu)
        self.pbtnCaseSave.setObjectName("pbtnCaseSave")
        self.pbtnCaseSave.setCursor(QCursor(Qt.PointingHandCursor))
        self.pbtnCaseSave.setToolTip('Registry-Einträge werden in das jewilige X-Ways-Verzeichnis als REG-Datei gespeichert')
        self.verticalLayout_2.addWidget(self.pbtnCaseSave)
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.pbtnAppClose.setSizePolicy(sizePolicy)
        self.pbtnAppClose.setMinimumSize(QSize(100, 30))
        self.pbtnAppClose.setMaximumSize(QSize(5500, 100))
        self.pbtnAppClose.setBaseSize(QSize(150, 30))
        self.pbtnAppClose.setCursor(QCursor(Qt.PointingHandCursor))
        self.pbtnAppClose.setContextMenuPolicy(Qt.NoContextMenu)
        self.pbtnAppClose.setObjectName("pbtnAppClose")
        self.pbtnAppClose.setCursor(QCursor(Qt.PointingHandCursor))
        self.verticalLayout_2.addWidget(self.pbtnAppClose)
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setMinimumSize(QSize(250, 0))
        self.groupBox.setCursor(QCursor(Qt.ArrowCursor))
        self.groupBox.setContextMenuPolicy(Qt.NoContextMenu)
        self.groupBox.setFlat(True)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout.setObjectName("verticalLayout")
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.Label_FolderCase.setObjectName("Label_FolderFall")
        self.verticalLayout.addWidget(self.Label_FolderCase)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.Label_FolderContainer.setObjectName("Label_FolderContainer")
        self.verticalLayout.addWidget(self.Label_FolderContainer)
        self.label_5.setObjectName("label_5")
        self.verticalLayout.addWidget(self.label_5)
        self.Label_FolderExport.setObjectName("Label_FolderExport")
        self.verticalLayout.addWidget(self.Label_FolderExport)
        self.label_7.setObjectName("label_7")
        self.verticalLayout.addWidget(self.label_7)
        self.Label_FolderSave.setObjectName("Label_FolderSicherungen")
        self.verticalLayout.addWidget(self.Label_FolderSave)
        self.label_9.setObjectName("label_9")
        self.verticalLayout.addWidget(self.label_9)
        self.Label_FolderTemp.setObjectName("Label_FolderTemp")
        self.verticalLayout.addWidget(self.Label_FolderTemp)
        self.label_11.setObjectName("label_11")
        self.verticalLayout.addWidget(self.label_11)
        self.Label_FolderHashDB.setObjectName("Label_FolderHashDB")
        self.verticalLayout.addWidget(self.Label_FolderHashDB)
        self.label_13.setObjectName("label_13")
        self.verticalLayout.addWidget(self.label_13)
        self.Label_XWAYSApp.setObjectName("Label_FolderApp")
        self.verticalLayout.addWidget(self.Label_XWAYSApp)
        self.label_15.setObjectName("label_15")
        self.verticalLayout.addWidget(self.label_15)
        self.Label_XWAYSPath.setObjectName("Label_FolderStandardDir")   # 'Pfad'
        self.verticalLayout.addWidget(self.Label_XWAYSPath)
        self.verticalLayout_2.addWidget(self.groupBox)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.menubar.setGeometry(QRect(0, 0, 969, 23))
        self.menubar.setContextMenuPolicy(Qt.CustomContextMenu)
        self.menubar.setAcceptDrops(False)
        self.menubar.setObjectName("menubar")
        self.menuMenu.setObjectName("menuMenu")
        self.setMenuBar(self.menubar)        # frm_XW_Launcher
        self.actionHilfe.setAutoRepeat(False)
        self.actionHilfe.setObjectName("actionHilfe")
        self.actionEinstellungen.setObjectName("actionEinstellungen")
        self.actionInfo.setObjectName("actionInfo")
        self.actionRemoveLauncher.setObjectName("actionRemoveLauncher")
        self.actionUpdateLauncher.setObjectName("actionUpdateLauncher")
        self.menuMenu.addAction(self.actionHilfe)
        self.menuMenu.addAction(self.actionInfo)
        self.menuMenu.addSeparator()
        self.menuMenu.addAction(self.actionUpdateLauncher)
        self.menuMenu.addAction(self.actionUpdatePersonalToolTips)
        self.menuMenu.addAction(self.actionUpdateCaseToolTips)
        self.menuMenu.addAction(self.actionEinstellungen)
        self.menuMenu.addAction(self.actionColorSchema)
        self.menuMenu.addSeparator()
        self.menuMenu.addAction(self.actionRemoveLauncher)
        self.menuMenu.addSeparator()
        self.menubar.addAction(self.menuMenu.menuAction())
        self.retranslateUi()    # (frm_XW_Launcher)

        self.setTabOrder(self.List1, self.pbtnCaseOpen)      # frm_XW_Launcher
        self.setTabOrder(self.pbtnCaseOpen, self.pbtnCaseCreate)      # frm_XW_Launcher
        self.setTabOrder(self.pbtnCaseCreate, self.pbtnCaseRemove)      # frm_XW_Launcher
        self.setTabOrder(self.pbtnCaseRemove, self.pbtnCaseSave)      # frm_XW_Launcher
        self.setTabOrder(self.pbtnCaseSave, self.pbtnAppClose)      # frm_XW_Launcher
        self.setTabOrder(self.pbtnAppClose, self.List1)      # frm_XW_Launcher
        # self.actionInfo.triggered.connect(self._showInfoBox)
        self.List1.currentItemChanged.connect(self._getCaseInfoFromList)
        for name, guiObject in sorted(self.__dict__.items()):
            try:
                if guiObject.objectName().startswith("Label_Folder"):
                    guiObject.setIndent(20)
                    guiObject.setTextInteractionFlags(Qt.TextSelectableByMouse)
                    guiObject.setCursor(QCursor(Qt.PointingHandCursor))
            except AttributeError:
                pass
            except:
                logger_setupUi.debug('Ein Fehler ist aufgetreten: guiObject: {0}:{1}'.format(exc_info()[1], exc_info()[0]))
                # 'QIcon' object has no attribute 'objectName'
                pass
        logger_setupUi.debug('Ende frm_xw_launcher.setupUi, instanz initialisiert')

    def retranslateUi(self):            # (,frm_XW_Launcher)
        self.setWindowTitle(_translate("frm_XW_Launcher", "XWays-Launcher und Fallübersicht", None))      # frm_XW_Launcher
        self.label_2.setText(_translate("frm_XW_Launcher", "Aktive Fälle", None))
        self.List1.setWhatsThis(_translate("frm_XW_Launcher","<html><head/><body><p>aktive x-Ways Fälle/Arbeitsverzeichnisse</p></body></html>",None))
        self.pbtnCaseOpen.setWhatsThis(_translate("frm_XW_Launcher","<html><head/><body><p>X-Ways mit ausgewählten Arbeitsverzeichnis-Einstellungen ausführen</p></body></html>",None))
        self.pbtnCaseOpen.setText(_translate("frm_XW_Launcher", "XWAYS-Fall öffnen", None))
        self.pbtnCaseCreate.setWhatsThis(_translate("frm_XW_Launcher","<html><head/><body><p>X-Ways mit ausgewählten Arbeitsverzeichnis-Einstellungen ausführen</p></body></html>",None))
        self.pbtnCaseCreate.setText(_translate("frm_XW_Launcher", "Fall-Arbeitsverzeichnisse neu anlegen", None))
        self.pbtnCaseRemove.setWhatsThis(_translate("frm_XW_Launcher","<html><head/><body><p>ausgewähltes Arbeitsverzeichnis aus Liste entfernen</p></body></html>",None))
        self.pbtnCaseRemove.setText(_translate("frm_XW_Launcher", "Fall entfernen", None))
        self.pbtnCaseSave.setText(_translate("frm_XW_Launcher", "Falleinstellungen sichern", None))
        self.pbtnAppClose.setWhatsThis(_translate("frm_XW_Launcher", "<html><head/><body><p>Launcher schließen</p></body></html>", None))
        self.pbtnAppClose.setText(_translate("frm_XW_Launcher", "Launcher beenden", None))
        self.pbtnXDoku.setText(_translate("frm_XW_Launcher", "Handbuch zu X-Ways aufrufen", None))
        self.pbtnXDoku.setToolTip(_translate("frm_XW_Launcher", "Dateiname muss mit <span style='font-style:italic; font-weight:bold'>XWI-/XWF-Anleitung</span> beginnen", None))
        self.groupBox.setTitle(_translate("frm_XW_Launcher", "selektierte Einstellungen", None))
        self.label.setText(_translate("frm_XW_Launcher", "Fall Verzeichnis", None))
        self.Label_FolderCase.setText(_translate("frm_XW_Launcher", "-", None))
        self.label_3.setText(_translate("frm_XW_Launcher", "Container Verzeichnis", None))
        self.Label_FolderContainer.setText(_translate("frm_XW_Launcher", "-", None))
        self.label_5.setText(_translate("frm_XW_Launcher", "Export Verzeichnis", None))
        self.Label_FolderExport.setText(_translate("frm_XW_Launcher", "-", None))
        self.label_7.setText(_translate("frm_XW_Launcher", "Sicherung Verzeichnis", None))
        self.Label_FolderSave.setText(_translate("frm_XW_Launcher", "-", None))
        self.label_9.setText(_translate("frm_XW_Launcher", "Temp Verzeichnis", None))
        self.Label_FolderTemp.setText(_translate("frm_XW_Launcher", "-", None))
        self.label_11.setText(_translate("frm_XW_Launcher", "Hash Verzeichnis", None))
        self.Label_FolderHashDB.setText(_translate("frm_XW_Launcher", "-", None))
        self.label_13.setText(_translate("frm_XW_Launcher", "X-Ways App", None))
        self.Label_XWAYSApp.setText(_translate("frm_XW_Launcher", "-", None))
        self.label_15.setText(_translate("frm_XW_Launcher", "X-Ways Pfad", None))
        self.Label_XWAYSPath.setText(_translate("frm_XW_Launcher", "-", None))
        self.menubar.setAccessibleName(_translate("frm_XW_Launcher", "<p style=""text-align:right"">X-Ways Launcher und Vorgangsübersicht</p>", None))
        # <html><head/><body><p style=""text-align:justify"">X-Ways Launcher und Vorgangsübersicht</p></body></html>
        self.menuMenu.setTitle(_translate("frm_XW_Launcher", "&Menu", None))
        self.actionHilfe.setText(_translate("frm_XW_Launcher", "Hilfe", None))
        self.actionHilfe.setToolTip(_translate("frm_XW_Launcher", "Bedienungsanleitung und &Hilfe", None))
        self.actionHilfe.setShortcut(_translate("frm_XW_Launcher", "Alt+H", None))
        self.actionUpdateLauncher.setText(_translate("frm_XW_Launcher", "Launcher &aktualisieren", None))
        self.actionUpdateLauncher.setToolTip(_translate("frm_XW_Launcher", "Launchereinstellung aus der Registry erneut einlesen.", None))
        self.actionUpdateLauncher.setShortcut(_translate("frm_XW_Launcher", "Alt+A", None))
        self.actionEinstellungen.setText(_translate("frm_XW_Launcher", "&Einstellungen", None))
        self.actionEinstellungen.setToolTip(_translate("frm_XW_Launcher", "X-Ways Optionen", None))
        self.actionEinstellungen.setShortcut(_translate("frm_XW_Launcher", "Alt+E", None))
        self.actionInfo.setText(_translate("frm_XW_Launcher", "Info", None))
        self.actionInfo.setToolTip(_translate("frm_XW_Launcher", "Programm&informationen", None))
        self.actionInfo.setShortcut(_translate("frm_XW_Launcher", "Alt+I", None))
        self.actionRemoveLauncher.setText(_translate("frm_XW_Launcher", "Launcher entfernen", None))
        self.actionRemoveLauncher.setToolTip(_translate("frm_XW_Launcher", "Launchereinstellung aus der Registry entfernen.", None))
        self.actionRemoveLauncher.setShortcut(_translate("frm_XW_Launcher", "Ctrl+Alt+R", None))
        self.actionColorSchema.setText(_translate("frm_XW_Launcher", "Farbeinstellungen", None))
        self.actionColorSchema.setToolTip(_translate("frm_XW_Launcher", "Farbeinstellungen anpassen", None))
        self.actionColorSchema.setShortcut(_translate("frm_XW_Launcher", "Ctrl+Alt+C", None))
        self.actionUpdateCaseToolTips.setText(_translate("frm_XW_Launcher", "Setze eigene Tooltips.txt", None))
        self.actionUpdateCaseToolTips.setToolTip(_translate("frm_XW_Launcher", "eigene Tooltips.txt wird aus dem Benutzerverzeichnis in das gewählte Fallverzeichnis kopiert", None))
        self.actionUpdateCaseToolTips.setShortcut(_translate("frm_XW_Launcher", "Ctrl+Alt+T", None))
        self.actionUpdatePersonalToolTips.setText(_translate("frm_XW_Launcher", "Aktualisiere eigene Tooltips.txt", None))
        self.actionUpdatePersonalToolTips.setToolTip(_translate("frm_XW_Launcher", "eigene Tooltips.txt wird durch die Tooltips.txt aus dem gewählten Fallverzeichnis ersetzt", None))
        self.actionUpdatePersonalToolTips.setShortcut(_translate("frm_XW_Launcher", "Ctrl+T", None))

    def _getCaseInfoFromList(self, currItem, prevItem):
        logger_getCaseInfoFromList = logging.getLogger('XWAYS_Launcher.frm_xw_launcher._getCaseInfoFromList{sig}'.format(sig=inspect.signature(self._getCaseInfoFromList)))
        logger_getCaseInfoFromList.debug('Beginn frm_xw_launcher._getCaseInfoFromList {}'.format(''))

        try:
            # if self.localCaseDict[currItem.text():
            self.Label_FolderCase.setText(self.localCaseDict[currItem.text()][self.Label_FolderCase.objectName().replace('Label_Folder', '')])
            self.Label_FolderContainer.setText(self.localCaseDict[currItem.text()][self.Label_FolderContainer.objectName().replace('Label_Folder', '')])
            self.Label_FolderExport.setText(self.localCaseDict[currItem.text()][self.Label_FolderExport.objectName().replace('Label_Folder', '')])
            self.Label_FolderSave.setText(self.localCaseDict[currItem.text()][self.Label_FolderSave.objectName().replace('Label_Folder', '')])
            self.Label_FolderTemp.setText(self.localCaseDict[currItem.text()][self.Label_FolderTemp.objectName().replace('Label_Folder', '')])
            self.Label_FolderHashDB.setText(self.localCaseDict[currItem.text()][self.Label_FolderHashDB.objectName().replace('Label_Folder', '')])
            self.Label_XWAYSApp.setText(" : ".join([self.localCaseDict[currItem.text()]['App'], self.localCaseDict[currItem.text()]['AppVersion']]))    # replace ist mir hier zu umständlich ;)
            self.Label_XWAYSPath.setText(self.localCaseDict[currItem.text()][self.Label_XWAYSPath.objectName().replace('Label_Folder', '')])
            logger_getCaseInfoFromList.debug('Ende frm_xw_launcher._getCaseInfoFromList  ... return item.text(): {}'.format(currItem.text()))
        except:
            logger_getCaseInfoFromList.debug('Ende frm_xw_launcher._getCaseInfoFromList  ... return eines unbekannten Falls im currItem.text(), oder nach Löschung eines Falls')
            self.Label_FolderCase.setText('')
            self.Label_FolderContainer.setText('')
            self.Label_FolderExport.setText('')
            self.Label_FolderSave.setText('')
            self.Label_FolderTemp.setText('')
            self.Label_FolderHashDB.setText('')
            self.Label_XWAYSApp.setText('')
            self.Label_XWAYSPath.setText('')
        return currItem


# if __name__ == "__main__":
#     import sys
#     app = QApplication(sys.argv)
#     frm_XW_Launcher = QMainWindow()
#     ui = Ui_frm_XW_Launcher()
#     ui.setupUi(frm_XW_Launcher)
#     frm_XW_Launcher.show()
#     sys.exit(app.exec_())
