benötigte Module/dependencies<br>
<br>
Python 3.6.4<br>
PyQt5.10<br>
pywin32 223<br>
<br>
Installation:<br>
<br>
* copy Winpython 3.6 into the applications root directory  -> C:\\xways-launcher\
* create a sub-directory 'XWF-Versionen' if you intend to use X-Ways Forensics applications
  + naming convention for application directories refers to application version like:
    -> xwf_19.3 SR-6 x64

* create a sub-directory 'XWI-Versionen' if you intend to use X-Ways Investigator applications
  + much alike for the forensics app but with 'xwi_' as prefix  
  
  
  
Remote update now possible: upload a prepared directory as a zip file into the program location,  
at the next execution of the script, this file is unzipped into the intended folder according
to the prefix of the zip file:
+ xwi_....zip -> /XWI-Versionen/xwi_...

